#!/usr/bin/env python3
import gevent.monkey
gevent.monkey.patch_all()
from terradactsl import TerraDactSL
TerraDactSL().main()
