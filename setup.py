from setuptools import setup, find_packages

setup(
    name="terradactsl",
    version="0.1",
    packages=find_packages(),
    install_requires=[
        "coloredlogs",
        "termcolor",
        "terrascript>=0.8",
        "pyyaml",
        "jinja2",
        "networkx",
        "plotly",
        "frozendict",
        "psutil"
    ],
    include_package_data=True
)
