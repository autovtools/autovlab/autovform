#!/usr/bin/env python3

import os
import time
import json
import logging
import argparse
import traceback

import gevent
from gevent import sleep

import terradactsl
from terradactsl import utils as utils

class TerraDactSL():
    """
    A Python x Terraform Domain Specific Language
    for describing and building virtual networks.

    This class implements the CLI interface.
    """

    def __init__(self):
        self.project = None
        self.project_tf = None
        self.args = self.get_args()
        self.project_tf_path = None
        self.tf_proc = None
        self.customizers = {}
        # seconds to wait after terraform apply starts before customizers start
        self.customizer_delay = 60

        self.summary = None
        self.last_op = None
        self.last_op_start = None

        show_line_info = False
        self.log_level = logging.INFO
        self.show_line_info = False

        if self.args.verbosity:
            self.log_level = logging.DEBUG
            if self.args.verbosity > 1:
                self.show_line_info = True
        elif self.args.quiet:
            self.log_level = logging.ERROR

        self.logger = utils.get_logger(
            name=self.__class__.__name__,
            log_level=self.log_level,
            log_file=self.args.log_file,
            show_line_info=show_line_info
        )

        self.config = utils.merge_config(*self.args.config, disable_jinja=self.args.disable_jinja)
        if self.args.var_file:
            # Load any settings from var_files that we are supposed to
            #   (JSON format => us and terraform, HCL => terraform only)
            extra_config = utils.merge_config(*self.args.var_file, strict=False)
            self.config.update(extra_config)

        if self.args.working_dir and not os.path.exists(self.args.working_dir):
            raise ValueError(f"{self.args.working_dir} does not exist")
        if self.args.plugin_dir and not os.path.exists(self.args.plugin_dir):
            raise ValueError(f"{self.args.plugin_dir} does not exist")

    def get_args(self):
        """
        Parse CLI args and return the result.
        """
        parser=argparse.ArgumentParser(
            description="PyTerraDactSL: A Python x Terraform Domain Specific Language" \
                        " for describing and building virtual networks."
        )
        parser.add_argument(
            "config",
            nargs="+",
            help="A config file (json) describing the project to build."
                 " Repeatable. All config files are merged into a single project description."
                 " If a key appear in multiple files, the rightmost value is used."
        )
        parser.add_argument(
            '-v', '--verbosity',
            action="count",
            help="Increase debug logging. Repeatable for more verbosity."
        )
        parser.add_argument(
            '-q', '--quiet',
            action="store_true",
            help="Only log errors"
        )
        parser.add_argument(
            '-l', '--log-file',
            help="Also log all output to this file"
        )
        parser.add_argument(
            '--debug-shell',
            action="store_true",
            help="[ADVANCED USERS ONLY] Drop into an IPython shell before building."
        )
        parser.add_argument(
            '--visualize',
            action="store_true",
            help="Generate visualization by default."
        )

        parser.add_argument(
            "--var-file",
            action="append",
            help="Extra terraform var-files that should be passed as var-files to terraform."
                 " Repeatable. If files are in json format, they are also merged into the config."
                 " (Ex. infrastructure creds)"
        )
        parser.add_argument(
            "--plugin-dir",
            help="Terraform -plugin-dir option. default: /opt/terraform/",
            default="/opt/terraform"
        )
        parser.add_argument(
            "--working-dir",
            help="Working directory to use for plan generation and terraform workspace organization."
                 " If given, it must exist and be accesible"
                 " By default, GROUP_NAME/PROJECT_NAME is used (created as needed)"
        )
        parser.add_argument(
            "--disable-jinja",
            help="If set, disallow jinja syntax in config files",
            action="store_true",
        )
        
        args = parser.parse_args()

        return args

    def format_ips(self, name, ips, zones, zone_to_network, ip_colorer=None):
        main_banner = "#"*80
        banner = "-"*80
        summary = f"""
{main_banner}
{name}
{main_banner}
"""
        summary = utils.vm_summary(summary)
        if not isinstance(ips, list):
            ips = [ips]
        if not isinstance(zones, list):
            zones = [zones]
        max_zone_len = max(len(str(zone)) for zone in zones)
        networks = [zone_to_network[zone] for zone in zones]
        max_net_len = max(len(str(net)) for net in networks)

        for ip, zone, network in zip(ips, zones, networks):
            if not ip:
                ip = "(DHCP)"
            ip = str(ip).ljust(16)
            zone_str = str(zone).ljust(max_zone_len)
            network = zone_to_network[zone]
            network = str(network).ljust(max_net_len)
            line = f"  - {ip}[ {zone_str} | {network} ]\n{banner}\n"
            if ip_colorer:
                line = ip_colorer(line)
            summary += line

        return summary

    def _ip_summary(self):
        ips = {}
        for vm in self.project.get_vms(): 
            for info in vm.instances:
                ips[info["vsphere_name"]] = (info["ipv4_address"], info["attached_to"])
        return ips

    def _ip_to_vm(self):
        lookup = {}
        for vm in self.project.get_vms(): 
            for info in vm.instances:
                for ip in info["ipv4_address"]:
                    utils.dict_append(
                        lookup,
                        ip,
                        info["vsphere_name"]
                    )
        return lookup

    def _zone_maps(self):
        unique_zones = set()
        for project in self.project.get_projects():
            for zone in project.zones.values():
                unique_zones.add(zone)
        zone_to_network = {}
        network_to_zone = {}
        for zone in unique_zones:
            for info in zone.instances:
                utils.dict_append(
                    zone_to_network,
                    info["zone_name"],
                    str(info["zone_network"]),
                )
                utils.dict_append(
                    network_to_zone,
                    str(info["zone_network"]),
                    info["zone_name"]
                )

        return zone_to_network, network_to_zone

    def ip_summary(self):
        summary = ""
        ip_map = self._ip_summary()
        zone_to_network, network_to_zone = self._zone_maps()
        for vsphere_name, ips_zones in ip_map.items():
            ips, zones = ips_zones
            summary += self.format_ips(vsphere_name, ips, zones, zone_to_network)
        print(summary)

    def _format_route_networks(self, networks, network_to_zone):
        if not networks:
            return ""
        summary = ""
        banner = "-"*80
        zones = [network_to_zone[network] for network in networks]
        max_zone_len = max(len(zone) for zone in zones)
        for network in networks:
            zone = network_to_zone[network]
            network = str(network).ljust(16)
            zone = str(zone).ljust(max_zone_len)
            summary += f"    - {network}[ {zone} ]\n{banner}\n"

        return summary

    def _route_summary(self, route_summary):
        summary = ""
        ip_map = self._ip_summary()
        ip_to_vm = self._ip_to_vm()
        zone_to_network, network_to_zone = self._zone_maps()
        for vsphere_name, routes in route_summary.items():
            ips, zones = ip_map[vsphere_name]
            summary += self.format_ips(
                vsphere_name,
                ips,
                zones,
                zone_to_network,
                ip_colorer=utils.zone_summary
            )

            for route_label, route_dict in routes.items():
                summary += f"\n{route_label}:\n"
                vsphere_names = [ip_to_vm[via] for via in list(route_dict)]
                max_len = max(len(vsphere_name) for vsphere_name in vsphere_names)
                for (via, networks), vsphere_name in zip(route_dict.items(), vsphere_names):
                    if not via:
                        via = "(DHCP)"
                    else:
                        vsphere_name = str(vsphere_name).ljust(max_len)
                        via = str(via).ljust(16)
                        via += f"[ {vsphere_name} ]"
                    summary += f"  @ {via}\n"
                    summary += self._format_route_networks(networks, network_to_zone)
        return summary

    def route_summary(self):
        route_summary = self.project.get_routes()
        summary = self._route_summary(route_summary)
        print(summary)

    def save(self):
        if not self.project_tf:
            return False

        terra_dir = self.args.working_dir
        if not terra_dir:
            terra_dir = os.path.join(
                os.getcwd(),
                self.project.config["group_name"],
                self.project.config["project_name"]
            )
            os.makedirs(terra_dir, exist_ok=True)

        self.project_tf_path = os.path.join(terra_dir, "main.tf.json")

        with open(self.project_tf_path, "w") as tf_file:
            json.dump(self.project_tf, tf_file, indent=2)

        return True

    def op_complete(self, result):
        banner = "-"*80
        now = time.time()
        summary = f"""
{banner}
Operation {self.last_op} completed. Elasped: {utils.get_elapsed(self.last_op_start)}
{banner}
"""
    def invoke_terraform(self, operation, helpers=[]):
        if not (self.project_tf and self.project_tf_path):
            raise Exception("Cannot apply until project is rendered and saved!")

        if operation not in ["plan", "apply", "destroy"]:
            raise ValueError(f"Invalid operation: {operation}")
        self.last_op = operation
        self.last_op_start = time.time()
        exc = None

        # Simplify the logging output
        old_fmt = self.logger.handlers[0].formatter
        sub_fmt = utils.get_subprocess_formatter("TF")
        utils.apply_formatter(self.logger, sub_fmt)

        try:
            res = utils.invoke_terraform(
                operation,
                self.project_tf_path,
                var_files=self.args.var_file,
                plugin_dir=self.args.plugin_dir,
                logger=self.logger,
            )
            if not res:
                self.logger.warning(f"{operation} cancelled")
            else:
                proc, greenlets = res
                greenlets.extend(helpers)
                self.tf_proc = proc
                res = utils.wait_proc(proc, greenlets=greenlets, logger=self.logger)
                if res != 0:
                    self.stop_customizers()
                    return False
                if helpers:
                    self.logger.info("Waiting for helpers to finish...")
                    self.check_customizers()
                    gevent.joinall(helpers)
            
                gevent.joinall(greenlets)
                return res
        except KeyboardInterrupt:
            self.logger.warning(f"Interrupted. Stopping helpers")
            self.stop_customizers()
        except Exception as e:
            # Restore the detailed logging
            utils.apply_formatter(self.logger, old_fmt)
            self.logger.warning(f"Caught exception from invoke_terraform: {e}")
            self.logger.warning(traceback.format_exc())
        finally:
            # Restore the detailed logging
            utils.apply_formatter(self.logger, old_fmt)
    
    def plan(self):
        return self.invoke_terraform("plan")

    def apply(self):
        customizers = self.project.get_customizers()
        helpers = []
        # Kill any greenlets we are about to lose handles to
        self.stop_customizers()
        self.customizers = {}
        if customizers:
            self.logger.info(f"Running {len(customizers)} customizers...")
            # Delay the helpers so the user has time to review the plan
            #   before we generate tons of logs
            for cust in customizers:
                helper = gevent.spawn_later(self.customizer_delay, cust.run)
                helpers.append(helper)
                self.customizers[cust.vsphere_name] = helper
            helpers.append(
                gevent.spawn(self.monitor_customizers)
            )

        return self.invoke_terraform("apply", helpers=helpers)

    def destroy(self):
        self.stop_customizers()
        return self.invoke_terraform("destroy")

    def monitor_customizers(self, frequency=120):
        while self.check_customizers():
            sleep(frequency)

    def check_customizers(self):
        if not self.customizers:
            self.logger.warning("No customizers are being tracked!")
            return

        banner = "-"*80
        main_banner = "#"*80
        finished = {
            name:cust
            for name, cust in self.customizers.items()
            if cust.dead
        }
        running = {
            name:cust
            for name, cust in self.customizers.items()
            if not cust.dead
        }
        
        finished_summary = utils.greenlet_summary(finished)
        running_summary = utils.greenlet_summary(running)
        summary = f"""
{main_banner}
Customizers: ({len(self.customizers)})
{main_banner}
Finished: ({len(finished)})
{banner}
{finished_summary}
{banner}
Running: ({len(running)})
{banner}
{running_summary}
{main_banner}
"""
        self.logger.info(summary)
        # Return number of customizers still running
        return len(running)

    def stop_customizers(self):
        if self.customizers:
            gevent.killall(self.customizers.values())
            gevent.joinall(
                self.customizers.values(),
                timeout=10
            )
    
    def main(self):
        # TODO: TUI
        try:
            self.project = terradactsl.Project(
                config=self.config,
                log_level=self.log_level,
                log_file=self.args.log_file,
                show_line_info=self.show_line_info,
            )    

            self.summary = self.project.render()
            self.project_tf = self.project._tf

            print(self.summary)

            res = self.save()
        except Exception as e:
            self.logger.error(traceback.format_exc())
            self.logger.error(e)

        if self.project and self.args.visualize:
            viz = utils.project_viz(self.project)
            fig = utils.render_viz(self.project, viz)

        if self.args.debug_shell:
            # Only require IPython if user cares about debug shell
            import IPython
            IPython.embed()

