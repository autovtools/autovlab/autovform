from .zone import Zone

class UplinkZone(Zone):

    def __init__(self, project, uplink=None, **kwargs):
        # Before the Zone config is normalized, patch in non-standard defaults
        #   that make sense for an UplinkZone
        if not kwargs.get("config", None):
            kwargs["config"] = {}
        # mutate kwargs["config"]
        config = kwargs["config"]
        self.apply_default(config, "port_group", uplink)
        # DHCP
        self.apply_default(config, "zone_network", "")
        self.apply_default(config, "dhcp", True)
        self.apply_default(config, "create_zone_folders", False)
        
        # Run the Zone constructor like normal
        super().__init__(project, **kwargs)

