import os
import itertools
from ipaddress import IPv4Address

import terrascript
import terrascript.data
import terrascript.resource

import terradactsl
from terradactsl import utils as utils
from terradactsl import opts as opts
from terradactsl import Configurable, VM

class Zone(Configurable):
    opts = opts.ZONE_OPTS
    unknown_key = "vms"
    unknown_opts = VM.opts
    attributes = ["vms", "projects", "config"]

    def __init__(self, project, **kwargs):
        self.inherit_logger_settings(project, kwargs)
        super().__init__(**kwargs)
        self.vms = {}
        self.projects= {}
        self.project = project
        self.visited = False

        # Used to store a generator for host IPs in the zone
        # Changes for each instance
        self.zone_hosts = None

        self.static_assignments = []
        # Used to collect all instance-specific information that
        #   can be retreived later (to build firewalls)
        self.instances = []
        self.vapp_id = None

        project_keys = [
            "create_zone_folders",
            "merge_zone_folders",
            "datacenter_id",
            "datastore_id",
            "dvs_id",
            "num_instances",
            "log_level",
            "first_instance",
            "resolve_hosts",
            "domain",
            "create_vapp",
            "group_name"
        ]
        self.inherit(project_keys, self.project)
        self.init_logger()
        if self.config["num_instances"] is None:
            # Not instanced => 1 instance
            self.num_instances  = 1
        else:
            self.num_instances = self.config["num_instances"]


        # Register VMs
        if self.config["vms"] is not None:
            for vm_name, vm_config in self.config["vms"].items():
                self.logger.info(f"Adding VM {vm_name}")
                self.add_vm(vm_name, vm_config)

        for vm_name, vm in self.vms.items():
            if "ipv4_address" in vm.config and vm.config["ipv4_address"]:
                # vm requested specific ip address;
                #   reserve it so we don't double assign
                self.config["exclude_hosts"].append(
                    vm.config["ipv4_address"]
                )

        if self.config["projects"] is not None:
            for project_name, project_config in self.config["projects"].items():
                self.logger.info(f"Adding sub-Project {project_name}")
                self.add_project(project_name, project_config)

        # Back up the static part of exclude_hosts
        #   (the gateway will vary by instance)
        self.exclude_hosts = self.config["exclude_hosts"]

        self.original_config = self.config.copy()

    def add_project(self, project_name, project_config):
        if project_name in self.projects:
            raise ValueError(f"ERROR: Duplicate project_name: {project_name}. Project Names must be unique.")

        project = terradactsl.Project(project=self.project, zone=self, config=project_config, name=project_name)

        self.projects[project_name] = project

    def add_vm(self, vm_name, vm_config):
        if vm_name in self.vms:
            raise ValueError(f"ERROR: Duplicate vm_name: {vm_name}. VM Names must be unique.")
        
        if isinstance(vm_config, VM):
            self.vms[vm_name] = vm_config
        else:
            self.vms[vm_name] = VM(self.project, config=vm_config, name=vm_name)

    def assign_network(self, vm):
        """
        Returns the networking information for a VM, which should be part of this Zone.

        Called by VMs to ask the containing zone for the appropriate networking info needed to render.
        """
        # network_id: vsphere_distributed_port_group.pg.id
        zone_network = self.config["zone_network"]
        ip = ""
        ip_assignment = "static"

        if self.config["dhcp"]:
            ip_assignment = "dhcp"
        else:
            # Assign a static IP
            ip_gen = self.zone_hosts
            if ip_gen == "":
                # We don't have a generator; Only DHCP
                ip = ""
            else:
                ip = next(ip_gen) 
                if ip == self.config["zone_gateway"]:
                    # If the IP matches the gateway, discard it
                    #   (it's already in use, don't assign it to a host)
                    ip = next(ip_gen)

                # Record the IPs we gave out for later
                self.static_assignments.append(ip)
                ip = str(ip)

        ipv4_gateway = ""
        if self.config["zone_gateway"]:
            ipv4_gateway = str(self.config["zone_gateway"])
        ipv4_netmask = ""
        if zone_network and ip_assignment == "static":
            ipv4_netmask = zone_network.prefixlen

        net_info = {
            "port_group"    : self.config["port_group"],
            "network_id"    : self.config["network_id"],
            "ipv4_address"  : ip,
            "ipv4_netmask"  : ipv4_netmask,
            "ipv4_gateway"  : ipv4_gateway,
            "ip_assignment" : ip_assignment,
            "dns_servers"   : self.config["dns_servers"]
        }
        self.logger.warning(f"{self.name} assignment to {vm.name}")
        self.logger.warning(utils.sanitize_config(net_info))
        return net_info

    def assign_folder(self, vm):
        return self.config["instance_folder"]

        """
        def get_full_suffix(self):
            suffix = self.get_suffix()
            if suffix:
                suffix  = f"Z{suffix}"
            return utils.suffix_join(
                self.project.get_full_suffix(),
                suffix
            )
        """

    def peek_hosts(self):
        # Make a copy of zone_hosts generator so we can 'preview'
        #   what the host ip assignments will be
        # https://docs.python.org/3/library/itertools.html#itertools.tee

        # Always start from the un-consumed generator, since we are peeking
        self.zone_hosts_bk, hosts_copy = itertools.tee(self.zone_hosts_bk)

        # implement exclude_first
        num_hosts = int(self.config["exclude_first"])
        for index in range(num_hosts):
            next(hosts_copy)

        hosts_list = []

        # Only generate what we need
        # Respect exclude_hosts
        vms = list(self.vms.keys())
        for project in self.projects.values():
            vms.extend(project.infra.vms.keys())
        for vm_name in vms:
            host_ip = None
            while True:
                host_ip = str(next(hosts_copy))
                if host_ip not in self.config["exclude_hosts"]:
                    break
            if host_ip:
                hosts_list.append(host_ip)

        return hosts_list

    def _text_summary(self, zone_network=None, zone_gateway=None, hosts_list=None):
        summary = f"""
-------------------------------------------------------------------------------
{self.name} ({self.config["instance_name"]}) | {self.project.name}
-------------------------------------------------------------------------------
    vsphere_folder  : {self.config["instance_folder"]}
    port_group      : {self.config["port_group"]}
    zone_network    : {zone_network}
    zone_gateway    : {zone_gateway}
    zone_hosts      : {hosts_list}
-------------------------------------------------------------------------------
"""
        return summary

    def _html_summary(self, zone_network=None, zone_gateway=None, hosts_list=None):
        summary = f'<b>{self.name}</b>'
        attrs = {
            "zone_network"  : zone_network,
            "zone_gateway"  : zone_gateway,
            "port_group"    : self.config["port_group"],
        }
        summary += utils.format_html_attrs(attrs)
        hosts_list = utils.dumps(hosts_list)
        attrs = {
            "project_name"  : self.project.name,
            "vsphere_folder": self.config["instance_folder"],
            "zone_hosts"    : hosts_list
        }
        extra = utils.format_html_attrs(attrs)
        summary += f"<extra>{extra}</extra>"
        return summary

    def summary(self, color=True, html=False):
        hosts_list = None
        if self.zone_hosts == "":
            # We are only capable of assigning DHCP IPs
            hosts_list = [""] * len(self.vms)
        else:
            # We actually have an IP generator
            hosts_list = self.peek_hosts()

        # Make empty strings more human-readable
        zone_network = str(self.config["zone_network"])
        if zone_network == "":
            zone_network = "(DHCP)"

        zone_gateway = str(self.config["zone_gateway"])
        if zone_gateway == "":
            zone_gateway = "(DHCP)"

        summary = None
        if html:
            summary = self._html_summary(
                zone_network=zone_network,
                zone_gateway=zone_gateway,
                hosts_list=hosts_list
            )
        else:
            summary = self._text_summary(
                zone_network=zone_network,
                zone_gateway=zone_gateway,
                hosts_list=hosts_list
            )

            if color:
                summary = utils.vm_summary(summary)

        return summary

    def _prep_instance(self, curr_instance):
        """
        Populate any instance_* variables that change for each instance, such as names.
        """
        self.curr_instance = curr_instance
        curr = self.curr_instance
        if curr is None:
            curr = 0
        #self.config["instance_name"] = utils.get_zone_instance_name(self, self.curr_instance)
        self.name = self.next_instance_name()
        self.config["instance_name"] = self.name

        if self.original_config["zone_network"]:
            # Explicit network from user
            self.config["zone_network"] = utils.to_network(self.original_config["zone_network"])
        else:
            # Select the correct IPv4Network assigned through subnetting
            self.config["zone_network"] = self.config["zone_networks"][curr]


        if not self.original_config["zone_gateway"]:
            # Default gateway = first IP in network
            if self.config["zone_network"]:
                self.config["zone_gateway"] = next(self.config["zone_network"].hosts())
            else:
                self.config["zone_gateway"] = ""

        # self.zone_hosts = a generator of IP addresses used to assign IPs to VMs
        if self.original_config["zone_hosts"]:
            # User gave us explicit IP assignments
            # Convert list -> generator
            self.zone_hosts = (host for host in self.original_config["zone_hosts"])
        elif self.config["zone_network"] == "":
            # We are on an unmanaged network; everyone has to be DHCP
            self.zone_hosts = ""
            self.zone_hosts_bk = ""
        else:
            # We have a real zone_network and can assign IPs
            self.zone_hosts = self.config["zone_network"].hosts()
            # Save an un-consumed backup of our iterator so we can replay assignments
            self.zone_hosts, self.zone_hosts_bk = itertools.tee(self.zone_hosts)

        # We need to set instance_folder *after* the project renders,
        #   So that the project's instance_folders are ready
        self.config["instance_folder"] = self.project.assign_folder(self)
        if self.original_config["create_zone_folders"]:
            # Expand instance_folder into full path,
            #   if we are creating a subfolder
            if self.original_config["zone_folder"] is None:
                # If merge_zone_folders, then do not use our instance_name
                #    (which has a number appeneded based on curr_instance)
                # Also don't append a suffix if we are already in our own instance folder
                zone_folder = self.config["instance_name"]
                if self.original_config["merge_zone_folders"] or self.project.instance_folders:
                    zone_folder = self.config["zone_name"]

                self.config["instance_folder"] = os.path.join(
                    self.config["instance_folder"],
                    zone_folder
                )
            else:
                # We got an explicit folder name from the user; use that
                self.config["instance_folder"] = os.path.join(
                    self.config["instance_folder"],
                    self.config["zone_folder"]
                )
        # Assert now instance_folder is the correct, full path to put VMs in

        # Set up exclude_hosts to prevent zone.assign_network
        #   from double-assigning
        self.config["exclude_hosts"] = self.exclude_hosts

        zone_gateway = self.config["zone_gateway"]
        if zone_gateway:
            # Make sure we can't assign a host the same IP as the gateway
            self.config["exclude_hosts"].append(str(zone_gateway))

            # Get DNS settings
            if not self.original_config["dns_servers"]:
                # Assume the gateway is our DNS server
                self.config["dns_servers"] = [str(zone_gateway)]

        # Discard the first X IPs, if requested
        if self.config["exclude_first"]:
            num_hosts = int(self.config["exclude_first"])
            for index in range(num_hosts):
                next(self.zone_hosts)

        # Record whatever instance-specific info we want to look up later
        info = {}
        info["zone_name"] = self.name
        info["zone_network"] = self.config["zone_network"]
        info["zone_gateway"] = self.config["zone_gateway"]
        info["static_assignments"] = self.static_assignments
        info["instance_folder"] = self.config["instance_folder"]
        info["zone_hosts"] = self.zone_hosts
        info["exclude_hosts"] = self.config["exclude_hosts"]
        info["root_project"] = self.root_project
        # We don't know IP addresses assigned to infra VMs yet, so register a callback
        info["summary"] = lambda: self.summary(html=True)
        # We don't know the port_group or network_id yet, so we have to add them later
        self.instances.append(info)

    def _render_data(self, _tf):
        if self.original_config.get("port_group", None):
            # Make sure the resource name is unique, even if the same port group is used
            #   multiple times in the same project

            resource_name = utils.get_resource_name(
                self.config["port_group"],
                prefix="pg_{}_".format(
                    self.project.name
                )
            )
            _tf += terrascript.data.vsphere_network(
                resource_name,
                name          = self.config["port_group"],
                datacenter_id = self.config["datacenter_id"]
            )
            # Update the network_id so it can be retrieved later
            self.config["network_id"] = "${" + f"data.vsphere_network.{resource_name}.id" + "}"

    def _render_resources(self, _tf):
        """
        Necessary for each instance:
            * folder
            * port group
        """
        curr = self.curr_instance
        zone_name = self.config["zone_name"]
        instance_name = self.config["instance_name"]
        # Prevent non-VM zone resources from depending on any Infra VMs
        # A zone's VMs depend on the firewall, but we get a cycle if
        #   a zone's port groups depend on the firewall
        deps = self.get_dependencies(exclude_types=["vsphere_virtual_machine"])

        if self.config["create_zone_folders"]:
            resource_name = utils.get_resource_name(instance_name, prefix="zone_folder_")
            instance_folder = self.config["instance_folder"]

            # Be careful to create only one zone folder when merge_zone_folders is true
            previous_instances = self.instances[:curr]
            if not any(instance_folder == info["instance_folder"] for info in previous_instances):
                # Assert there is no previously created folder with the same path
                # Create new resource

                # Prevent zone folder from depending on any Infra VMs
                deps = self.get_dependencies(exclude_types=["vsphere_virtual_machine"])

                _tf += terrascript.resource.vsphere_folder(
                    resource_name,
                    depends_on    = deps,
                    path          = instance_folder,
                    type          = "vm",
                    datacenter_id = self.config["datacenter_id"]
                )

                self.add_resource(resource_name, "vsphere_folder")

        if self.config["create_vapp"] and self.vms:
            vapp_name = utils.get_vapp_name(self, prefix=self.project.name)
            folder_id = self.project.folder_ids[-1]
            parent_pool = self.project.vapp_id
            if not parent_pool:
                parent_pool = self.project.get_pool_id()
            self.vapp_id = self.project.render_vapp_container(_tf, folder_id, parent_pool, vapp_name)
        else:
            # Clear the vapp_id for this instance
            self.vapp_id = None

        if self.original_config["port_group"] is None:
            # Create a port group
            resource_name = utils.get_resource_name(
                instance_name,
                prefix="pg_{}_".format(
                    self.project.name
                )
            )
            full_name = f"vsphere_distributed_port_group.{resource_name}"

            pg_name = utils.get_port_group_name(self)
            self.config["port_group"] = pg_name
            vlan_id = self.project.get_vlan(pg_name)

            
            _tf += terrascript.resource.vsphere_distributed_port_group(
                resource_name,
                depends_on    = deps,
                name          = pg_name,
                distributed_virtual_switch_uuid = self.config["dvs_id"],
                vlan_id       = vlan_id
            )

            self.add_resource(resource_name, "vsphere_distributed_port_group")
            # Update the network_id so it can be retrieved later
            #   (_render_data did not set this for us)
            self.config["network_id"] = "${" + f"{full_name}.id" + "}"

    def _render_vms(self, _tf):
        vm_summary = ""
        for vm_name, vm in self.vms.items():
            self.logger.info(f"Rendering {vm_name}...")
            # Clear and reset the depencies for the VM
            vm.clear_dependencies()
            vm.instance_depends_on(self)
            vm.instance_depends_on(self.project.infra)

            vm_summary += vm.render(_tf, self, self.curr_instance)
            self.logger.info(f"Rendered {vm_name}.")
        return vm_summary

    def _render_projects(self, _tf):
        project_summary = ""
        for project_name, project in self.projects.items():
            self.logger.info(f"Rendering {project_name}...")
            # Clear and reset the depencies for the VM
            project.dependencies = []
            project.depends_on(self)
            project.instance_depends_on(self)
            project.instance_depends_on(self.project.infra)
            project.depends_on(self.project)
            project.instance_depends_on(self.project)

            project_summary += project.render(_tf=_tf)
            self.logger.info(f"Rendered {project_name}.")
        return project_summary
        
    def render(self, _tf, curr_instance):
        self.root_project = self.project.root_project
        self.render_index += 1
        if self.original_config.get("zone_networks", None) is None:
            # There is no entry for zone_networks
            self.config["zone_networks"] = self.project.assign_zone_networks(self)

        self.logger.debug("Rendering data...")
        self._render_data(_tf)

        self.logger.info(f"Rendering instance {curr_instance}")
        
        self._prep_instance(curr_instance)
        self._render_resources(_tf)
        # Assert self.config["network_id"] is set by now (before VMs ask for their network info)
        #   (It was set by either render_data or render_resources)

        # Store the port group info in the most recent instance info
        self.instances[-1]["network_id"] = self.config["network_id"]
        self.instances[-1]["port_group"] = self.config["port_group"]
        #self.instances[-1]["summary"] = self.summary(html=True)
        
        # Now that zone resources (e.g. port groups) are known,
        #   We have to tell our firewall, because it depends on them
        #self.infra.depends_on(self)
        # Everything we make after this (e.g. VMs) depends
        #   on the fw / infra
        #self.depends_on(self.infra)

        vm_summary = self._render_vms(_tf)
        project_summary = ""
        if self.base_name != self.project.config["uplink_zone"]:
            project_summary = self._render_projects(_tf)

        summary = self.summary()
        summary += vm_summary
        summary += project_summary

        return summary
