import os
import json
import time
import urllib
import hashlib
import textwrap
import tempfile
import shutil
from pathlib import Path
from datetime import timedelta
from ipaddress import IPv4Address, IPv4Network 

import jinja2
import yaml
import termcolor
import requests

def dumps(blob, indent=2, prefix="", yml=True):
    if yml:
        blob_str = yaml.dump(blob, indent=indent, default_flow_style=False, sort_keys=False)
        if blob_str.endswith("\n"):
            return blob_str[:-1]
        return blob_str

    blob_str = json.dumps(blob, indent=indent)
    if prefix:
        return textwrap.indent(blob_str, prefix)
    else:
        return blob_str

def sort_dict(obj):
    """
    Sorts a dictionary (obj) by its keys and returns a new dict.
    """
    _obj = {}
    for key in sorted(obj.keys()):
        _obj[key] = obj[key]
    return _obj

def dict_append(_dict, key, val):
    if key in _dict:
        if not isinstance(_dict[key], list):
            _dict[key] = [_dict[key]]
        _dict[key].append(val)
    else:
        _dict[key] = val

def dict_merge(dict_a, dict_b):
    # https://stackoverflow.com/questions/20656135/python-deep-merge-dictionary-data
    for key, val in dict_b.items():
        if isinstance(val, dict):
            child = dict_a.setdefault(key, {})
            dict_merge(child, val)
        else:
            dict_a[key] = val

    return dict_a
    
def hash_file(filename):
    chunk_size = 65536
    hasher = hashlib.sha256()
    with open(filename, "rb") as in_file:
        while True:
            data = in_file.read(chunk_size)
            if not data:
                break
            hasher.update(data)
    return hasher.hexdigest()

def load_config(config, is_file=True):
    content = config
    if is_file:
        with open(config, "r") as f:
            content = f.read()
    
    return yaml.safe_load(content)

def try_load(config, strict=True, is_file=True):
    try:
        return load_config(config, is_file=is_file)
    except ValueError as e:
        # strict=False -> ignore invalid JSON files
        if strict:
            raise(e)

def merge_config(*configs, strict=True, disable_jinja=False, jinja_funcs={}):
    res = {}
    for config in configs:
        if isinstance(config, dict):
            res.update(res)
            continue
        # It's a filename
        is_file = True
        config = os.path.abspath(os.path.expanduser(config))
        if not disable_jinja:
            search_dir, basename = os.path.split(config)
            basename = os.path.basename(config)
            jinja_env = jinja2.Environment(
                loader=jinja2.FileSystemLoader(search_dir),
                cache_size=0
            )

            for func_name, func in jinja_funcs.items():
                jinja_env.globals[func_name] = func

            template = jinja_env.get_template(os.path.basename(basename))
            config = template.render()
            is_file = False

        res.update(try_load(config, strict=strict, is_file=is_file))
    return res

def list_to_gen(_list):
    return (item for item in _list)
    
def gen_to_list(gen, max_items=None):
    if max_items is None:
        return list(gen)
    else:
        return [
            item for item, _ in
                zip(gen, range(max_items))
        ]

def sanitize_config(config, as_str=True):
    """
    Blank out any 'secret' top-level values in a config dictionary.
    """
    secrets = ["user", "password", "secrets.json"]
    if not isinstance(config, dict):
        return config
    conf = {
        key:sanitize_config(val, as_str=False)
        if not (any(key and key.endswith(secret) for secret in secrets))
        else "..."
        for key, val in config.items()
    }
    if as_str:
        return json.dumps(conf, indent=2)
    else:
        return conf

def colored(*args, **kwargs):
    return termcolor.colored(*args, **kwargs)

def vm_summary(summary):
    return colored(summary, "cyan")

def zone_summary(summary):
    return colored(summary, "yellow")

def project_summary(summary):
    return colored(summary, "white")

def greenlet_summary(greenlet_dict, alive_color="yellow", dead_color="green"):
    if not greenlet_dict:
        return ""

    max_len = max([len(key) for key in greenlet_dict])
    greenlets = sort_dict(greenlet_dict)

    summary = []
    for name, greenlet in greenlet_dict.items():
        status = None
        if greenlet.dead:
            status = colored("Finished", dead_color)
        else:
            status = colored("Running", alive_color)
        summary.append(f"{name.ljust(max_len)} : {status}") 
    return "\n".join(summary)

def get_elapsed(since):
    now = time.time()
    secs = int(now - since)
    return str(timedelta(seconds=secs))

def make_serializable(info):
    ignore = ["zone_hosts", "static_assignments", "exclude_hosts"]
    _info = {} 
    for key, val in info.items():
        if key in ignore:
            continue
        if isinstance(val, IPv4Address) or isinstance(val, IPv4Network):
            val = str(val)
        elif isinstance(val, list):
            val = tuple(val)
        elif isinstance(val, dict):
            val = make_serializable(val)
        _info[key] = val

    return _info

def format_html_attrs(attrs):
    # We can only use a subset of html:
    # https://github.com/plotly/plotly.js/blob/master/src/components/fx/hover.js#L1850
    summary = ""
    for key, val in attrs.items():
        # WARNING: Possible XSS
        # Users + plans are mostly trusted
        # Plotly only supports *some* html and santizies the rest,
        #   so we can't just escape everything
        summary += f"<br><br><em>{key}:</em><br>{val}"
    return summary

def get_temp_dir(prefix=None):
    return tempfile.mkdtemp(prefix=prefix)

def get_certs(*urls):
    res = {}
    for url in urls:
        if url.startswith("http://") or url.startswith("https://"):
            # Assume it is a step-ca url, e.g. https://ca.example.com:9000/roots
            if not url.endswith("roots") or url.endswith("roots/"):
                url = os.path.join(url, "roots")
            base_name = urllib.parse.urlparse(url).hostname
            resp = requests.get(url, verify=False)
            certs = resp.json()["crts"]
            if certs:
                # Uniquely  name all the certs
                if len(certs) > 1:
                    for i, cert in enumerate(certs):
                        res[f"{base_name}.{i}"] = cert
                else:
                   res[base_name] = certs[0]
        else:
            #  Assume it is a file
            file_proto = "file://"
            if url.startswith(file_proto):
                url = url[len(file_proto):]
            file_path = Path(url)
            cert_name = file_path.name
            if cert_name in res:
                # Name conflict; force unique, (hopefully descriptive) names
                raise ValueError(
                    f"Duplicate cert name {cert_name} !"\
                    " Cert names should be unique and descriptive!"
                )
            cert = file_path.read_text()
            res[cert_name] = cert
    return res

def write_topology(data, output_dir):
    """Write a directory topology (dict) to a real directory

    {
        "foo": "Foo Contents",              # file
        "bar_dir":{                         # sub directory
            "bar_file": "Bar contents"      #file
        },
        "foobar": "file:///path/to/foobar"  # contents read from another file
    }
    """
    for key, val in data.items():
        sub_path = Path(output_dir) / key
        if isinstance(val, dict):
            sub_path.mkdir(mode=0o755, parents=True, exist_ok=True)
            write_topology(val, str(sub_path))
        else:
            file_proto = "file://"
            if val.startswith(file_proto):
                # Assume user wants to copy the contents of another file
                src_path = file_proto[len(file_proto):]
                shutil.copyfile(src_path, str(sub_path))
            else:
                sub_path.write_text(val)

def archive(src_dir, output_file=None, fmt="gztar", prefix=None):
    """Archive a directory to a file

    Returns a str file name (if output_file was given)

    Else, a tempfile is created and returned
    Caller must delete the tempfile by calling ret_val.close()
    """
    output_file_path = output_file
    if output_file is None:
        output_file = tempfile.NamedTemporaryFile(prefix=prefix)
        output_file_path = output_file.name
    # Assert ouput_file_path is a str
    # Assert ouput_file is a str or a NamedTemporaryFile

    path = shutil.make_archive(output_file_path, fmt, src_dir)

    # shutil added an extension instead of writing to this file,
    #   so just move the created file to the tempfile
    shutil.copyfile(path, output_file_path)
    os.remove(path)

    # If output_file was originally None, caller must close it!
    return output_file
