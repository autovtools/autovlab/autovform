import math
from ipaddress import IPv4Address, IPv4Network

def subnets_to_bits(num_subnets):
    """
    Converts the number of subnets required to the number of network id bits
        needed to represent them.

    Mathematically, returns the smallest power of 2 >= num_subnets.
    """
    if num_subnets == 0:
        return 0
    return math.ceil(math.log2(num_subnets))

def to_network(network):
    if isinstance(network, str):
        network = IPv4Network(network)
    return network
    
def subnets(network, num_subnets):
    network = to_network(network)

    num_bits = subnets_to_bits(num_subnets)
    return network.subnets(prefixlen_diff=num_bits)
    
