def validate_opts(opts, config, ignore=[], fatal=True):
    missing_required = []
    unknown = []

    for k, v in opts.items():
        if v["required"] and not k in config:
            missing_required.append(k)
        elif not v["required"] and not k in config:
            # Apply defaults
            if "default" in v:
                config[k] = v['default']
            else:
                config[k] = None

    for k, v in config.items():
        if k in ignore:
            continue
        if not k in opts:
            unknown.append(k)

    msg = ''
    if missing_required:
        msg += f"Missing required options: {missing_required}\n"
    if unknown:
        msg += f"Got unknown options: {unknown}"

    if msg and fatal:
        raise ValueError(msg)
    elif not fatal:
        # fatal=False returns a (possible) warning message
        return missing_required, unknown, msg

    # fatal=True doesn't require a warning message
    return missing_required, unknown

def check_mutually_exclusive(config, args, required=1):
    # True iff exactly 'reuired' many options in the set were given 
    return sum([(config.get(key, None) is not None) for key in args]) == required
