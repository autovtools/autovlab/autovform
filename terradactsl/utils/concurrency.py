"""
Abstraction layer for concurrency operations
    in case gevent is later swapped for a different library
"""
import os
import shutil
import shlex
import logging
import time
from contextlib import contextmanager

import gevent
from gevent import sleep, subprocess
from gevent.pool import Pool

def spawn(target, *args, **kwargs):
    return gevent.spawn(target, *args, **kwargs)

def killall(*args, **kwargs):
    return gevent.killall(*args, **kwargs)

def get_pool(num_workers=None):
    return Pool(num_workers)

def add_to_pool(pool, target, *args, **kwargs):
    return pool.spawn(target, *args, **kwargs)

def start_pool(pool, logger):
    # Yield to the pool
    try:
        gevent.joinall(pool)
        return True
    except KeyboardInterrupt:
        logger.warning("Stopping workers...")
        pool.kill()
        return False

def terraform_init(plugin_dir=None, project_dir=None):
    cmd = [
        "terraform",
        "init",
    ]
    if plugin_dir:
        cmd.append(f"-plugin-dir={plugin_dir}")

    if project_dir:
        cmd.append(project_dir)

    return cmd

def terraform_plan(var_files=[], project_dir=None):
    cmd = [
        "terraform",
        "plan",
    ]
    if var_files: 
        for var_file in var_files:
            cmd.append(f"-var-file={var_file}")

    if project_dir:
        cmd.append(project_dir)

    return cmd

def terraform_op(operation, project_dir=None, var_files=[], auto_approve=False):
    if operation not in ["plan", "apply", "destroy"]:
        raise ValueError(f"Invalid operation: {operation}")

    terraform = shutil.which("terraform")
    if not terraform:
        raise ValueError("Cannot find terraform in PATH!")

    cmd = [
        "terraform",
        operation,
    ]
    if operation != "plan" and auto_approve:
        cmd.append(f"--auto-approve")

    for var_file in var_files:
        cmd.append(f"-var-file={var_file}")
    if project_dir:
        cmd.append(project_dir)

    return cmd

def print_cmd(cmd, logger):
    cmd_str = " ".join(
        [shlex.quote(tok) for tok in cmd]
    )
    banner_len = max(len(cmd_str) + 3, 80)
    banner = "#"*banner_len
    header = f"""
{banner}
{cmd_str}
{banner}
    """
    logger.info(header)

def wait_proc(proc, greenlets=[], logger=None):
    if not logger:
        logger = logging.getLogger()
    # Seconds to wait for a cancelled build to self-destroy
    kill_timeout = 60*30
    ret = None
    try:
        return proc.wait()
    except KeyboardInterrupt:
        logger.warning("Signaling subprocess to exit...")
        # Terraform actually sees the Ctrl+C
        #proc.terminate()
        try:
            proc.wait(kill_timeout)
        except KeyboardInterrupt:
            # Give user one more chance to be patient
            logger.error("[WARNING] Forcing Terraform to exit may leave resources orphaned, requiring manual deletion!")
            logger.warning("Waiting for processes to exit cleanly...")
            proc.wait(kill_timeout)

    except subprocess.TimeoutExpired:
        logger.error("[WARNING] TimeoutExpired. Some resources may be orphaned, requiring manual deletion!")
        proc.kill()

    if proc.poll() is None:
        logger.error("[WARNING] Foricibly killing subprocess")
        proc.kill()

    if greenlets:
        logger.error("[WARNING] Stopping any remaining greenlets...")
        gevent.killall(greenlets)

    return proc.poll()
    

@contextmanager
def pushd(path):
    oldpwd=os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(oldpwd)

def invoke_terraform(operation, project_tf, var_files=None, plugin_dir=None, auto_approve=False, logger=None, helpers=[]):
    # https://learn.hashicorp.com/terraform/development/running-terraform-in-automation
    env = {
        "TF_IN_AUTOMATION":"1"
    }
    if not logger:
        logger = logging.getLogger()

    project_dir = os.path.abspath(os.path.dirname(project_tf))
    logger.info(f"Project directory: {project_dir}")

    # Absolute-ify all the paths before we change directory
    var_files = [os.path.abspath(var_file) for var_file in var_files]
    plugin_dir = os.path.abspath(plugin_dir)

    with pushd(project_dir):
        cmd = terraform_init(
                plugin_dir=plugin_dir,
                project_dir=project_dir
        )
        return_code = run_with_logger(cmd, logger, env=env)
        if return_code:
            logger.warning(f"Init failed with exit code: {return_code}")
            return False

        cmd = terraform_op(
            operation,
            var_files=var_files,
            auto_approve=auto_approve,
            project_dir=project_dir
        )

        return run_with_logger(cmd, logger, env=env, block=False)

def log_pipe(pipe, logger, level):
    with pipe:
        for line in iter(pipe.readline, b""):
            logger.log(level, line.decode().rstrip('\n'))

def run_with_logger(cmd, logger, env=None, stdout_level=logging.INFO, stderr_level=logging.ERROR, block=True):
    """
    Run the subproccess command (given by proc_list) to completion, logging output to logger
        with the specified logging levels
    """

    if env:
        new_env = os.environ.copy()
        new_env.update(env)
        env = new_env

    print_cmd(cmd, logger)

    proc = subprocess.Popen(
        cmd,
        env=env,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )

    out_logger = spawn(log_pipe, proc.stdout, logger, stdout_level)
    err_logger = spawn(log_pipe, proc.stderr, logger, stderr_level)
    greenlets = [out_logger, err_logger]
    if block:
        return wait_proc(proc, greenlets=greenlets, logger=logger)
    else:
        return proc, greenlets

