import io
import random

import pkgutil
from frozendict import frozendict

import plotly.graph_objects as go
import networkx as nx
from PIL import Image

from .io import format_html_attrs

class VizNode(dict):
    """
    A hashable, JSON serializable dict
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__frozen__ = frozendict(self)
    def __hash__(self):
        return hash(self.__frozen__)
    def __eq__(self, other):
        return hash(self) == hash(other)

def _project_viz(viz, graph, parent=None):
    ignore = [None, "__vms__", "__fw__"]
    for node_info, sub_graph in graph.items():
        if node_info in ignore:
            continue
        if None in sub_graph:
            # node is a zone; make name unique
            node_info = sub_graph[None]

        if "__fw__" in sub_graph and sub_graph["__fw__"]:
            # Let the firewall repesent the "project" node
            node_info = sub_graph["__fw__"]

        if isinstance(node_info, dict):
            node_info = VizNode(node_info)

        viz.add_node(node_info)
        if parent:
            viz.add_edge(parent, node_info)
            
        if "__vms__" in sub_graph:
            # Direct connection
            for vm in sub_graph["__vms__"]:
                if isinstance(vm, dict):
                    vm = VizNode(vm)
                viz.add_node(vm)
                viz.add_edge(node_info, vm)
        if isinstance(sub_graph, dict):
            _project_viz(viz, sub_graph, parent=node_info)

def project_viz(project):
    viz_list = []
    # If we have mutiple instances of the root project,
    #   split them into separate graphs
    # (We have a forest; draw each tree separately)
    for root_project, sub_graph in project.project_graph.items():
        #init_notebook_mode(connected=True)
        viz = nx.DiGraph()
        _project_viz(viz, {root_project: sub_graph})
        viz_list.append(viz)
    return viz_list

def get_viz_details(project):
    zones = project.get_zones()
    num_zones = sum(len(zone.instances) for zone in zones)
    vms = project.get_vms()
    num_vms = sum(len(vm.instances) for vm in vms)
    attrs = {
        "Projects": len(project.project_map),
        "Zones": num_zones,
        "VMs":num_vms 
    }
    return format_html_attrs(attrs)
    
def render_viz(project, viz_list):
    fig_list = []

    # TODO: Tweak based on user resolution
    config = {
      'scrollZoom': "cartesian",
      'toImageButtonOptions': {
        'format': 'svg', # one of png, svg, jpeg, webp
        'filename': project.name,
        'height': 2160,
        'width': 3840,
        'displayLogo': False,
        'scale': 1 # Multiply title/legend/axis/canvas sizes by this factor
      }
    }
    for project_name, viz in zip(project.instance_names, viz_list):
        pos = hierarchy_pos(viz)
        edge_trace = create_edges(viz, pos)
        node_trace = create_nodes(viz, pos)
        details = get_viz_details(project)
        fig = create_fig(
            viz,
            pos,
            edge_trace,
            node_trace,
            title=project_name,
            details=details
        )
        fig_list.append(fig)
        fig.show(config=config)
        #plot(fig)
    return fig_list

def create_edges(viz, pos):
    # https://plotly.com/python/network-graphs/
    edge_x = []
    edge_y = []
    for parent, child in viz.edges():
        x0, y0 = pos[parent]
        x1, y1 = pos[child]
        edge_x.append(x0)
        edge_x.append(x1)
        edge_x.append(None)
        edge_y.append(y0)
        edge_y.append(y1)
        edge_y.append(None)
        

    edge_trace = go.Scatter(
        x=edge_x,
        y=edge_y,
        line=dict(width=0.5, color='#888'),
        hoverinfo='none',
        mode='lines+text')

    return edge_trace

def create_nodes(viz, pos):
    hover_template = "%{text}"
    node_x = []
    node_y = []
    for node in viz.nodes():
        x, y = pos[node]
        node_x.append(x)
        node_y.append(y)

    summaries = [ node["summary"].replace("\n", "\n<br>") for node in viz.nodes() ]
    node_trace = go.Scatter(
        x=node_x, y=node_y,
        name="",
        mode='markers',
        text=summaries,
        hovertemplate=hover_template,
        hoverlabel=dict(
            bgcolor="#0f0f0f"
        ),
        opacity=.25,
    )
    
    return node_trace

def _get_icon(img_name, ext=".png"):
    return pkgutil.get_data("terradactsl.img", img_name + ext)

def get_icon(node):
    img_bytes = None
    if "zone_name" in node:
        img_bytes = _get_icon("zone")
    else:
        os_family = node["os_flavor"].lower()
        img_bytes = _get_icon(os_family)

    return Image.open(io.BytesIO(img_bytes))

def get_sizes(pos):
    font_size = 20
    marker_size = 120
    img_size = 0.3
    annotation_len = 140
    # Use node count to scale down sizes
    #   (best effort, targeting 4k)
    count = len(pos)
    if count <= 20:
        pass
    elif count > 20 and count <= 30:
        font_size = 18
        marker_size = 100
        img_size *= 0.8
        annotation_len = 120
    elif count > 30 and count <= 40:
        font_size = 16
        marker_size = 80
        img_size *= 0.5
        annotation_len = 110
    elif count > 40 and count <= 60:
        font_size = 14
        marker_size = 60
        img_size *= 0.4
        annotation_len = 100
    elif count > 60 and count <= 80:
        font_size = 12
        marker_size = 40
        img_size *= 0.3
        annotation_len = 90
    elif count > 80 and count <= 100:
        font_size = 12
        marker_size = 20
        img_size *= 0.2
        annotation_len = 80
    elif count > 100:
        font_size = 12
        marker_size = 10
        img_size *= 0.1
        annotation_len = 70

    #print(count)
    #print(font_size, marker_size, img_size)
    return font_size, marker_size, img_size, annotation_len

def create_fig(viz, pos, edge_trace, node_trace, title="", details=""):
    font_size, marker_size, img_size, annotation_len = get_sizes(pos)
    
    fig = go.Figure(
        data=[edge_trace, node_trace],
        layout=go.Layout(
            title=dict(
                text=title,
                font=dict(size=32),
                xref="paper",
                yref="paper",
                x=0,
                y=1,
            ),
            dragmode="pan",
            hovermode='closest',
            paper_bgcolor=("#0f0f0f"),
            plot_bgcolor=("#0f0f0f"),
            autosize=True,
            margin=dict(
                l=0, r=0, b=0, t=0, pad=0
            ),
            font = dict(
                family = "Courier New, monospace",
                size = font_size,
            ),
            hoverlabel=dict(
                font_family = "Courier New, monospace",
                font_size=font_size
            ),
            annotations=[
                dict(
                    text=details,
                    font = dict(
                        size=20
                    ),
                    showarrow=False,
                    xref="paper",
                    yref="paper",
                    x=0,
                    y=0.97,
                ) 
            ],
            xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
            yaxis=dict(showgrid=False, zeroline=False, showticklabels=False)
        ),
    
    )
    fig.update_traces(
        marker=dict(
            size=marker_size,
            color="#d4d4d4"
        ), 
        selector=dict(mode='markers')
    )
    for node_info, coords in pos.items():
        fig.add_layout_image(
            dict(
                source=get_icon(node_info),
                x=coords[0],
                y=coords[1],
            )
        )
        text = None
        if "fqdn" in node_info:
            # VM node
            text = f'<b>{node_info["fqdn"]}</b>'
            ip_addr = node_info["ipv4_address"]
            if isinstance(ip_addr, str):
                text += f"<br>{ip_addr}"
        else:
            # Zone node
            text = node_info["zone_name"]
            text += "<br>" + node_info["zone_network"]

        fig.add_annotation(
            x=coords[0],
            y=coords[1],
            text=text,
            xref="x",
            yref="y",
            ax=0,
            ay=annotation_len,
            bgcolor="#0f0f0f"
        )

    
    # Add gateway labels to the edges between firewall and zones
    for parent, child in viz.edges():
        label = None
        if "zone_gateway" in child:
            # fw -> zone edge
            label = child["zone_gateway"]
        elif "zone_gateway" in parent:
            # zone -> vm edge
            if not isinstance(child["ipv4_address"], str):
                # It's a firewall; WAN ip is first
                label = child["ipv4_address"][0]
            else:
                # Regular VMs are already labeled with IP address
                continue
        else:
            continue
        
        px, py = pos[parent]
        cx, cy = pos[child]
        ax = (px + cx) / 2
        ay = (py + cy) / 2
        
        fig.add_annotation(
            x=(px + cx) / 2,
            y=(py + cy) / 2,
            text=label
        )

    
    fig.update_layout_images(dict(
        sizex=img_size,
        sizey=img_size,
        xref="x",
        yref="y",
        xanchor="center",
        yanchor="middle",
        sizing="contain"
    ))
    fig.update_layout(
        template="plotly_dark",
        showlegend=False,
    )

    return fig

# Adapted From:
# https://epidemicsonnetworks.readthedocs.io/en/latest/_modules/EoN/auxiliary.html#hierarchy_pos
# Other References:
#   https://stackoverflow.com/a/11484144
#   https://github.com/pygraphviz/pygraphviz/issues/56
def hierarchy_pos(G, root=None, width=4., vert_gap = 1, vert_loc = 0, leaf_vs_root_factor = 1, jitter = 0.04):

    '''
    If the graph is a tree this will return the positions to plot this in a 
    hierarchical layout.
    
    Based on Joel's answer at https://stackoverflow.com/a/29597209/2966723,
    but with some modifications.  

    We include this because it may be useful for plotting transmission trees,
    and there is currently no networkx equivalent (though it may be coming soon).
    
    There are two basic approaches we think of to allocate the horizontal 
    location of a node.  
    
    - Top down: we allocate horizontal space to a node.  Then its ``k`` 
      descendants split up that horizontal space equally.  This tends to result
      in overlapping nodes when some have many descendants.
    - Bottom up: we allocate horizontal space to each leaf node.  A node at a 
      higher level gets the entire space allocated to its descendant leaves.
      Based on this, leaf nodes at higher levels get the same space as leaf
      nodes very deep in the tree.  
      
    We use use both of these approaches simultaneously with ``leaf_vs_root_factor`` 
    determining how much of the horizontal space is based on the bottom up 
    or top down approaches.  ``0`` gives pure bottom up, while 1 gives pure top
    down.   
    
    
    :Arguments: 
    
    **G** the graph (must be a tree)

    **root** the root node of the tree 
    - if the tree is directed and this is not given, the root will be found and used
    - if the tree is directed and this is given, then the positions will be 
      just for the descendants of this node.
    - if the tree is undirected and not given, then a random choice will be used.

    **width** horizontal space allocated for this branch - avoids overlap with other branches

    **vert_gap** gap between levels of hierarchy

    **vert_loc** vertical location of root
    
    **leaf_vs_root_factor**

    xcenter: horizontal location of root
    '''
    global LEAF_JITTER
    if not nx.is_tree(G):
        raise TypeError('cannot use hierarchy_pos on a graph that is not a tree')

    if root is None:
        if isinstance(G, nx.DiGraph):
            root = next(iter(nx.topological_sort(G)))  #allows back compatibility with nx version 1.11
        else:
            root = random.choice(list(G.nodes))

    LEAF_JITTER = jitter
    def _hierarchy_pos(G, root, leftmost, width, leafdx = 0.1, vert_gap = 0.5, vert_loc = 0, 
                    xcenter = 0.5, rootpos = None, 
                    leafpos = None, parent = None):
        '''
        see hierarchy_pos docstring for most arguments

        pos: a dict saying where all nodes go if they have been assigned
        parent: parent of this branch. - only affects it if non-directed

        '''
        global LEAF_JITTER

        if rootpos is None:
            rootpos = {root:(xcenter,vert_loc)}
        else:
            rootpos[root] = (xcenter, vert_loc)
        if leafpos is None:
            leafpos = {}

        children = list(G.neighbors(root))
        leaf_count = 0
        if not isinstance(G, nx.DiGraph) and parent is not None:
            children.remove(parent)  
        if len(children)!=0:
            rootdx = width/len(children)
            nextx = xcenter - width/2 - rootdx/2

            for child in children:
                _vert_gap = vert_gap
                if "zone_name" in child:
                    # Child is a zone
                    _vert_gap = vert_gap/4
                nextx += rootdx
                rootpos, leafpos, newleaves = _hierarchy_pos(G,child, leftmost+leaf_count*leafdx, 
                                    width=rootdx, leafdx=leafdx,
                                    vert_gap = vert_gap, vert_loc = vert_loc - _vert_gap, 
                                    xcenter=nextx, rootpos=rootpos, leafpos=leafpos, parent=root)
                leaf_count += newleaves

            leftmostchild = min((x for x,y in [leafpos[child] for child in children]))
            rightmostchild = max((x for x,y in [leafpos[child] for child in children]))
            leafpos[root] = ((leftmostchild+rightmostchild)/2, vert_loc)
        else:
            LEAF_JITTER = -1*LEAF_JITTER
            leaf_count = 1
            leafpos[root]  = (leftmost, vert_loc + LEAF_JITTER)
#        pos[root] = (leftmost + (leaf_count-1)*dx/2., vert_loc)
#        print(leaf_count)
        return rootpos, leafpos, leaf_count

    xcenter = width/2.
    if isinstance(G, nx.DiGraph):
        leafcount = len([node for node in nx.descendants(G, root) if G.out_degree(node)==0])
    elif isinstance(G, nx.Graph):
        leafcount = len([node for node in nx.node_connected_component(G, root) if G.degree(node)==1 and node != root])

    rootpos, leafpos, leaf_count = _hierarchy_pos(G, root, 0, width, 
                                                    leafdx=width*1./leafcount, 
                                                    vert_gap=vert_gap, 
                                                    vert_loc=vert_loc, 
                                                    xcenter=xcenter)
    pos = {}
    for node in rootpos:
        pos[node] = (leaf_vs_root_factor*leafpos[node][0] + (1-leaf_vs_root_factor)*rootpos[node][0], leafpos[node][1]) 
#    pos = {node:(leaf_vs_root_factor*x1+(1-leaf_vs_root_factor)*x2, y1) for ((x1,y1), (x2,y2)) in (leafpos[node], rootpos[node]) for node in rootpos}
    xmax = max(x for x,y in pos.values())
    for node in pos:
        pos[node]= (pos[node][0]*width/xmax, pos[node][1])
    return pos
