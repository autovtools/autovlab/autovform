import string

def suffix_join(*args, delim="-", prefix=""):
    args = [arg for arg in args if arg]
    return prefix + delim.join(args)

def zero_pad(num, zfill=2):
    return str(num).zfill(zfill)

def get_instance_suffix(curr_instance, first_instance=1):
    if curr_instance is None:
        return ""
    curr = first_instance + curr_instance
    return zero_pad(curr)

def get_terraform_attr(resource_name, attr, type_prefix=""):
    if type_prefix:
        resource_name = f"{type_prefix}.{resource_name}"
    return "${" + f"{resource_name}.{attr}" + "}"

def get_terraform_resource(expression):
    res = expression
    if expression.startswith("${") and expression.endswith("}"):
        res = expression[2:-1]
        tokens = res.split(".")
        # we only want type.resource_name ; drop any attrs after
        res = ".".join(tokens[:2])
    return res

def _sanitize_name(name, allowed=string.ascii_letters + string.digits, replace="-", case=None):
    # Be paranoid, replace any illegal characters
    for char in list(name):
        if not char in allowed:
            name = name.replace(char, replace)

    if not case:
        return name

    if case.lower() == "lower":
        return name.lower()
    elif case.lower() == "upper":
        return name.upper()


def sanitize_host_name(host_name):
    allowed = string.ascii_letters + string.digits +  "-"
    return _sanitize_name(host_name, allowed=allowed, replace="-", case="lower")

def sanitize_domain_name(domain):
    # TODO: Squash Windows domain names
    allowed = string.ascii_letters + string.digits +  "-." 
    return _sanitize_name(domain, allowed=allowed, replace="-", case="lower")

def get_resource_name(name, prefix="", curr_instance=None):
    """
    Returns a valid resource identifer for this object, derived from the name.

    See https://www.terraform.io/docs/configuration/syntax.html#identifiers

    """
    inst = get_instance_suffix(curr_instance)
    resource_name = f"{prefix}{name}"
    if inst:
        resource_name += inst

    allowed = string.ascii_letters + string.digits +  "-_"
    return _sanitize_name(resource_name, allowed=allowed, replace="-")

def get_port_group_name(zone, prefix="dvPG", delim="_", delim2=".", delim3="-"):
    project_name = zone.project.name
    tokens = project_name.split(delim)

    group_name = zone.project.config["group_name"] 
    instance_name = zone.config["instance_name"] 
    pg_name = delim.join(
        [
            group_name,
            project_name,
        ]
    )
    pg_name = delim2.join(
        [
            pg_name,
            instance_name
        ]
    )
    if prefix:
        pg_name = delim3.join([prefix, pg_name])

    return pg_name

def get_zone_instance_name(zone, curr_instance):
    inst = get_instance_suffix(curr_instance, zone.config["first_instance"])
    res = zone.config['zone_name']
    if inst:
        res += f"-Z{inst}"
    return res

def get_instance_name(prefix, curr_instance, first_instance=0):
    inst = get_instance_suffix(curr_instance, + first_instance)
    if inst:
        prefix += f"-{inst}"
    return prefix
"""
def get_project_domain_name(project, curr_instance):
    inst = get_instance_suffix(curr_instance, project.config["first_instance"])
    res = project.config["domain"]
    if inst:
        res += f"-{inst}"
    return res
"""
 
    
def get_fw_vsphere_name(infra, fqdn):
    group_name = infra.project.config["group_name"]
    return _get_vsphere_name(group_name, fqdn)

def get_vm_vsphere_name(vm, delim="_", delim2="."):
    group_name = vm.project.config["group_name"]
    return _get_vsphere_name(group_name, vm.config["fqdn"])

def _get_vsphere_name(group_name, fqdn, delim="_", delim2="."):
    fqdn_tokens = fqdn.split(".")[::-1]
    vsphere_name = delim2.join(fqdn_tokens)
    return delim.join([group_name, vsphere_name])

def get_vapp_name(obj, delim="_", delim2=".", prefix=""):
    group_name = obj.config["group_name"]
    right = suffix_join(prefix, obj.name, delim=delim2)
    return suffix_join(group_name, right, delim=delim)

def get_domain_name(project):
    #domain_name = project.name_tokens()
    curr_project = project
    domain = ""
    while curr_project:
        tokens = curr_project.get_name_tokens(-1)
        base_domain = suffix_join(curr_project._base_name, *tokens)
        domain = f"{domain}.{base_domain}"
        project.logger.info(domain)
        curr_project = curr_project.project

    project.logger.warning(domain)
    return domain[1:]
    
