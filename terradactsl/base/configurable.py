import os
import json
import logging
import string
import itertools

from terradactsl import utils as utils 

class Configurable():
    opts = {}
    default_key = None
    unknown_key = None
    unknown_opts = None
    attributes = []
    ignore_keys = []
    # Needed  so __getattr__ override works before self.config is ready
    config = None

    def __init__(self, config=None, name=None, is_file=False, log_level=None, log_file=None, show_line_info=False):
        self.name = None
        if not hasattr(self, "_base_name"):
            self._base_name = name
        if config is None:
            config = {}

        logger_name = self.__class__.__name__
        obj_name = f"{logger_name.lower()}_name"
        if name is not None:
            self.name = name
        else:
            if isinstance(config, dict) and obj_name in config:
                self.name = config[obj_name]

        self.base_name = self.name

        self.log_level = log_level
        self.log_file = log_file
        self.show_line_info = show_line_info

        if log_level is None:
            log_level = logging.INFO

        # Use detailed name for logging instead of the class name
        self.logger = utils.get_logger(
            name=self.name,
            log_level=log_level,
            log_file=log_file,
            show_line_info=self.show_line_info
        )

        self.project = None
        self.root_project = None
        self.resources = []
        self.dependencies= []
        self.instances = []
        self.instance_resources = []
        self.instance_dependencies = []
        self.curr_instance = None
        self.render_index = -1

        self._summary = ""

        if is_file:
            self.var_file = os.path.abspath(config)
            self.logger.debug(f"Loading config from {config}...")
            config = utils.load_config(config)

        self.config = config
        self.normalize_config()
        # Config is normalized - do strict validation (exit on error)
        utils.validate_opts(self.opts, self.config, ignore=self.ignore_keys)

        self._instance_names = self.get_instance_name_generator()
        self.instance_names = []
        self.instance_tokens = []

    def __getattr__(self, name):
        # Allow DSL-like access to child objects
        #   e.g. my_project.MYZone.MyVM
        if not self.attributes or self.config is None:
            # Fail
            return self.__getattribute__(name)
        # If __getattr__ is called, the attribute isn't 'real'
        # Attempt to find the requested attribute in any of self.attributes
        for attr_name in self.attributes:
            attr = getattr(self, attr_name)
            if name in attr:
                return attr[name]

        # Fail
        return self.__getattribute__(name)

    def __dir__(self):
        # Allow interactive tabcomplete for "fake" attributes
        res = dir(self.__class__)
        for attr_name in self.attributes:
            attr = getattr(self, attr_name)
            res.extend(list(attr))
        return res

    def wrap_curr_instance(self, curr, denom=None):
        num_instances = self.config["num_instances"]
        if num_instances is None:
            curr = 0
        else:
            if denom is None:
                denom = num_instances
            curr = int(curr / denom) % num_instances
        return curr 

    def get_instance_name_generator(self):
        instance_names = self.config.get("instance_names", None)
        if isinstance(instance_names, list):
            return utils.list_to_gen(instance_names)
        elif isinstance(instance_names, str):
            if instance_names == "alphabetic":
                # Alphabetic: A - ZZ
                alpha = string.ascii_uppercase
                alpha2 = [
                    "".join(prod)
                    for prod in itertools.product(alpha, alpha)
                ]
                alpha = list(alpha)
                alpha.extend(alpha2)
                return utils.list_to_gen(alpha)
            elif instance_names == "none":
                name = ""
                return utils.list_to_gen([name]*1000)
        elif self.config.get("num_instances", -1) is None:
            # There is only one instance
            return ("" for _ in range(1))
        else:
            # Numeric: 1 - 999
            return (
                utils.get_instance_suffix(curr, self.config["first_instance"])
                for curr in range(1000)
            )


    def reset_instance_names(self):
        self._instance_names = self.get_instance_name_generator()

    def _next_instance_name(self, base, inst_list):
        inst_tokens = next(self._instance_names)
        self.instance_tokens.append(inst_tokens)
        inst_list.append(inst_tokens)
        return base

    def next_instance_name(self, delim='-'):
        base = self.base_name
        if not base:
            return None
        inst_list = []
        try:
            base = self._next_instance_name(base, inst_list)
        except StopIteration:
            self.reset_instance_names()
            base = self._next_instance_name(base, inst_list)

        """
        if self.project:
            # Turn everything from above us into a prefix
            for tok in self.project.get_name_tokens(None):
                inst_list.insert(0, (tok[0], True))
        """
        
        for inst in inst_list:
            tokens = [base, inst]
            """
            if inst[1]:
                # Prefix is True
                tokens[0], tokens[1] = tokens[1], tokens[0]
            """
            #self.logger.info(tokens)
            base = utils.suffix_join(*tokens)

        self.instance_names.append(base)
        return base

    @staticmethod
    def apply_default(config, key, val):
        if config.get(key, None) is None:
            config[key] = val

    def _merge_unknown(self, conf):
        if self.unknown_key in self.config:
            # Merge into the existing object (existing has priority)
            existing = self.config[self.unknown_key]
            if existing:
                conf.update(existing)

        self.config[self.unknown_key] = conf

    def normalize_config(self):
        """
        Support alternate config syntax / shorthand to make configs simpler and cleaner.

        __default_key__
        If there is a default_key, a non-dict config is the value of that key
        Ex) "my-vm-name":"MyTemplate"
            might map to
            "my-vm-name:{
                "template":"MyTemplate"
            }

        __unknown_key__
        If there is an unknown_key, a non-dict config is the value of that key
        Ex) "my-vm-name":"MyTemplate"
            might map to
            "my-vm-name:{
                "template":"MyTemplate"
            }
        """
        #  default_key Normalization
        if not isinstance(self.config, dict):
            if self.default_key:
                conf = {
                    self.default_key:self.config
                }
                self.config = conf
            else:
                # This could be user error (invalid key)
                # OR it could be nested unknown keys
                # (e.g. specifying a VM at the Project level)

                    
                if self.unknown_key:
                    name_key = f"{self.__class__.__name__.lower()}_name"
                    name = self.opts["default_name"]["default"]
                    self.config = {
                        name_key : name,
                        self.unknown_key : {
                            self.name: self.config
                        }
                    }
                    self.name = name
                    self.base_name = name
                    
                else:
                    # This is a pretty likely user error, so try to be helpful
                    msg = f"Unexpected type where config expected ({type(self.config)})."
                    if type(self.config) in [str, bool, int]:
                        msg += f" This could happen if '{self.name}' was used as an option in a block that does not support it."
                    self.logger.error(msg)
                    raise ValueError(msg)
        
        # Now we should have a dict
        # We were probably constructed with a passed name;
        #   push that name into the config dict

        # Child classes might pretend to be their parent
        #   e.g. an UplinkZone is a Zone and needs a zone_name key
        # Search our class and parent classes for the right X_name key
        classes = [self.__class__]
        classes.extend(self.__class__.__bases__)
        for class_obj in classes:
            name_key = f"{class_obj.__name__.lower()}_name"
            if name_key in self.opts:
                # We found the right key
                if not name_key in self.config:
                    self.config[name_key] = self.name
                break

        # unknown_key Normalization
        # First pass: do config normalization before strict validation
        missing_required, unknown, warning = utils.validate_opts(
            self.opts,
            self.config,
            ignore=self.ignore_keys,
            fatal=False
        )
        self.logger.debug(warning)
        if unknown and self.unknown_key:
            # Unkown options are stored in unknown_key
            # This allows for simpler / cleaner configs
            #   with less indentation
            
            conf = {}
            if self.unknown_opts:
                # Collect any options that belong to a single child object
                # This allows giving multile VM options at once without
                #   interpreting them as multiple VMs
                for key in unknown[:]:
                    if key in self.unknown_opts:    #pylint: disable=unsupported-membership-test
                        conf[key] = self.config[key]
                        del self.config[key]
                        unknown.remove(key)
                if conf:
                    self.logger.info(f"Unrecognized options {list(conf)} interpreted as a {self.unknown_key} named {self.name}")
                    conf = {self.name: conf}
                    self._merge_unknown(conf)

                    name = self.opts["default_name"]["default"]
                    self.logger.info(f"Since {self.name} is really a {self.unknown_key}: renaming {self.name} -> {name}")
                    self.name = name

                    conf = {}

            for key in unknown:
                if key in self.config:
                    conf[key] = self.config[key]
                    del self.config[key]
    
            if unknown:
                # There are still unknown keys; treat them as multiple objects
                self.logger.info(f"Unrecognized options {unknown} interpreted as {self.unknown_key}")
                self._merge_unknown(conf)


        self.logger.debug(
            "Normalized Config:\n" +
            utils.sanitize_config(self.config)
        )

    def inherit(self, config_keys, other, force=False):
        """
        Inherit config attributes from another Configurable.

        config_keys: list of attriutes to inherit
        other:       Configurable to inherit from
        """
        for key in config_keys:
            if force or self.config.get(key, None) is None:
                self.config[key] = other.config[key]


    def inherit_logger_settings(self, project, kwargs):
        if not "log_level" in kwargs:
            kwargs["log_level"] = project.log_level

        if not "log_file" in kwargs:
            kwargs["log_file"] = project.log_file

        if not "show_line_info" in kwargs:
            kwargs["show_line_info"] = project.show_line_info


    def init_logger(self, default=logging.INFO):
        # Start with the log_level used in construction
        # (If user explictily passed something, use that)
        log_level = self.log_level

        if log_level is None:
            # Else, use the config
            # Last resort: use default
            log_level = self.config.get("log_level", default)

        if isinstance(log_level, str):
            log_level = logging.getLevelName(log_level)

        self.logger.setLevel(log_level)
        self.config["log_level"] = log_level

        self.log_level = log_level

        """
        def get_suffix(self):
            suffix = utils.get_instance_suffix(
                self.curr_instance,
                self.config.get("first_instance", 1)
            )
            self.logger.warning(f"{self.name} has suffix: {suffix}")
            return suffix
        """

    def get_name_tokens(self, curr):
        tokens = []
        if self.project:
            tokens.extend(
                self.project.get_name_tokens(curr)
            )
        if curr is None:
            curr = -1
        self.logger.debug(self.instance_tokens)
        tokens.append(self.instance_tokens[curr])
        return tokens

    def depends_on(self, other, exclude=[]):
        self.logger.debug(f"Depending on {other.resources}")
        #deps = [ "${" + exp + "}" for exp in other.resources]
        deps = other.resources
        deps = [dep for dep in deps if not any(dep.startswith(exc) for exc in exclude)]
        self.dependencies.extend(deps)

    def instance_depends_on(self, other, exclude=[]):
        #self.logger.debug(f"Instance Dependency: {self.name} => {other.instance_resources}")
        #deps = [ "${" + exp + "}" for exp in other.resources]
        for index, resources in enumerate(other.instance_resources):
            resources = [resc for resc in resources if not any(resc.startswith(exc) for exc in exclude)]
            # TODO: Verify
            #index = other.wrap_curr_instance(index)
            if index < len(self.instance_dependencies):
                self.instance_dependencies[index].extend(resources)
            else:
                self.instance_dependencies.append(resources)

    def _add_resource(self, full_name):
        if not full_name in self.resources:
            self.resources.append(full_name)

        """
        curr = self.curr_instance
        if curr is None:
            curr = 0
        """
        # TODO: Verify
        curr = self.render_index
        while curr >= len(self.instance_resources):
            self.instance_resources.append([])

        self.logger.info(f"{curr} vs {len(self.instance_resources)}: {self.instance_resources}")
        resources = self.instance_resources[curr] #pylint: disable=invalid-sequence-index
        if not full_name in resources:
            resources.append(full_name)

    def add_resource_list(self, resources):
        for full_name in resources:
            self._add_resource(full_name)

    def add_resource(self, resource_name, resource_type):
        full_name = f"{resource_type}.{resource_name}"
        self._add_resource(full_name)
        return full_name

    def clear_dependencies(self):
        self.dependencies = []
        self.instance_dependencies = []

    def get_dependencies(self, exclude_types=[]):
        deps = self.dependencies.copy()
        # TODO: Verify
        curr = self.render_index
        """
        curr = self.curr_instance
        if curr is None:
            curr = 0
        """
        if curr < len(self.instance_dependencies):
            deps.extend(self.instance_dependencies[curr])   #pylint: disable=invalid-sequence-index
        if exclude_types:
            self.logger.debug(f"Filtering out {exclude_types}")
            filtered = []
            for dep in deps:
                if not any(dep.startswith(prefix) for prefix in exclude_types):
                    filtered.append(dep)
            deps = filtered
        # de-duplicate
        deps = list(set(deps))
        self.logger.debug(f"{self.name} depends on {deps}")
        return deps
