#!/usr/bin/env python3
import json
import html
import copy
import textwrap

import terrascript
import terrascript.data
import terrascript.resource

from terradactsl import utils as utils
from terradactsl import opts as opts

from terradactsl import Configurable
from terradactsl.customization import GuestInfo

class VM(Configurable):

    opts = opts.VM_OPTS
    default_key = "template"
    unknown_key = None
    attributes = ["config"]

    def __init__(self, project, **kwargs):
        self.inherit_logger_settings(project, kwargs)
        super().__init__(**kwargs)
        self.project = project
        self.zone = None
        self.customization_info = {}
        self.customizers = []
        self.instances = []

        project_attrs = [
            "datacenter_id",
            "datastore_id",
            "log_level",
            "vm_user",
            "vm_password",
            "first_instance"
        ]
        self.inherit(project_attrs, self.project)

        self.init_logger()

        mutually_exclusive = ["template", "template_tags"]
        if not utils.check_mutually_exclusive(self.config, mutually_exclusive):
            msg = f"Exactly one of {mutually_exclusive} must be given!"
            self.logger.error(msg)
            raise Exception(msg)
    
        # The most common use of template_tags is probably latest matching
        # os_flavor and os_variant, and ':' should not be in an template name,
        # So, support template: OS_FLAVOR:OS_VARIANT -> template_tags
        template = self.config.get("template", None)
        template_tags = self.config.get("template_tags", None)
        if template:
            if ":" in self.config["template"]:
                try:
                    os_flavor, os_variant = template.split(":")
                    template_tags = {
                        "os_flavor": os_flavor,
                        "os_variant": os_variant
                    }
                except Exception as e:
                    msg = f"Error parsing {template} as 'os_flavor+os_variant' : {e}"
                    self.logger.error(msg)
                    raise Exception(msg)

        if template_tags:
            avail = self.project.tshell.get_vm_by_tags(
                tags=template_tags,
                sort_by=["os_version"]
            )
            if not avail:
                msg = f'Could not find any template matching query tags: {template_tags}'
                self.logger.error(msg)
                raise ValueError(msg)
            self.logger.debug(f"Available templates: {avail}")
            # Choose most recent
            self.config["template"] = avail[-1]

        if "template_resource_name" not in self.config:
            self.config["template_resource_name"] = utils.get_resource_name(
                self.config["template"],
                prefix="template_"
            )

        template_attrs = [
            "guest_id",
            "firmware",
            "scsi_type",
            "disks.0.size",
            "disks.0.thin_provisioned",
            "disks.0.eagerly_scrub",
            "network_interface_types[0]",
            "id"
        ]
        self.inherit_from_template(template_attrs)
        # Convert the attributes that have a different name
        #   on the template to the name used by the new vm
        #   (e.g. 'id' of the template --> 'template_uuid')
        self.rename_keys(template_attrs)

        # Fill in some additonal defaults
        self.config["customization_type"] = self.get_customization_type()
        
        # "Fake" options that a calling script might have hacked in
        if "options_type" not in self.config:
            self.config["options_type"] = self.get_options_type()

        self.num_instances = self.config["num_instances"]
        if self.num_instances is None:
            instance_names = self.config["instance_names"]
            if instance_names and isinstance(instance_names, list):
                self.num_instances = len(instance_names)
                self.config["num_instances"] = self.num_instances
            else:
                self.num_instances = 1


        # Some config variables are mutated by _prep_render
        self.original_config = self.config.copy()

        self.logger.debug(
            utils.sanitize_config(self.config)
        )

    def get_options_type(self):
        """
        Return either 'linux_options' or 'windows_options'

        """
        # Cut off potential +guestinfo, since terraform only knows linux / windows
        cust_type = self.config["customization_type"].split("+")[0]

        # Assert this function won't be called if doing non-standard customization
        return "{}_options".format(cust_type)

    def get_customization_type(self):
        if self.config["customization_type"] is None:
            # None => no option given => guess
            # Get the 'customization_type' tag from template
            template = self.config["template"]
            self.logger.debug(f"Getting customization type from template: {template}")
            tags = self.project.get_vm_tags(template)
            self.logger.debug(f"Got tags: {tags}")
            if isinstance(tags, bool) or tags is None:
                msg = f"[{self.name}] Failed to get tags from {template}!" \
                       " This could be a temporary vSphere problem," \
                       " or the template may not exist."
                self.logger.error(msg)
                raise Exception(msg)
            elif not "customization_type" in tags:
                self.logger.warning(
                    "'customization_type' not in config or {} tags." \
                    " Defaulting to 'none'.".format(
                        template
                    )
                )
                return "none"
            else:
                return tags["customization_type"]

        elif self.config["customization_type"].lower() in ["none", "null"]:
            # Normalize user input
            # "none" => do no customization
            return "none"
        else:
            # User explicitly set it
            return self.config["customization_type"]

    def _rename_key(self, old_key, new_key, delete_old=True, keep_existing=True):
        if not keep_existing or new_key not in self.config:
            # Allow the user to override settings with the short names
            self.config[new_key] = self.config[old_key]
        if delete_old:
            del self.config[old_key]
        
    def rename_keys(self, current_keys):
        # Rename keys with hacky names to short names 
        for curr in current_keys:
            if '.' in curr:
                key = curr.split('.')[-1] 
                self._rename_key(curr, key)

        # Special cases
        self._rename_key("network_interface_types[0]", "adapter_type")
        self._rename_key("id", "template_uuid")


    def inherit_from_template(self, config_keys):
        for key in config_keys:
            if not key in self.config:
                self.config[key] = "data.vsphere_virtual_machine.{}.{}".format(
                    self.config["template_resource_name"],
                    key
                )
                # Wrap the value like ${some.terraform.expression}
                #   so terraform will evaulate it
                self.config[key] = "${" + self.config[key] + "}"
    
    def _render_disk(self):
        return {
            "label"             : "{}.vmdk".format(self.config["vsphere_name"]),
            "size"              : self.config["size"],
            "thin_provisioned"  : self.config["thin_provisioned"],
            "eagerly_scrub"     : self.config["eagerly_scrub"],
        }

    def _render_network_interface(self):
        network_id = self.net_conf["network_id"]
        if isinstance(network_id, list):
            # Special case for things with multiple networks
            #   (e.g. firewalls)
            return [
                {
                "network_id"        : net_id,
                "adapter_type"      : self.config["adapter_type"],
                }
                for net_id in network_id
            ]
        else:
            return {
                "network_id"        : network_id,
                "adapter_type"      : self.config["adapter_type"],
            }

    def _render_customize_network_interface(self):
        if self.net_conf.get("ip_assignment", "dhcp") == "dhcp":
            return {}
        else:
            return {
                "ipv4_address"  : self.net_conf["ipv4_address"],
                "ipv4_netmask"  : self.net_conf["ipv4_netmask"],
            }

    def _render_customize(self):
        options_type = self.config["options_type"]
        customize = {
            options_type        : self._render_customize_options(),
            "timeout"           : 0,
            "network_interface" : self._render_customize_network_interface(),
            "ipv4_gateway"      : self.net_conf["ipv4_gateway"],
        }

        self.customization_info["ipv4_gateway"] = customize["ipv4_gateway"]
        #https://www.terraform.io/docs/providers/vsphere/r/virtual_machine.html#global-dns-settings
        if self.net_conf.get("dns_servers", None):
            customize["dns_server_list"] = self.net_conf["dns_servers"]
            self.customization_info["dns_server_list"] = customize["dns_server_list"]
        return customize
        
    def _render_customize_options(self):
        options = {
            "host_name" : self.config["host_name"],
            "domain" : self.config["domain"],
            "time_zone" : self.config["time_zone"],
        }
        self.customization_info = options.copy()
        return options

    def _render_data(self, _tf):
        # data block for the parent template
        _tf += terrascript.data.vsphere_virtual_machine(
            self.config["template_resource_name"],
            name=self.config["template"],
            datacenter_id=self.config["datacenter_id"]
        )

    def _render_clone(self):
        clone_block = {
            "template_uuid" : self.config["template_uuid"],
            "linked_clone"  : self.config["linked_clone"],
        }
        cust_type = self.config["customization_type"]
        self.logger.debug(f"Guest uses '{cust_type}' customization")

        # Cut off potential +guestinfo, since terraform only knows linux / windows
        cust_type = cust_type.split("+")[0]
        
        if cust_type not in ["linux", "windows"]:
            # omit customize block if using non-standard customization
            return clone_block
        else:
            self.logger.debug(
                f"Using '{self.config['customization_type']}' customization."
            )
            clone_block["customize"] = self._render_customize()
            return clone_block

    def _render_extra_config(self):
        cust_type = self.config["customization_type"]
        cust_stages = cust_type.split("+")
        self.logger.debug(f"cust_stages: {cust_stages}.")
        extra_config = {}
        # So far, only (linux|windows)[+(guestinfo|guestinfo_agent)] make sense
        for stage in cust_stages:
            if stage.startswith("guestinfo"):
                guestinfo = GuestInfo(self.config, self.project)
                customizer = guestinfo.get_customizer()
                # Currently, only one guestinfo is really supported,
                #   But in the future, it might be valid to combine multiple guestinfo customizers
                extra_config.update(customizer.get_extra_config())
                self.customization_info = customizer.guestinfo

                # guestinfo_agent will customize itself on boot
                if stage == "guestinfo":
                    # Mark that this VM needs custom customization
                    self.customizers.append(customizer)

        return extra_config

    def _render_lifecycle(self):
        return {
            "ignore_changes": [
                "memory",
                "num_cpus",
                "annotation", 
                "tags",
                "network_interface",
                "disk",
                "clone",
            ]
        }

    def _render_vm(self, _tf):
        resource_name = self.config["resource_name"]

        self.logger.debug(f"Creating resource: {resource_name}")
        resource_pool_id = self.project.config["resource_pool_id"]
        is_vapp = False
        if self.zone.vapp_id:
            resource_pool_id = self.zone.vapp_id
            is_vapp = True
        elif self.project.vapp_id:
            resource_pool_id = self.project.vapp_id
            is_vapp = True

        if is_vapp:
            vm_moid = utils.get_terraform_attr(resource_name, "moid", type_prefix="vsphere_virtual_machine")
            self.project.render_vapp_entity(_tf, vm_moid, resource_pool_id)

        kwargs = {
            "depends_on"        : self.get_dependencies(),
            "name"              : self.config["vsphere_name"],
            "resource_pool_id"  : resource_pool_id,
            "datastore_id"      : self.config["datastore_id"],
            "num_cpus"          : self.config["num_cpus"],
            "memory"            : self.config["memory"],
            "guest_id"          : self.config["guest_id"],
            "firmware"          : self.config["firmware"],
            "scsi_type"         : self.config["scsi_type"],
        
            "lifecycle"         : self._render_lifecycle(),
            "disk"              : self._render_disk(),
            "network_interface" : self._render_network_interface(),
            "clone"             : self._render_clone(),
            "extra_config"      : self._render_extra_config(),

            "wait_for_guest_net_routable" : False,
            "wait_for_guest_net_timeout"  : 0,
        }
        if not self.project.config["create_vapp"]:
            # "cannot set folder while VM is in a vApp container"
            kwargs["folder"] = self.config["vsphere_folder"]

        _tf += terrascript.resource.vsphere_virtual_machine(
            resource_name,
            **kwargs 
        )
        
        self.add_resource(resource_name, "vsphere_virtual_machine")

    def get_net_conf(self):
        net_conf = {}
        keys = ["port_group", "ipv4_address", "ipv4_netmask", "ipv4_gateway", "network_id"]
        for key in keys:
            if self.original_config[key] is not None:
                net_conf[key] = self.original_config[key]
        self.logger.debug(f"net_conf: {net_conf}")
        return net_conf

    def _prep_render(self, curr_instance, zone):
        self.zone = zone
        self.curr_instance = curr_instance

        self._summary = ""
        # Fix up instance and zone-specific variables
        
        if self.original_config["resolve_host"] is None:
            self.config["resolve_host"] = zone.config["resolve_hosts"]

        if not self.original_config["attached_to"]:
            self.config["attached_to"] = zone.name
        # Get zone networking info
        net_conf = zone.assign_network(self)
        # Apply any manual overrides
        net_conf.update(self.get_net_conf())
        self.net_conf = net_conf

        # Update the config so customizers can see the settings
        self.config.update(self.net_conf)

        if self.original_config["host_name"] is None:
            self.config["host_name"] = self.next_instance_name()

        if self.original_config["domain"] is None:
            domain = zone.config["domain"] 
            if not domain:
                domain = self.project.domain
            self.config["domain"] = domain

        # Prepare FQDN
        self.config["host_name"] = utils.sanitize_host_name(self.config["host_name"])
        self.config["domain"] = utils.sanitize_domain_name(self.config["domain"])

        fqdn = ".".join([self.config["host_name"], self.config["domain"]])
        self.config["fqdn"] = fqdn

        if self.original_config["vsphere_name"] is None:
            self.config["vsphere_name"] = utils.get_vm_vsphere_name(self, zone)
        
        if self.original_config["vsphere_folder"] is None:
            self.config["vsphere_folder"] = zone.assign_folder(self)

        if self.original_config["resource_name"] is None:
            prefix = f"vm_{self.project.name}_{zone.curr_instance}_"
            self.config["resource_name"] = utils.get_resource_name(
                self.config["vm_name"],
                prefix=prefix,
                curr_instance=self.render_index
            )


        aliases = []
        for alias in self.original_config["aliases"]:
            if not "." in alias:
                alias += f'.{self.config["domain"]}'
            aliases.append(alias)
        self.config["aliases"] = aliases
        # Save any instance-info we need later
        info = {}
        info["host_name"] = self.config["host_name"]
        info["domain"] = self.config["domain"]
        info["fqdn"] = fqdn
        info["aliases"] = aliases
        info["ipv4_address"] = self.net_conf.get("ipv4_address", None)
        info["ip_assignment"] = self.net_conf.get("ip_assignment", None)
        info["vsphere_name"] = self.config["vsphere_name"]
        info["attached_to"] = self.config["attached_to"]
        info["parent_project"] = self.project.name
        info["root_project"] = self.root_project
        info["resource_name"] = f'vsphere_virtual_machine.{self.config["resource_name"]}'
        # Store os_flavor so we can look up network diagram icons later
        tags = self.project.get_vm_tags(self.config["template"])
        info["os_flavor"] = tags.get("os_flavor", None)

        self.instances.append(info)     

    def render(self, _tf, zone, curr_instance, color=True):
        self.root_project = zone.root_project
        summary = ""
        self.reset_instance_names()
        for curr in range(self.num_instances):
            self.render_index += 1
            self._prep_render(curr_instance, zone)

            self._render_data(_tf)
            self._render_vm(_tf)

            summary += self.summary(color=color)
            self.instances[-1]["summary"] = self.summary(html=True)
        return summary

    def _text_summary(self, ip_address=None, ipv4_gateway=None, cust_info=None):
        cust_info = utils.dumps(cust_info)
        cust_info = textwrap.indent(cust_info, " "*8)
        summary = f"""
-------------------------------------------------------------------------------
{self.config["vsphere_name"]}
-------------------------------------------------------------------------------
    host_name           : {self.config["host_name"]}
    domain              : {self.config["domain"]}
    aliases             : {self.config["aliases"]}
    attached_to         : {self.config["attached_to"]} ({self.zone.config["instance_name"]})
    ip_address          : {ip_address}
    ipv4_gateway        : {ipv4_gateway}
    port_group          : {self.net_conf["port_group"]}
    vsphere_folder      : {self.config["vsphere_folder"]}
    template       : {self.config["template"]}
    customization_type  : {self.config["customization_type"]}
    customization_info  :
{cust_info}
-------------------------------------------------------------------------------
"""
        return summary

    def _html_summary(self, ip_address=None, ipv4_gateway=None, cust_info=None):
        summary = f'<b>{self.config["vsphere_name"]}</b>'
        # Primary attributes
        attached_to = self.config["attached_to"]
        if not isinstance(ip_address, str):
            ip_address = utils.dumps(ip_address)
        if not isinstance(attached_to, str):
            attached_to = utils.dumps(attached_to)
        port_group = self.net_conf["port_group"]
        
        if not isinstance(port_group, str):
            port_group = utils.dumps(port_group)

        attrs = {
            "ip_address"        : ip_address,
            "ipv4_gateway"      : ipv4_gateway,
            "port_group"        : port_group,
            "template"          : self.config["template"],
            "attached_to"       : attached_to,
            "aliases"           : utils.dumps(self.config["aliases"]),
            "vsphere_folder"    : self.config["vsphere_folder"],
        }
        summary += utils.format_html_attrs(attrs)
        # Secondary attributes
        # Don't include hosts, it wont fit in the hovertext
        _cust_info = copy.deepcopy(cust_info) 
        if "hosts" in _cust_info:
            _cust_info["hosts"] = f'<i>{len(_cust_info["hosts"])} DNS entries</i>'
        route_attrs = {}
        if "gateway_routes" in _cust_info:
            # Abbreviate routes so they fit in hovertext
            for route_name, route in _cust_info["gateway_routes"].items():
                key = f'{route["via"]} [{route_name}]'
                if "routes" in route:
                    route_attrs[key] = utils.dumps(route["routes"])
                else:
                    route_attrs[key] = "default"
                
            del _cust_info["gateway_routes"]
        attrs = {
            "customization_type": self.config["customization_type"],
            "customization_info": utils.dumps(_cust_info)
        }
        extra = utils.format_html_attrs(attrs)
        if route_attrs:
            extra += "<br><b>gateway_routes</b>"
            extra += utils.format_html_attrs(route_attrs)
        summary += f"<extra>{extra}</extra>"
        return summary

    def summary(self, color=True, html=False):
        ipv4_address = self.net_conf["ipv4_address"]
        ipv4_netmask = self.net_conf["ipv4_netmask"]
        ip_address = None
        if isinstance(ipv4_address, list):
            ip_address = [
                "{}/{}".format(str(address), netmask)
                for address, netmask in zip(ipv4_address, ipv4_netmask)
            ]
        else:
            ip_address = "{}/{}".format(
                str(ipv4_address),
                ipv4_netmask
            )
        zone_instance = self.zone.config["instance_name"]
        ipv4_gateway = self.net_conf.get("ipv4_gateway", None)
        # Simplify ipv4_gateway for firewalls
        if isinstance(ipv4_gateway, list):
            if "gateway_routes" in self.customization_info:
                if "default" in self.customization_info["gateway_routes"]:
                    default = self.customization_info["gateway_routes"]["default"]
                    if default["via"]:
                        ipv4_gateway = default["via"]
                    else:
                        ipv4_gateway = "(DHCP)"

        summary = None
        if html:
            summary = self._html_summary(
                ip_address=ip_address,
                ipv4_gateway=ipv4_gateway,
                cust_info=self.customization_info
            )
        else:
            summary = self._text_summary(
                ip_address=ip_address,
                ipv4_gateway=ipv4_gateway,
                cust_info=self.customization_info
            )

            if color:
                summary = utils.vm_summary(summary)

        return summary
