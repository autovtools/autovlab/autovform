#!/usr/bin/env python3
import os
import random
import logging

import yaml

import terrascript
import terrascript.data
import terrascript.resource
import terrascript.provider

from terrashell import TerraShell

from terradactsl import utils as utils
from terradactsl import opts as opts
from terradactsl import Configurable, Zone, UplinkZone, VM
from .infrastructure import Infrastructure

class Project(Configurable):

    opts = opts.PROJECT_OPTS
    unknown_key = "zones"
    unkown_opts = Zone.opts
    attributes = ["zones", "project_map", "vm_map", "config"]
    # Packinator keys we might get from a shared config file
    ignore_keys = ["ssh_user", "ssh_password"]

    def __init__(self, project=None, zone=None, **kwargs):
        if "name" in kwargs:
            self._base_name = kwargs["name"]
        
        if project:
            self.config = kwargs["config"]
            # Inherit defaults from our parent project before we call __init__()
            project_keys = list(self.opts.keys())
            # These settings should only be defined if manually set by the user
            exclude = [
                "uplink",
                "uplink_zone",
                "instance_supernet",
                "instance_network",
                "infra",
                "vms",
                "zones",
                "projects",
                "project_name",
                "num_instances",
                "instance_names",
                "domain"
            ]
            for key in exclude:
                project_keys.remove(key)
            self.inherit(project_keys, project)

            #kwargs["name"] = ".".join([project.name, kwargs["name"]])
            kwargs["config"] = self.config

            self.inherit_logger_settings(project, kwargs)

        super().__init__(**kwargs)
        self.init_logger()

        if not self._base_name and "project_name" in self.config:
            self._base_name = self.config["project_name"]

        if not self.name:
            msg = f"Constructed project has no project_name! Is the project config missing?"
            raise ValueError(msg)

        # Set if we are a subprojects
        self.project = project
        self.zone = zone

        self.zones = {}
        self.use_cluster = False
        self.instance_subnets = None

        self.project_folders = []
        self.instance_folders = []
        self.folder_ids = []

        self.vlan_gen = None
        self.vm_tags = {}
        self.visited = False
        self.instances = []
        self.info = None
        self.restored_index = None
        self.project_graph = {}
        self.project_map = {}
        self.dns_map = {}
        self.vm_map = {}
        self.vapp_id = None
        self.domain = None

        self.num_instances = self.config["num_instances"]
        if self.num_instances is None:
            instance_names = self.config["instance_names"]
            if instance_names and isinstance(instance_names, list):
                self.num_instances = len(instance_names)
                self.config["num_instances"] = self.num_instances
            else:
                self.num_instances = 1
        
        if self.zone:
            # We are a sub-project
            pass
        else:
            # We are root project
            mutually_exclusive = ["vsphere_resource_pool", "vsphere_compute_cluster"]
            if not utils.check_mutually_exclusive(self.config, mutually_exclusive):
                msg = f"Exactly one of {mutually_exclusive} must be given!"
                self.logger.error(msg)
                raise Exception(msg)

            if self.config["zones"] is None:
                self.logger.warning(f"Adding a 'zones' block for {self.name}")
                self.config["zones"] = {}

            # assume uplink_zone = "WAN" if WAN exists and uplink not set.
            if not self.config["uplink"] and not self.config["uplink_zone"]:
                if "WAN" in self.config["zones"]:
                    self.logger.info(f"Assuming the zone named 'WAN' is the UplinkZone")
                    self.config["uplink_zone"] = "WAN"

            # Now enforce the 'exactly one'
            mutually_exclusive = ["uplink", "uplink_zone"]
            if not utils.check_mutually_exclusive(self.config, mutually_exclusive):
                msg = f"Exactly one of {mutually_exclusive} must be given!" \
                       " (Or define a zone named 'WAN')."
                msg += 'If you want an air-gapped network, set uplink to "" (empty string).'
                self.logger.error(msg)
                raise Exception(msg)
        

        # Set up TerraShell
        self.vsphere_creds = {
            key:self.config[key]
            for key in ["vsphere_server", "vsphere_user", "vsphere_password"]
        }
        self.tshell = TerraShell(
            log_level=self.log_level,
            log_file=self.log_file,
            show_line_info=self.show_line_info,
            vsphere_creds=self.vsphere_creds
        )

        if not self.project:
            # We are root, figure out the right resource pool id
            if "vsphere_resource_pool" in self.config:
                self.config["resource_pool_id"] = "${data.vsphere_resource_pool.pool.id}"
            else:
                self.config["resource_pool_id"] = "${data.vsphere_compute_cluster.cluster.resource_pool_id}"
                self.use_cluster = True

            
        self.config["datacenter_id"] = "${data.vsphere_datacenter.dc.id}"
        self.config["datastore_id"]  = "${data.vsphere_datastore.datastore.id}"
        self.config["dvs_id"]        = "${data.vsphere_distributed_virtual_switch.dvs.id}"


        # Prepare some networking info
        self.instance_supernet_gen = None
        if not self.project:
            # We are root project
            if self.config["meta_overlap"] or not self.instance_supernet_gen:
                # We either don't have a generator or user wants us to reset it (and re-use IPs)
                net = utils.to_network(self.config["meta_supernet"])
                self.instance_supernet_gen = net.subnets(new_prefix=self.config["meta_cidr"])

        # Prepare infrastructure infrastructure
        self.infra = Infrastructure(
            self,
            config=self.config["infra"]
        )

        # Register Zones
        # Make an UplinkZone and add it, if the user didn't define one
        if self.config["uplink_zone"] is None:
            if self.zone:
                # Give ourselves a reference to our parent's LAN, which is our WAN
                # Note: Don't render it twice!
                self.zones["WAN"] = self.zone
            else:
                self.add_zone("WAN", None, uplink=self.config["uplink"])
                self.zones["WAN"].name = "*WAN*"
            self.config["uplink_zone"] = "WAN"

        # Some objects must be part of a Zone.
        # If the user put them in the project config, create a default zone
        #   and move the objects into the Zone
        # (This can help remove layers of indentation and simplify user configs)
        zone_keys = ["vms", "projects"]
        if any(self.config[key] is not None for key in zone_keys):
            zone_config = {
                "create_zone_folders": False,
                
            }
            for key in zone_keys:
                if self.config[key] is not None:
                    zone_config[key] = self.config[key]
                    self.config[key] = None
            zone_name = Zone.opts["default_name"]["default"]
            self.logger.info(f"Adding default zone {zone_name}")
            self.add_zone(zone_name, zone_config)

        if self.config["zones"] is not None:
            uplink_zone = self.config["uplink_zone"]
            for zone_name, zone_config in self.config["zones"].items():
                uplink = None
                if uplink_zone and zone_name == uplink_zone:
                    self.logger.info(f"Adding UplinkZone {self.config['uplink_zone']}")
                    uplink = self.config["uplink"]
                else:
                    self.logger.info(f"Adding zone {zone_name}")
                self.add_zone(zone_name, zone_config, uplink=uplink)
                

    def _render_vars(self, _tf):
        user_vars = [
            "vsphere_user",
            "vsphere_password",
            "vsphere_server",
            "vsphere_datacenter",
            "vsphere_datastore",
            "vsphere_dvs",
            "parent_folder",
        ]
        if self.use_cluster:
            user_vars.append("vsphere_compute_cluster")
        else:
            user_vars.append("vsphere_resource_pool")
            
        for var in user_vars:
            _tf += terrascript.Variable(
                var,
                type="string",
                description=self.opts[var]["description"]
            )

    def _render_data(self, _tf):
        datacenter_id = self.config["datacenter_id"]

        # Assumption: only one of each of these is needed for a single project
        _tf += terrascript.data.vsphere_datacenter(
            "dc",
            name = "${var.vsphere_datacenter}"
        )
        _tf += terrascript.data.vsphere_datastore(
            "datastore",
            name = "${var.vsphere_datastore}",
            datacenter_id = datacenter_id
        )
        _tf += terrascript.data.vsphere_distributed_virtual_switch(
            "dvs",
            name = "${var.vsphere_dvs}",
            datacenter_id = datacenter_id
        )
        if self.use_cluster:
            _tf += terrascript.data.vsphere_compute_cluster(
                "cluster",
                name = "${var.vsphere_compute_cluster}",
                datacenter_id = datacenter_id
            )
        else:
            _tf += terrascript.data.vsphere_resource_pool(
                "pool",
                name = "${var.vsphere_resource_pool}",
                datacenter_id = datacenter_id
            )
       
    def _render_provider(self, _tf):
        _tf += terrascript.provider.vsphere(
            user                 = "${var.vsphere_user}",
            password             = "${var.vsphere_password}",
            vsphere_server       = "${var.vsphere_server}",
            allow_unverified_ssl = self.config["insecure_connection"]
        )

    def render_vapp_entity(self, _tf, target_id, container_id, depends_on=None):
        self.logger.debug(
            "render_vapp_entity currently disabled because\n" \
            " A) Nested vApp entities are built before the parent container is exists, causing failure.\n" \
            " B) start_delay never actually applies, plan always shows 120 -> 0 change is needed."
        )
        # Possibly related:
        # https://github.com/hashicorp/terraform-provider-vsphere/issues/943
        # https://github.com/hashicorp/terraform-provider-vsphere/issues/767

        return

        # Called by VMs if zone.vapp_id is set
        # Mostly because start_delay defaults to 120s, making vApp power very slow
        resource_name = utils.get_resource_name(
            target_id,  
            prefix="vapp_entity_"
        )
        if depends_on is None:
            depends_on = []

        _tf += terrascript.resource.vsphere_vapp_entity(
            resource_name,
            depends_on = depends_on,
            target_id = target_id,
            container_id = container_id,
            start_delay = 0,
            stop_delay = 120,
        )
        #self.add_resource(resource_name, "vsphere_vapp_entity")

    def get_pool_id(self):
        pool = self.config["resource_pool_id"]
        if self.project:
            # Subprojects need to get current resource pool from parent
            if self.zone.vapp_id:
                pool = self.zone.vapp_id
            elif self.project.vapp_id:
                pool = self.project.vapp_id
        return pool

    def render_vapp_container(self, _tf, parent_folder_id, parent_pool, vapp_name):

        resource_name = utils.get_resource_name(
            vapp_name,  
            prefix="vapp_container_"
        )
        _tf += terrascript.resource.vsphere_vapp_container(
            resource_name,
            name = vapp_name,
            parent_folder_id = parent_folder_id,
            parent_resource_pool_id = parent_pool
        )
        vapp = self.add_resource(resource_name, "vsphere_vapp_container")
        vapp_id = utils.get_terraform_attr(vapp, "id")
        if parent_pool != self.config["resource_pool_id"]:
            depends_on = [vapp_id, parent_pool]
            depends_on = [utils.get_terraform_resource(dep) for dep in depends_on]
            self.render_vapp_entity(_tf, vapp_id, parent_pool, depends_on=depends_on)
        return vapp_id

    def get_folders(self):
        folders = None
        if self.instance_folders:
            folders = self.instance_folders
        elif self.project_folders:
            folders = self.project_folders
        else:
            folders = [self.config["parent_folder"]]
        return folders

    def _should_create_folder(self, folder_type):
        create = self.config[f"create_{folder_type}"]
        if self.project:
            # Subproject
            return create and not self.config["create_vapp"]
        else:
            # Root project
            return create

    def should_create_project_folder(self):
        return self._should_create_folder("project_folder")

    def should_create_instance_folder(self):
        return self._should_create_folder("instance_folders") \
            and self.name != self.config["project_name"]

    def render_resources(self, _tf, curr_instance):
        project_folder_resource = None
        # parent_folder isn't "portable", so don't bake it into the plan;
        #   pass it as a variable
        parent_folder = "${var.parent_folder}"

        project_folder = parent_folder
        folder_path = project_folder

        folder_id = None
        if self.project:
            folder_id = self.project.folder_ids[-1]

        if self.should_create_project_folder():
            if self.project:
                folder_path = self.project.get_folders()[-1]

            folder_path = os.path.join(
                folder_path,
                self.base_name
            )

            resource_name = utils.get_resource_name(
                folder_path.replace(f"{parent_folder}/", ""),
                prefix="project_",
            )

            _tf += terrascript.resource.vsphere_folder(
                resource_name,
                depends_on    = self.get_dependencies(),
                path          = folder_path,
                type          = "vm",
                datacenter_id = self.config["datacenter_id"]
            )
            project_folder_resource = self.add_resource(resource_name, "vsphere_folder")
            folder_id = utils.get_terraform_attr(project_folder_resource, "id")
            project_folder = folder_path

        self.project_folders.append(project_folder)

        # Only create an instance folder if asked to
        #   and if we have a unique instance name
        if self.should_create_instance_folder():
            deps = self.get_dependencies()
            project_folder_resources = [resc for resc in self.resources if resc.startswith("vsphere_folder.project_")]
            deps.extend(project_folder_resources)

            prefix = "inst_{}_".format(
                folder_path.replace(f"{parent_folder}/", "")
            )

            resource_name = utils.get_resource_name(
                self.name,
                prefix=prefix,
            )

            folder_path = os.path.join(
                folder_path,
                self.name
            )

            _tf += terrascript.resource.vsphere_folder(
                resource_name,
                depends_on    = deps,
                path          = folder_path,
                type          = "vm",
                datacenter_id = self.config["datacenter_id"]
            )
            self.logger.warning(f"{self.name} instance folder: {resource_name}: {folder_path}")
            instance_folder_resource = self.add_resource(resource_name, "vsphere_folder")
            self.instance_folders.append(folder_path)
            folder_id = utils.get_terraform_attr(instance_folder_resource, "id")

        self.folder_ids.append(folder_id)

        if self.config["create_vapp"]:
            vapp_name = utils.get_vapp_name(self)
            #self.config["parent_resource_pool_id"] = self.config["resource_pool_id"]
            self.config["parent_resource_pool_id"] = self.get_pool_id()
            self.vapp_id = self.render_vapp_container(
                _tf,
                folder_id,
                self.config["parent_resource_pool_id"],
                vapp_name
            )
            

    def _render_infra(self, _tf, curr_instance):
        return self.infra.render(_tf, curr_instance)
        
    def render_infra(self, _tf):
        summary = ""
        self.infra.reset_render_index()
        # Reset dependencies
        self.infra.clear_dependencies()
        # Infrastructure depends on the project
        self.infra.instance_depends_on(self)

        for curr, info in enumerate(self.instances):
            curr = self.restore_info(curr)
            self.logger.info(f"Rendering {self.name} Infra #{curr}...")

            summary += self.get_banner(
                f"{self.name} Infrastructure"
            )
            summary += self._render_infra(_tf, curr)
        return summary

    def get_child_projects(self):
        children = []
        for zone_name, zone in self.zones.items():
            if zone_name == self.config["uplink_zone"]:
                continue
            for project in zone.projects.values():
                children.append(project)
        return children

    def compute_wan_ips(self):
        for curr, info in enumerate(self.instances):
            # TODO: Verify
            self.restore_info(curr)
            self.logger.warning(f"[{self.name}] computing Infra WAN for {curr}")
            self.infra.compute_wan_ips(curr)

        for project in self.get_child_projects():
            project.compute_wan_ips()

    def render_all_infra(self, _tf):
        # Render the infrastructure (which needs access to the rendered zones)
        #   and finish the summary
        summary = ""
        """
        for zone_name, zone in self.zones.items():
            if zone_name == self.config["uplink_zone"]:
                continue
            for project in zone.projects.values():
                if project != self:
                    self.logger.debug(f"Recursing to {project.name} Infrastructure...")
                    summary += project.render_all_infra(_tf)

        """
        for project in self.get_child_projects():
            summary += project.render_all_infra(_tf)

        header = self.render_infra(_tf)
        return header + summary

    def prep_infra(self):
        for curr, info in enumerate(self.instances):
            # TODO: Verify
            curr = self.restore_info(curr)
            self.infra.prep_resources(curr)
            return

    def init_terrascript(self):
        # These should be rendered exactly once (by root project)
        _tf = terrascript.Terrascript()
        self._render_vars(_tf)
        self._render_provider(_tf)
        self._render_data(_tf)
        return _tf

    def _render_zone(self, _tf, zone, curr_instance):
        zone.clear_dependencies()
        zone.instance_depends_on(self)
        # This is why infra must precompute its resource names
        zone.instance_depends_on(self.infra)
        self.logger.info(f"{self.name} rendering {zone.name} instance {curr_instance}")
        return zone.render(_tf, curr_instance=curr_instance)

    def reset_names(self, zone):
        # Reset the zone's name generator
        # Reset subproject name generators
        zone._instance_names = zone.get_instance_name_generator()
        for project in zone.projects.values():
            project._instance_names = project.get_instance_name_generator()

    def render_zone(self, _tf, zone):
        self.reset_names(zone)

        if self.zone:
            return self._render_zone(_tf, zone, self.curr_instance)

        """
        summary = ""
        for curr_instance in range(self.num_instances):
            summary += self._render_zone(_tf, zone, curr_instance)
        """
        summary = self._render_zone(_tf, zone, self.curr_instance)

        return summary

    def _render(self, _tf, curr_instance):
        # Every project, including subprojects, can render resources
        self.name = self.next_instance_name()
        if self.project:
            self.name = ".".join([self.project.name, self.name])
        self.logger.warning(f"Changed name to: {self.name}")
        if self.config["domain"] is None:
            self.domain = utils.get_domain_name(self) 
        else:
            self.domain = self.config["domain"]

        self.render_resources(self._tf, curr_instance)
        
        self.stash_info()

        # DEBUG
        self.infra.prep_resources(curr_instance)

        summary = self.summary()

        for zone_name, zone in self.zones.items():
            # Note: don't use *real* zone name here,
            #   because we may have named our key "WAN"
            if self.zone and zone_name == self.config["uplink_zone"]:
                # Subprojects should not render uplink_zones that were
                #   already rendered by the parent project
                self.logger.info(f"Skipping uplink_zone {zone_name}...")
                continue

            self.logger.info(f"Rendering {zone_name}...")
            self.logger.info(f"{self.name} rendering {zone_name} ({zone.name})")
            # Reset and push dependencies

            summary += self.render_zone(self._tf, zone)
            self.logger.info(f"Rendered {zone_name}.")

        return summary

    def get_banner(self, banner_text):
        banner = "#"*80
        return f"""
{banner}
{banner_text}
{banner}
"""
    def restore_info(self, curr):
        self.logger.debug("!"*80)
        self.logger.debug(f"!!! Restoring {curr}: {self.name} !!!")
        self.logger.debug(self.info)

        #self.curr_instance = curr
        # TODO: Verify
        self.curr_instance = self.wrap_curr_instance(curr)
        self.info = self.instances[curr]
        self.restored_index = curr
        self.name = self.info["instance_name"]
        self.network_map = self.info["network_map"]
        self.instance_folders = self.info["folders"]
        self.root_project = self.info["root_project"]
        self.vapp_id = self.info["vapp_id"]
        self.domain = self.info["domain"]

        self.logger.info(f"!!! Restored {self.curr_instance}: {self.name} !!!")
        self.logger.debug(self.info)
        self.logger.debug("!"*80)
        return self.curr_instance


    def get_root_project(self):
        if not self.project:
            return self
        return self.project.get_root_project()

    def get_project_by_name(self, instance_name):
        project_map = self.get_project_map()
        return project_map[instance_name]
    

    def get_index_by_name(self, instance_name):
        index = None
        project = self.get_project_by_name(instance_name)
        for index, info in enumerate(project.instances):
            if info["instance_name"] == instance_name:
                break
        return index

    def get_info_by_name(self, instance_name):
        project = self.get_project_by_name(instance_name)
        index = project.get_index_by_name(instance_name)
        return project.instances[index]

    def get_infra_info_by_name(self, instance_name):
        project = self.get_project_by_name(instance_name)
        index = project.get_index_by_name(instance_name)
        return project.infra.instances[index]

    def get_wan_ip_by_name(self, instance_name):
        project = self.get_project_by_name(instance_name)
        index = project.get_index_by_name(instance_name)
        return project.infra.wan_ips[index]

    def stash_info(self):
        # Get the index of this info blob after we append
        index = len(self.instances)

        self.info = {
            "instance_name"     : self.name,
            "folders"           : self.get_folders(),
            "instance_index"    : len(self.instances),
            "parent_project"    : None,
            "network_map"       : self.network_map,
            "vapp_id"           : self.vapp_id,
            "root_project"      : None,
            "domain"            : self.domain
        }
        if self.project:
            self.info["parent_project"] = self.project.name
            self.root_project = self.project.root_project
        else:
            self.root_project = self.name

        self.info["root_project"] = self.root_project
        self.info["summary"] = self.summary(color=False)
        self.instances.append(self.info)

    def render(self, _tf=None):
        self._tf = _tf
        if self._tf is None:
            # We are root project on first instance,
            #   start a new script
            self._tf = self.init_terrascript()

        # TODO: Verify
        if self.project:
            # We are subproject
            curr = len(self.instances)
            self.curr_instance = self.wrap_curr_instance(curr)
            ## Reset the name generator
            #self._instance_names = self.get_instance_name_generator()

        # Reset the name generator
        #self._instance_names = self.get_instance_name_generator()


        summary_list = []

        # Compute Networking Info
        self.instance_subnets = self.get_instance_subnets()
        self.network_map = self._get_network_map()


        for curr in range(self.num_instances):
            self.render_index += 1
            self.logger.warning(f"!!!! {self.name} render (multi) {curr} !!!!!!")
            self.curr_instance = curr
            summary = self._render(self._tf, curr)
            summary_list.append(summary)

        banner = self.get_banner(self.name)

        infra_summary = ""
        if not self.project:
            # We are the root project; it's time to recursively render infrastructure 
            #   (which might need zone info from anywhere in the project)
            self.logger.info("Rendering all infrastructure...")
            # Precompute the wan IPs and routes
            self.compute_wan_ips()
            self.get_project_graph()
            infra_summary = self.render_all_infra(self._tf)
            self.update_project_graph()
            summary_list.insert(0, self.summary(vsphere_only=True))
            summary_list[0] += infra_summary
            banner = self.get_banner(self.config["project_name"])

        summary = banner.join(summary_list)
        return summary

    def get_name_tokens(self, curr):
        # TODO: Verify
        if curr is None or curr >= len(self.instance_tokens):
            curr = -1

        tokens = []
        if self.project:
            tokens.extend(
                self.project.get_name_tokens(curr)
            )
        self.logger.info(f"[{self.name}] Index {curr} : {self.instance_tokens}")
        tokens.append(self.instance_tokens[curr])
        return tokens
        
    def unvisit(self, parent=False):
        self.visited = False
        for zone_name, zone in self.zones.items():
            if not parent and zone_name == self.config["uplink_zone"]:
                self.logger.debug(f"Will not unvisit parent {zone_name}")
                continue
            zone.visited = False
            for subproject in zone.projects.values():
                if not subproject.visited:
                    continue
                subproject.visited = False
                subproject.unvisit()

    def get_parent_reachable(self):
        reachable = []
        if self.project and not self.project.visited:
            self.project.vistited = True
            reachable = self.project.get_reachable()
            self.logger.error(f"{self.name} can reach {reachable} via parent {self.project.name}.")
        return reachable

    def get_directly_reachable(self):
        reachable = []
        for zone_name, networks in self.get_network_map().items():
            zone = self.zones[zone_name]
            restored_index = self.restored_index
            if zone_name == self.config["uplink_zone"]:
                restored_index = self.project.restored_index
            info = zone.instances[restored_index]
            zone_name = info["zone_name"]
            zone_network = info["zone_network"]
            #label = ".".join([self.project.name, zone_name])
            #label = ".".join([self.name, zone_name])
            label = zone_name
            self.logger.info(f"get_directly_reachable: {label} : {zone_network}")

            zone_network = info["zone_network"]
            if zone_network:
                reachable.append(
                {
                    label: str(zone_network)

                }
            )
        self.logger.error(f"{self.name} can reach {reachable} directly")
        return reachable

    """
    def consolidate_reachable(self, reachable):
        pass
        
    def get_reachable(self):
        pass
    """

    def get_project_map(self):

        project_map = {}

        for index, info in enumerate(self.instances):
            project_map[info["instance_name"]] = self

        for project in self.get_projects():
            if project != self:
                project_map.update(project.get_project_map())

        self.project_map.update(project_map)
        return project_map

    def get_fw(self):
        """
        Return fqdn, vm for current project firewall
        """
        fw_list = []
        for vm in self.infra.vms.values():
            role = None
            if isinstance(vm, dict):
                role = vm["role"]
            else:
                role = vm.config["role"]
            if role == "firewall":
                fw_list.append(vm)

        # Get the correct info blob for the firewall
        for vm in fw_list:
            fw = self.get_vm_attrs(vm, None, self.name)
            # There should only be one infra VM with role "firewall"
            if fw:
                return fw[0]

    def update_project_graph(self, graph=None):
        # Only update the root project_graph
        # Lower project_graphs might have been ovewritten
        if not self.project_map:
            self.get_project_map()
        if graph is None:
            graph = self.project_graph
        for project_name, sub_graph in graph.items():
            if not isinstance(sub_graph, dict):
                continue
            # Invoke any late callbacks
            for key, val in sub_graph.items():
                if callable(val):
                    sub_graph[key] = val()
            if "__fw__" in sub_graph:
                # We are ready to update
                infra = self.project_map[project_name].infra
                fw_name = sub_graph["__fw__"]["fqdn"]
                # There can only be one instance of each project's firewall
                # (If the project had multiple instances, each fw_name is unique)
                info = infra.vms[fw_name].instances[-1]
                sub_graph["__fw__"] = utils.make_serializable(info)
                self.logger.info(f"Updated {project_name}")
            self.update_project_graph(graph=sub_graph)

    def get_project_graph(self):
        self.logger.warning("-"*80)
        self.logger.warning(f"Beginning route traversal from {self.base_name}")
        self.logger.warning("-"*80)

        restored_index = self.restored_index
        project_graph = {}
        for index, info in enumerate(self.instances):
            self.restore_info(index)
            if self.project and self.project.name != self.info["parent_project"]:
                self.logger.error(f'{self.name} is connected to {self.info["parent_project"]}, not {self.project.name}')
                continue
            self.unvisit()
            self.logger.warning(f"Visit: {self.name}")
            if self.project:
                self.logger.warning(f"    Parent: {self.project.name}")

            if not self.name in project_graph:
                project_graph[self.name] = {}

            fw = self.get_fw()
            project_graph[self.name]["__fw__"] = utils.make_serializable(fw)

            for zone_name, networks in self.network_map.items():
                if zone_name == self.config["uplink_zone"]:
                    self.logger.info(f"[{self.name}] Skipping uplink_zone {zone_name}")
                    continue
                zone = self.zones[zone_name]
                #network = networks[self.restored_index % self.num_instances]
                zone_info = zone.instances[self.restored_index]
                zone_info = utils.make_serializable(zone_info)

                network = zone_info["zone_network"]
                if network:
                    network = str(network)
                
                self.logger.info(f"Found: {self.name} | {zone_name}: {network}")

                if not zone_name in project_graph[self.name]:
                    project_graph[self.name][zone_name] = {}
                if network:
                    project_graph[self.name][zone_name][None] = zone_info
                    self.logger.warning(f"Added: {self.name}[{zone_name}] = {network}")
                else:
                    self.logger.warning(f"No Route: {self.name}[{zone_name}]")

                # Include zone VMs in the graph
                vms = []
                for vm in zone.vms.values():
                    vms.extend(self.get_vm_attrs(vm , None, self.name))
                vms = [utils.make_serializable(vm) for vm in vms]
                project_graph[self.name][zone_name]["__vms__"] = vms
            
                for project in zone.projects.values():
                    self.logger.info(self.name)
                    self.logger.info(f"[{self.name}]: Recursing to {project.base_name} via {zone_name}")
                    subproject_graph = project.get_project_graph()
                    self.logger.info(f"[{self.name} -> {project.base_name}]")
                    project_graph[self.name][zone_name].update(
                        subproject_graph
                    )

        #self.project_graph.update(routes)
        self.restore_info(restored_index)
        self.project_graph = project_graph
        return project_graph
        #return self.project_graph

    """
    def get_fqdn(self, vm, parent_project=None):
        fqdns = []
        for info in vm.instances:
            if parent_project and parent_project != info["parent_project"]:
                continue
            host_name = info["host_name"]
            domain = info["domain"]
            fqdns.append(f"{host_name}.{domain}")
            
        return fqdns
    """

    def _get_vm_attrs(self, info, attr, parent_project=None):
        if parent_project and parent_project != info["parent_project"]:
            return
        if attr is None:
            return info
        else:
            return info[attr]

    def get_vm_attrs(self, vm, attr, parent_project=None):
        """
        Returns all vm.instances[X][attr] for all instances belonging to parent_project.
        
        If parent_project is None, all such attrs are returned
        """
        vm_attrs = []
        if isinstance(vm, dict):
            vm_attrs.append(
                self._get_vm_attrs(vm, attr, parent_project=parent_project)
            )
        else:
            for info in vm.instances:
                vm_attrs.append(
                    self._get_vm_attrs(info, attr, parent_project=parent_project)
                )
            
        return [attr for attr in vm_attrs if attr is not None]

    def get_vm_inventory(self, parent=None):
        project_map = {}
        for info in self.instances:
            instance_name = info["instance_name"]
            if parent and parent != info["parent_project"]:
                continue
            project_map[instance_name] = {}
            hosts = self.get_vms(recurse=False, internal=True)
            fqdns = []
            for host in hosts:
                fqdns.extend(self.get_vm_attrs(host, "fqdn", instance_name))
            if fqdns:
                 project_map[instance_name]["hosts"] = {fqdn: None for fqdn in fqdns}
            children = {}
            for project in self.get_child_projects():
                child_map = project.get_vm_inventory(parent=instance_name)
                children.update(child_map)
            if children:
                project_map[instance_name]["children"] = children
        return project_map

    def get_ansible_inventory(self):
        inv = self.get_vm_inventory()
        return yaml.dump(inv, default_flow_style=False)

    def get_dns_map(self):
        hosts = {}
        root_project = self.get_root_project()
        # Get all vms in the whole project
        for vm in root_project.get_vms():
            info = None
            if isinstance(vm, dict):
                # Not a real VM, just a placeholder
                info_list = [vm]
                self.logger.debug(f"----Fake VM: {info_list}")
            else:
                self.logger.debug(f"{vm.name}: {vm.instances}")
                if not vm.instances or not vm.config["resolve_host"]:
                    continue
                info_list = vm.instances

            for info in info_list:
                # Skip VMs in a different root project to prevent DNS conflicts
                if info["root_project"] != self.root_project:
                    self.logger.debug(f"[{self.name}] Ignoring {info['host_name']}: {info['root_project']} vs {self.root_project}")
                    continue
                fqdn = info["fqdn"]
                ip_addr = info["ipv4_address"]
                if ip_addr:
                    if isinstance(ip_addr, list):
                        ip_addr = ip_addr[0]

                hosts[fqdn] = str(ip_addr)
                for alias in info["aliases"]:
                    hosts[alias] = str(ip_addr)

                self.logger.debug(f"[{self.name}] HOST MAPPING: {fqdn} (AKA {info['aliases']}) -> {ip_addr}")
        self.dns_map = hosts
        self.vm_map = {}
        for fqdn, vm in hosts.items():
            host_name = fqdn.split(".", 1)[0]
            self.vm_map[host_name] = vm
        return hosts

    def get_customizers(self):
        customizers = []
        for vm in self.infra.vms.values():
            if isinstance(vm, VM) and vm.customizers:
                customizers.extend(vm.customizers)
        for zone_name, zone in self.zones.items():
            if zone_name == self.config["uplink_zone"]:
                continue
            for vm in zone.vms.values():
                if vm.customizers:
                    customizers.extend(vm.customizers)
            for project in zone.projects.values():
                customizers.extend(project.get_customizers())
        return customizers

    def get_vlan_gen(self):
        used = self.tshell.get_used_vlans(
            dc=self.config["vsphere_datacenter"],
            dvs=self.config["vsphere_dvs"],
        )

        if used is False:
            self.logger.error("Unable to determine available VLANs")
            return None

        r_min, r_max = self.config["vlan_range"]
        avail = range(r_min, r_max)
        avail = [vlan for vlan in avail if vlan not in used]
        random.shuffle(avail)
        self.logger.info(f"Available VLANs: {len(avail)}")

        if not avail:
            self.logger.error(
                "No available VLANs in range: {}".format(
                    self.config["vlan_range"]
                )
            )
            return None

        for vlan in avail:
            yield vlan

    def get_vlan(self, pg_name):
        """
        If the port group exists, returns the current VLAN ID
        Else, returns a currently unused VLAN ID.
        """
        vlan_id = None
        if pg_name:
            vlan_id = self.tshell.get_vlan(network=pg_name)

        if vlan_id:
            self.logger.debug(f"Using existing VLAN: {vlan_id}")
            return vlan_id

        if not self.vlan_gen:
            self.vlan_gen = self.get_vlan_gen()

        vlan_id = next(self.vlan_gen)

        return vlan_id

    def assign_folder(self, zone):
        folder = self.get_folders()[-1]
        self.logger.info(f"[{self.name}] Placing zone {zone.name} in folder {folder}")
        return folder

    def next_instance_supernet(self):
        # Ask the root project to assign the next instance_supernet
        if self.project:
            return self.project.next_instance_supernet()
        else:
            # We are the root project
            try:
                return next(self.instance_supernet_gen)
            except StopIteration as e:
                meta_supernet = self.config["meta_supernet"]
                meta_cidr = self.config["meta_cidr"]
                msg = f"meta_supernet ( {meta_supernet} ) / meta_cidr ( {meta_cidr} )" \
                       " cannot produce enough networks!\n" \
                       " Use a larger (by number of hosts) meta_supernet\n" \
                       " and/or a smaller (by number of hosts) meta_cidr"
                self.logger.error(msg)
                raise(e)

    def get_instance_subnets(self):
        # Return a list of IPV4Networks of length num_instances
        instance_subnets = None
        net = None
        if self.config["instance_network"]:
            # User gave an explicit manual subnet to use
            # (don't auto-subnet)
            net = utils.to_network(self.config["instance_network"])
            # Every instance uses the same ip scheme
            instance_subnets = [net] * self.num_instances

            # Exit early
            return instance_subnets

        elif self.config["instance_supernet"] is None:
            # Use the next availabe instance_supernet
            net = self.next_instance_supernet()
        else:
            # User gave explicity instance_supernet to use
            net = utils.to_network(self.config["instance_supernet"])

        # We still need to compute instance_subnets

        # Each instance gets a unique subnet of instance_supernet
        pre = self.config["instance_cidr"]

        first = self.config["first_instance_subnet"]
        instance_max = self.num_instances + first
        instance_subnets = utils.gen_to_list(
            net.subnets(new_prefix=pre),
            instance_max
        )
        if first:
            # Slice off any subnet we want to skip
            # Ex) 192.168.0.0/16 -> 24 ; skip 1
            #     gives the first instance 192.168.1.0/24 (instead of 192.168.0.0/24)
            # In practice, this convention gives a handy place to put
            #     additional,shared VMs not tied to a particular instance
            instance_subnets = instance_subnets[first:]

        # We probably generated more subnets than we need; drop any extra
        #instance_subnets = instance_subnets[:self.num_instances]

        if not instance_subnets or len(instance_subnets) < self.num_instances:
            raise ValueError(
                "{} subnets are available, but {} instances were requested." \
                " Are instance_supernet and instance_cidr reasonable?".format(
                    len(instance_subnets),
                    self.num_instances
                )
            )

        return instance_subnets

    def _get_network_map(self):
        """
        # Example: 3 instances, 2 zones
        # instance_subnets is like this:
        #   (instance-oriented)
        [subnet("192.168.1.0/24", 25), subnet("192.168.2.0/24", 25), subnet("192.168.3.0/24", 25)] 

        # We transform it to a zone-oriented view like this:
        {
            "LAN":["192.168.1.0/25", "192.168.2.0/25", "192.168.3.0/25"],
            "DMZ":["192.168.1.128/25", "192.168.2.128/24", "192.168.3.128/24"],
        }
        """
        # Start with a dictionary mapping each zone name to an empty list
        network_map = {
            zone_name : []
            for zone_name in self.zones.keys()
        }

        for instance in self.instance_subnets:
            # For each instance, append to each zone's list in the network_map

            # Do the subnet computation (e.g. split /24 -> /25)
            # (Get all zone_networks needed for this instance)

            # Only assign auto-subnets where the user did not give an explity zone_network
            zones_needing_subnets = [
                    zone_name for zone_name, zone in self.zones.items()
                    if zone.original_config["zone_network"] is None
                    and zone_name != self.config["uplink_zone"]
            ]
            self.logger.error(f"zones needing subnets: {zones_needing_subnets}")
            num_subnets = len(
                zones_needing_subnets
            )
            gen = utils.subnets(instance, num_subnets)

            # Only walk the portion of the generator we need
            zone_networks = utils.gen_to_list(gen, num_subnets)

            # zone_networks is like:
            #   ["192.168.1.0/25", "192.168.128.0/25"]
            #          (LAN)              (DMZ)

            # Create a network map, assigning the computed subnets as needed
            index = 0
            for zone_name, zone in self.zones.items():
                zone_network = zone.original_config["zone_network"]
                # '' => DHCP outside of user's control; probably WAN
                if zone_network is None and zone_name != self.config["uplink_zone"]:
                    # No manual network => give it an subnet
                    zone_network = zone_networks[index]
                    index += 1

                # Assign this zone_network to the current instance of this zone
                network_map[zone_name].append(zone_network)

        return network_map
        
    def assign_zone_networks(self, zone):
        # Called when a zone wasn't given explicit networking info
        #   and it must be inferred / computed from project options
        zone_name = zone.config["zone_name"]
        self.logger.warning(f"{self.name} | Assigning {self.network_map[zone_name]}")
        return self.network_map[zone_name]

    def merge_zones(self, zone_name, zone=None, zone_config=None):
        dup_warning = "[WARNING] Merging duplicate zones: {}"
        self.logger.warning(dup_warning.format(zone_name))
        if not zone:
            zone = Zone(self, config=zone_config, name=zone_name)
        for vm_name, vm in zone.vms.items():
            self.zones[zone_name].add_vm(vm_name, vm)

    def add_zone(self, zone_name, zone_config, uplink=None):
        if zone_name in self.zones:
            self.merge_zones(zone_name, zone_config=zone_config)
            return

        if uplink is None:
            zone = Zone(self, config=zone_config, name=zone_name)
        else:
            zone = UplinkZone(self, config=zone_config, name=zone_name, uplink=uplink)
        new_name = zone.name
        if zone_name != new_name:
            self.logger.info(f"[{self.name}] renamed Zone {zone_name} -> {new_name}")
            # Fix up internal state to match new name
            zone.config["zone_name"] = new_name
            zone.reset_instance_names()
            zone.base_name = new_name
            zone._base_name = new_name

            if new_name in self.zones:
                self.merge_zones(new_name, zone=zone)
            else:
                self.zones[new_name] = zone
        else:
            self.zones[zone_name] = zone
    
        self.logger.info(f"Zone {zone_name} added")

    def get_projects(self, internal=False):
        projects = [self]
        if not internal:
            self.unvisit()
        self.visited = True

        for zone in self.zones.values():
            if zone.visited:
                continue
            zone.visited = True
            for project in zone.projects.values():
                if project.visited:
                    continue
                project.visited = True
                projects.extend(project.get_projects(internal=True))

        return projects

    def get_zones(self, internal=False):
        """
        zones = []
        uplink_zone = self.config["uplink_zone"]
        if not internal:
            zones.append(self.zones[uplink_zone])
        
        for zone_name, zone in self.zones.items():
            if zone_name == uplink_zone:
                continue
            zones.append(zone)
            for project in zone.projects.values():
                zones.extend(project.get_zones(internal=True))
        return zones
        """
            
        zones = []
        if not internal:
            self.unvisit()
        self.visited = True

        for zone in self.zones.values():
            if zone.visited:
                continue
            zone.visited = True
            zones.append(zone)
            for project in zone.projects.values():
                if project.visited:
                    continue
                project.visited = True
                zones.extend(project.get_zones(internal=True))

        return zones

    def get_infras(self):
        infra = []
        for project in self.get_projects():
            infra.append(project.infra)
        return infra
    
    def get_routes(self):
        route_summary = {}
        for infra in self.get_infras():
            for info in infra.instances:
                routes = {}
                for route_name, route in info["guestinfo"]["gateway_routes"].items():
                    routes[route_name] = {
                        route["via"] : route.get("routes", None)
                    }

                route_summary[info["vsphere_name"]] = routes
        return route_summary

    def get_vms(self, internal=False, recurse=True):
        vms = []

        vms.extend(self.infra.vms.values())
        self.visited = True

        for zone_name, zone in self.zones.items():
            if zone_name == self.config["uplink_zone"]:
                continue
            vms.extend(zone.vms.values())
            if not recurse:
                continue
            for project in zone.projects.values():
                vms.extend(project.get_vms(internal=True))
                
        return vms


    def get_vm_tags(self, vm_name):
        # Keep the cache at the root level
        if self.project:
            return self.project.get_vm_tags(vm_name)

        # Check the cache first
        if vm_name in self.vm_tags:
            tags = self.vm_tags[vm_name]
            return tags

        # Cache the result for next time
        tags = self.tshell.get_vm_tags(
            vm_name=vm_name
        )
        
        self.vm_tags[vm_name] = tags
        return tags

    def get_network_map(self):
        # Make a copy of newtwork_map with all string values
        network_map = {
            zone: [net if net is None else str(net) for net in networks]
            for zone, networks in
            self.network_map.items()
        }
        return network_map

    def summary(self, vsphere_only=False, color=True):
        if vsphere_only:
            if not self.project:
                # We are root
                summary = f"""
-------------------------------------------------------------------------------
vSphere Details
-------------------------------------------------------------------------------
{self.config["vsphere_server"]}
    vsphere_datacenter      : {self.config["vsphere_datacenter"]}
    vsphere_datastore       : {self.config["vsphere_datastore"]}
    vsphere_resources_pool  : {self.config["vsphere_resource_pool"]}
    vsphere_compute_cluster : {self.config["vsphere_compute_cluster"]}
    vsphere_dvs             : {self.config["vsphere_dvs"]}
"""
            return summary

        network_map = self.get_network_map()
        network_map = utils.dumps(network_map, prefix=" "*8)

        instance_subnets = [str(net) for net in self.instance_subnets]
        #folders = utils.dumps(self.get_folders(), prefix=" "*8)
        summary = f"""
-------------------------------------------------------------------------------
Project
-------------------------------------------------------------------------------
{self.name}
    group_name          : {self.config["group_name"]}
    num_instances       : {self.num_instances}
    instance_subnets    : {instance_subnets}
    folder              : {self.get_folders()[-1]}
    zones               : {list(self.zones)}
    network_map         :
{network_map}
-------------------------------------------------------------------------------
"""
        if color:
            summary = utils.project_summary(summary)
        return summary

