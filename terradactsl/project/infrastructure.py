import os
import copy

import terrascript
import terrascript.data
import terrascript.resource

from terradactsl import opts as opts
from terradactsl import Configurable
from terradactsl import utils
from terradactsl import VM

class Infrastructure(Configurable):
    """
    Class controlling the creation of project-level infrastructure.

    Invoked after the project is rendered and all values are known.
    Pre-computes resource names; the entire project should depend on them

    Behavior can be overriden by optional project-level 'infra' key.
    """

    opts = opts.INFRASTRUCTURE_OPTS
    attributes = ["vms", "config"]
    # Unknown keys are passed directly to VMs
    unknown_key = "vm_config"

    def __init__(self, project, **kwargs):
        kwargs["name"] = f"{project.name}-Infra"
        self.inherit_logger_settings(project, kwargs)
        super().__init__(**kwargs)
        self.project = project
        self.vms = {}
        self._summary = None
        project_attrs = [ 
            "datacenter_id",
            "first_instance",
            "log_level",
            "num_instances",
            "resolve_hosts"
        ]
        self.inherit(project_attrs, self.project)
        self.init_logger()
        self.original_config = self.config.copy()
        if self.config["create_folder"] is None:
            if self.project.project is None:
                # We are root infra
                self.config["create_folder"] = True
            else:
                # We are subinfra
                self.config["create_folder"] = self.project.config["create_subinfra_folders"]
        
        self._resources = []
        self.fw_names = []
        self.infra_folders = []
        self.vapp_id = None

        self.wan_ips = []
        self.render_index = -1

        mutually_exclusive = ["fw_template", "fw_template_tags"]
        if not utils.check_mutually_exclusive(self.config, mutually_exclusive):
            msg = f"Exactly one of {mutually_exclusive} must be given!"
            self.logger.error(msg)
            raise Exception(msg)


    def prep_resources(self, curr_instance):
        # Pretend we are rendering
        self.render_index += 1
        # Pre-compute + register the names of any resources we will create
        self._prep_instance_resources(curr_instance, add_resources=True)
        
        self.logger.debug(f"{self.name} resources:{self.resources} {self._resources}")
        self.logger.debug(f"{self.name}: wan_ips: {self.wan_ips}")

    def get_wan_ip(self, curr_instance):
        uplink_zone, uplink_info = self.get_uplink_info()

        # We are just a host on WAN
        self.logger.debug(f"[{self.name}] Getting WAN from {uplink_zone.name}: wrapping {curr_instance}")

        # TODO: Verify
        """
        curr = uplink_zone.project.wrap_curr_instance(curr_instance)
        self.logger.debug(f"Wrapped {curr}: {uplink_zone.instances[curr]}")
        zone_hosts = uplink_zone.instances[curr]["zone_hosts"]
        """
        zone_hosts = uplink_info["zone_hosts"]
        ip_addr = None
        if zone_hosts == "":
            self.logger.debug(f"{self.project.name} {uplink_zone.name} has no zone_hosts")
            return ""

        exclude_hosts = uplink_info["exclude_hosts"]
        for ip_addr in zone_hosts:
            ip_addr = str(ip_addr)
            if ip_addr not in exclude_hosts:
                break

        self.logger.debug(f"Chose WAN IP: {ip_addr}")
        return str(ip_addr)

    def compute_wan_ips(self, curr_instance):
        # wan_ips aren't known until render time
        ip_addr = self.get_wan_ip(curr_instance)
        ip_addr = str(ip_addr)
        self.wan_ips.append(ip_addr)

        self.update_ip(curr_instance, ip_addr)

    def update_ip(self, curr_instance, ip_addr):
        # Update the fake vm in self.vms so other firewalls
        #   can sucessfully resolve_hosts
        fw_name = self.fw_names[curr_instance]
        vm_info = self.vms[fw_name]
        if isinstance(vm_info, dict):
            self.logger.debug(f"{self.name} | {fw_name} | UPDATE IP: {ip_addr}")
            vm_info["ipv4_address"] = ip_addr

    def _prep_instance_resources(self, curr_instance, add_resources=False):
        self.name = f"{self.project.name}-Infra"
        self.curr_instance = curr_instance
        # self.wrap_curr_instance(curr_instance)
        curr = self.curr_instance
        if curr is None:
            curr = 0

        self.logger.debug(f"Prepping {self.name} resources...")
        if self.config["create_fw"]:
            fw_name = None
            fqdn = None
            if not add_resources:
                # We previously computed the correct name
                fw_name = self.fw_names[self.render_index]
                fqdn = fw_name
            else:
                fw_name = self.original_config["fw_name"]
                fqdn = f"{fw_name}.{self.project.domain}"

            resource_name = utils.get_resource_name(
                fqdn,
                prefix=f"fw_",
            )
            # curr_instance=curr_instance

            """
            # wan_ips aren't known until render time
            if add_resources:
                ip_addr = None
                self.wan_ips.append(ip_addr)
            else:
                ip_addr = self.get_wan_ip()
                self.wan_ips[curr] = str(ip_addr)
            """
            ip_addr = None
            if add_resources:
                self.fw_names.append(fqdn)
            else:
                ip_addr = self.wan_ips[curr]

            self.vms[fqdn] = {
                "host_name"         : fw_name,
                "domain"            : self.project.domain,
                "fqdn"              : fqdn,
                "aliases"           : [],
                "ipv4_address"      : ip_addr,
                "parent_project"    : self.project.name,
                "root_project"      : self.project.root_project,
                "role"              : "firewall"
            }
            if add_resources:
                self.instances.append(self.vms[fqdn])

            self.config["fw_resource_name"] = resource_name
            if add_resources:
                self.add_resource(resource_name, "vsphere_virtual_machine")
            
            if not self.original_config["fw_vsphere_name"]:
                self.config["fw_vsphere_name"] = utils.get_fw_vsphere_name(self, fw_name)

        folders = self.project.info["folders"]
        self.logger.info(f"[{self.name}] Folders: {folders}")
        index = self.project.restored_index
        if index is None:
            index = -1
        self.config["folder_path"] = folders[index]
        self.logger.info(f"[{self.name}] Chose: {folders[index]}")

        if self.should_create_folder():
            resource_name = utils.get_resource_name(
                self.project.name,
                prefix="infra_folder_",
            )
            #curr_instance=curr_instance
            self.config["folder_resource_name"] = resource_name
            if add_resources:
                self.add_resource(resource_name, "vsphere_folder")

        # Used by VMs that think we are a Zone
        self.config["instance_name"] = self.name

    def should_create_folder(self):
        return self.config["create_folder"] \
            and not self.project.config["create_vapp"]

    def _get_dhcp_ranges(self):
        curr = self.curr_instance
        if curr is None:
            curr = 0
        dhcp_ranges = []
        for zone in self.project.zones.values():
            if not zone.config["dhcp"]:
                dhcp_ranges.append(None)
                continue
            info = zone.instances[curr]
            if info["zone_network"] == "":
                # The zone gets its DHCP from somewhere else (probably uplink)
                # Therefore, we can't start a DHCP here.
                dhcp_ranges.append(None)
                continue
                
            # Skip network address and gateway
            first = 2 + zone.config["exclude_first"]
            # Start with the full range build DHCP
            dhcp_range = [
                info["zone_network"][first],
                # -2 for second-to-last IP (skip broadcast)
                info["zone_network"][-2]
            ]
            static = info["static_assignments"]
            if static:
                # We have mixed assingments,
                # start DHCP after largest static IP
                max_ip = max(static)
                dhcp_range[0] = max_ip + 1
                if dhcp_range[0] in static:
                    raise ValueError(
                        f"{dhcp_range[0]} cannot be statically assigned" \
                        " if DHCP is enabled in zone {zone.config['zone_name']}"
                    )
            dhcp_range = [str(dhcp_range[0]), str(dhcp_range[1])]
            dhcp_ranges.append(dhcp_range)

        return dhcp_ranges

    # pfSense client-side validation rejects non-alphanumeric names,
    # but we write them directly to the config file and it works fine ;p
    def flatten_route(self, route_name, zone_name, route):
        flattened = {}
        base_name = route_name
        """
        if not isinstance(route, dict):
            if not isinstance(route, list):
                route = [route]
            route_name = zone_name
            if base_name:
                route_name = f"{base_name}.{zone_name}"
            flattened[route_name] = route
            return flattened
        """
        self.logger.debug(f"Processing: {route_name} | {zone_name}")
        if None in route:
            direct = route[None]["zone_network"]
            if not isinstance(route, list):
                direct = [direct]
            direct_name = zone_name
            if base_name:
                direct_name = f"{base_name}.{zone_name}"
            flattened = {direct_name: direct}

        self.prep_route(route)

        for route_name, subroute in route.items():
            if not isinstance(subroute, dict):
                # We got the end of the route;
                #   this project is directly attached
                if not route_name in flattened:
                    flattened[route_name] = []
                flattened[route_name].append(subroute)
                self.logger.info(f"{route_name}: {subroute}")
                continue
            self.prep_route(subroute)
            
            """
            if base_name:
                route_name = f"{base_name}.{route_name}"
            """
            # The route continues; recursively expand
            #self.logger.debug(f"Flattening {base_name} subroute: {subroute}")
            for zone_name, subroute in subroute.items():
                flattened.update(
                    self.flatten_route(
                        route_name,
                        zone_name,
                        subroute
                    )
                )

        return flattened
        

    def format_route(self, project_name, via_zone_name, route):
        """
        Example output:
        {
            "MyRoute":{
                "via"   : "X.X.X.X",
                "zone"  : "LAN",
                "routes : ["Y.Y.Y.Y/Y", "Z.Z.Z.Z/Z"]
            },
            ...
        }

        """
        formatted = {} 

        flattened = self.flatten_route("", "", route)
        #self.logger.debug(f"Flattened: {flattened}")
        for route_name, routes in flattened.items():
            via_project = project_name
            if route_name:
                #via_project = route_name.split(".")[0]
                via_project = route_name.rsplit(".", 1)[0]
            self.logger.warning(f"route_name: {route_name}")
            self.logger.warning(f"via_project: {via_project}")
            via = self.project.get_wan_ip_by_name(via_project)
            self.logger.info(f"via {via} @ {via_project} -> {routes}")

            formatted[route_name] = {
                "via"       : via,
                "zone"      : via_zone_name,
                "routes"    : routes
            }
            #self.logger.warning(f"{route_name}: {formatted[route_name]}")

        #self.logger.debug(f"Formatted route: {formatted}")
        return formatted

    def prep_route(self, route):
        """
        Deletes any internal keys from a project subgraph that
            are not needed to compute the route
        """
        internal_keys = [None, "__vms__", "__fw__"]
        for key in internal_keys:
            if key in route:
                self.logger.debug(f"Ignoring internal key {key}")
                del route[key]

    def get_gateway_routes(self):
        """
        curr = self.curr_instance
        if curr is None:
            curr = 0
        """

        routes = {}
        # Get a map of zone_name -> list of reachable networks
        #project_routes = self.project.get_project_routes()
        #project_routes = self.project.project_graph[self.project.curr_instance]
        project_routes = self.project.project_graph

        if not self.project.project:
            # If the root project has multiple instances,
            #   don't try to route between them
            project_routes = {self.project.name: project_routes[self.project.name]}

        project_routes = copy.deepcopy(project_routes)
        #self.logger.debug(f"[{self.project.name}] project_routes:")
        #self.logger.debug(utils.sanitize_config(project_routes))
        
        for project_name, route_blob in project_routes.items():
            self.prep_route(route_blob)
            for zone_name, route in route_blob.items():
                self.prep_route(route)
                self.logger.debug(f"Adding Route {zone_name}")
                res = self.format_route(project_name, zone_name, route)

                routes.update(res)

        self.logger.debug(f"[{self.project.name}] Routes: {routes}")
        return routes

    def get_uplink_info(self):
        # TODO: Verify
        uplink_name = self.project.config["uplink_zone"]
        uplink_zone = self.project.zones[uplink_name]
        # TODO: Verify
        uplink_info = self.get_zone_info(uplink_zone)
        self.logger.debug(f"[{self.name}] {uplink_info}")
        return uplink_zone, uplink_info

    def get_zone_info(self, zone):
        curr = self.project.restored_index
        # TODO: Sometimes needed, sometimes not
        if len(self.project.instances) > len(zone.instances):
            denom = len(self.project.instances) / len(zone.instances)
            curr = zone.wrap_curr_instance(curr, denom=denom)

        return zone.instances[curr]

    def _get_guestinfo(self):
        # curr = self.curr_instance
        curr = self.project.info["instance_index"]
        if curr is None:
            curr = 0

        uplink_zone, uplink_info = self.get_uplink_info()
        uplink_gateway = uplink_info["zone_gateway"]

        # Compute the guestinfo block for this firewall instance
        guestinfo = {
            "zone_names"      : None,
            "ipv4_addresses"  : None,
            "ipv4_netmasks"   : None,
            "ipv4_gateways"   : None,
            "dns_servers"     : None,
            "hosts"           : {},
            "outbound_nat"    : None,
            "dhcp_ranges"     : None,
            "gateway_routes"  : {},
        }
        # host_name omitted so value from vm_config is used
        # customization_type omitted so VM computes it from tags

        # Let the user override any of these settings if they really want to
        advanced = self.config["guestinfo"]
        if advanced is None:
            advanced = {}
        """
        advanced = {
            key:val
            for key, val in self.config.items()
            if key in guestinfo and self.config[key] is not None
        }
        """
        guestinfo.update(advanced)


        # Apply defaults everywhere the user did not override
        """
        if "domain" not in advanced:
            guestinfo["domain"] = self.project.name
        """
        if "zone_names" not in advanced:
            guestinfo["zone_names"] = list(self.project.zones)

        # Each firewall has len(zones)-many IPs
        if "ipv4_addresses" not in advanced:
            guestinfo["ipv4_addresses"] = []
            self.logger.debug(f"{self.project.name} has zones {list(self.project.zones)}")
            for zone_name, zone in self.project.zones.items():
                ip_addr = None
                if zone == uplink_zone:
                    ip_addr = self.wan_ips[curr]
                    self.logger.debug(f"{self.name} is a host on {zone_name}: {ip_addr}")
                    self.update_ip(curr, ip_addr)
                else:
                    # We are the gateway everwhere else
                    zone_gateway = zone.instances[curr]["zone_gateway"]
                    self.logger.debug(f"{self.name} is the gateway ({zone_gateway}) for {zone_name}")
                    ip_addr = str(zone_gateway)

                guestinfo["ipv4_addresses"].append(ip_addr)

        if "ipv4_netmasks" not in advanced:
            guestinfo["ipv4_netmasks"] = []
            for zone in self.project.zones.values():
                zone_network = None
                if zone == uplink_zone:
                    zone_network = uplink_info["zone_network"]
                else:
                    zone_network = zone.instances[curr]["zone_network"]
                cidr = ""
                if zone_network:
                    cidr = zone_network.prefixlen
                # cidr = '' => dhcp
                guestinfo["ipv4_netmasks"].append(cidr)


        if "dns_servers" not in advanced:
            if uplink_gateway:
                guestinfo["dns_servers"] = [str(uplink_gateway)]
            # else, dns comes from DHCP
        
        if "hosts" not in advanced:
            guestinfo["hosts"] = {}

        # Always generate the map so the project has it as an attr
        dns_map = self.project.get_dns_map()
        if self.config["resolve_hosts"]:
            guestinfo["hosts"] = dns_map
            self.logger.debug(f"[{self.project.name}] Hosts:\n" + utils.dumps(guestinfo["hosts"]))

        if "outbound_nat" not in advanced:
            # Root project NATs, the rest don't
            if self.project.project is None:
                guestinfo["outbound_nat"] = True
            else:
                guestinfo["outbound_nat"] = False

        if "dhcp_ranges" not in advanced:
            guestinfo["dhcp_ranges"] = self._get_dhcp_ranges()

        if "gateway_routes" not in advanced and self.config["route_projects"]:
            guestinfo["gateway_routes"] = self.get_gateway_routes()

            # Make uplink route default
            self.logger.debug(guestinfo["gateway_routes"])

            guestinfo["gateway_routes"]["default"] = {
                "via"   : str(uplink_gateway),
                "zone"  : self.project.config["uplink_zone"],
            }


        if "ipv4_gateways" not in advanced and "default" in guestinfo["gateway_routes"]:
                # Apply default gateway to WAN
                ipv4_gateways = [None] * len(guestinfo["ipv4_addresses"])
                # First IP must be WAN
                ipv4_gateways[0] = "default"
                guestinfo["ipv4_gateways"] = ipv4_gateways

        # customization type uses VM default behavior (use tags) 
        return guestinfo

    # Functions a VM exepcts a Zone to have
    def assign_network(self, vm):
        return {}

    def assign_folder(self, vm):
        return self.config["folder_path"]

    def _prep_instance(self, curr_instance):
        self._prep_instance_resources(curr_instance)
        
        # TODO: Option to put firewalls in instance folders
        if self.config["create_folder"]:
            self.config["folder_path"] = os.path.join(
                self.config["folder_path"],
                self.config["folder_name"],
            )

        self.infra_folders.append(self.config["folder_path"])

        """
        self.config["instance_name"] = utils.get_zone_instance_name(
                self,
                self.curr_instance
        )
        """

        self.guestinfo = self._get_guestinfo()

    def _render_resources(self, _tf):
        if self.should_create_folder():
            resource_name = self.config["folder_resource_name"]
            _tf += terrascript.resource.vsphere_folder(
                resource_name,
                depends_on    = self.get_dependencies(),
                path          = self.config["folder_path"],
                type          = "vm",
                datacenter_id = self.config["datacenter_id"]
            )
            self.add_resource(resource_name, "vsphere_folder")

    def _render_fw(self, _tf):
        curr = self.project.info["instance_index"] #self.curr_instance

        port_groups = []
        network_ids = []
        uplink_zone, uplink_info = self.get_uplink_info()
        for zone_name, zone in self.project.zones.items():
            info = None
            if zone == uplink_zone:
                info = uplink_info
            else:
                info = zone.instances[curr]

            #self.logger.debug(f"!! {zone_name} #{curr}: {zone.instances}")

            port_groups.append(info["port_group"])
            network_ids.append(info["network_id"])

        self.logger.warning(f"[{self.project.name}]: {utils.dumps(port_groups)}")
        host_name = self.config["fw_name"].split(".", 1)[0]
        fqdn = f"{host_name}.{self.project.domain}"

        attached_to = [self.get_zone_info(zone)["zone_name"] for zone in self.project.zones.values()]
        fw_config = {
            "resource_name"     : self.config["fw_resource_name"],
            "vsphere_name"      : self.config["fw_vsphere_name"],
            "vm_name"           : host_name,
            "host_name"         : host_name,
            "guestinfo"         : self.guestinfo,
            "attached_to"       : attached_to,
            "port_group"        : port_groups,
            "network_id"        : network_ids,
            "ipv4_address"      : self.guestinfo["ipv4_addresses"],
            "ipv4_netmask"      : self.guestinfo["ipv4_netmasks"],
            "ipv4_gateway"      : self.guestinfo["ipv4_gateways"],
            "role"              : "firewall"
        }
        if self.config["fw_template"]:
            fw_config["template"] = self.config["fw_template"]
        else:
            fw_config["template_tags"] = self.config["fw_template_tags"]

        if self.config["vm_config"]:
            fw_config.update(self.config["vm_config"])

        self.instances[self.render_index] = fw_config
        fw = VM(self.project, config=fw_config, name=host_name)
        # Prevent the firewall from depending on itself or any other 'peer' VMs
        exclude = ["vsphere_virtual_machine"]
        fw.instance_depends_on(self, exclude=exclude)

        self.vms[fqdn] = fw

        self.logger.info(f"[{self.project.name} Infra] Rendering {host_name} as {self.config['fw_resource_name']}")
        # Temporarily impersonate a zone so the VM names things nicely
        infra_name = self.name
        self.name = uplink_info["zone_name"]

        res = fw.render(_tf, self, curr, color=False)
        # Restore real name
        self.name = infra_name
        return res

    def reset_render_index(self):
        self.render_index = -1

    def render(self, _tf, curr_instance):
        self.root_project = self.project.root_project
        # VM expects "zone" to have this key
        self.config["domain"] = self.project.domain
        # Number of firewalls we are going to build
        self.render_index += 1
        #self.restored_index = self.project.restored_index
        self.logger.info(f"Rendering infrastructure for Instance {curr_instance} ({self.render_index})")
        self._prep_instance(curr_instance)
        self._render_resources(_tf)

        fw_summary = self._render_fw(_tf)
        #self.project.restore_info(self.restored_index)
        summary = self.summary()
        summary += utils.project_summary(fw_summary)
            
        return summary

    def summary(self):
        summary = f"""
-------------------------------------------------------------------------------
{self.project.name} Infrastructure
-------------------------------------------------------------------------------
    num_instances       : {self.project.num_instances}
    infra_folder        : {self.infra_folders[-1]}
-------------------------------------------------------------------------------
"""
        return utils.project_summary(summary)

