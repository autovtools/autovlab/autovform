ZONE_OPTS = { 
    "zone_name": {
        "required": True,
        "description": "The name of this zone.",
    },  
   "zone_network":{
        "required": False,
        "description": "The ipv4_network used by this zone, given as X.X.X.X/Y"
                       " Needed if a Zone is not instanced.",
    },
    "dhcp": {
        "required": False,
        "description": "If true, this zone uses DHCP to assign IP addresses."
                       " (True => our project will run a DHCP server for zone_network)",
                       " Default: False (use static IPs)"
        "default": False
    },
    "zone_hosts": {
        "required": False,
        "description": "[ADVANCED] List of IP addresses to use for the hosts in this zone, instead of default assignment.",
    },
    "exclude_hosts": {
        "required": False,
        "description": "List of IP addresses to exclude from default (in-order) assignment.",
        "default": []
    },
    "exclude_first":{
        "required": False,
        "description": "Number of IPs to discard from the beginning of the zone network."
                       " Used with automatic IP assignments." " e.g. exclude_first: 10 => start assigning IPs at the 11th usable host.", "default": 0
    },
    "first_instance": {
        "required": False,
        "description": "The human-readable first instance number."
                       " e.g. 1 => Instance-01 is first;"
                       " 0 => Instance-00 is first"
                       " default: Inherit from project",
    },
    "zone_gateway":{
        "required": False,
        "description": "The gateway IP for hosts in this zone.",
    },
    "dns_servers": {
        "required": False,
        "description": "[ADVANCED] List of dns servers that hosts in this zone should use."
                       " If missing, zone_gateway is used as the dns_server. "
                       " Can be overriden by VMs.",
    },
    "create_zone_folders":{
        "required": False,
        "description": "If given, override the default value given by project.create_zone_folders.",
    }, 
    "merge_zone_folders":{
        "required": False,
        "description": "If given, override the default value given by project.merge_zone_folders."
    },  
    "zone_folder":{
        "required": False,
        "description": "Alternate folder name to use. default: zone_name",
    },
    "port_group":{
        "required": False,
        "description": "Name of the existing port group to use for this zone."
                       " If not provided, a new port group will be created for this zone."
    },
    "log_level": {
        "required": False,
        "description": "The string log level used by logging module to control log verbosity. [DEBUG, INFO, WARNING, ERROR, CRITICAL]"
    },

    "domain":{
        "required": False,
        "description": "Zone-level default domain name. Can be overriden by VMs. Defaults to Project-level domain name"
    },


    "vms":{
        "required": False,
        "description": "JSON object containing VM configs."
    },
    "projects": {
        "required": False,
        "description": "JSON object containing sub-project configs."
    },
    "instance_names": {
        "required": False,
        "description": "TODO",
        "default":"none"
    },

    "resolve_hosts": {
        "required": False,
        "description": "If true, DNS entries for all VMs"
                       " with static IPs are added to dns_hosts."
                       " Default: inherit from project"
    },

    "default_name": {
        "required":False,
        "description": "[ADVANCED] Default value for Zone",
        "default": "LAN"
    },

    "create_vapp": {
        "required": False,
        "description": "If true, create a vApp container containing the project's VMs"
                       " Requires vSphere DRS to be on."
                       "Default: Inherit from project",
    },  
}
