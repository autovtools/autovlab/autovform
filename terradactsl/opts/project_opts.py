PROJECT_OPTS = { 
    "project_name": {
        "required": True,
        "description": "Name of this project."
    },
    "group_name": {
        "required": False,
        "description": "The group / category containing this project. Used for naming / orgnazational purposes."
                        " default: PTDSL (PyTerraDactSL)",
        "default": "PTDSL"
    },
    "num_instances": {
        "required": False,
        "description": "The number of network instances to build within this project."
                       " e.g. num_instances : 2 => create 2 instances of each Zone",
    },
    "instance_names": {
        "required": False,
        "description": "TODO",
    },
    "vsphere_server": {
        "required": True,
        "description": "FQDN of the vSphere / vCenter instance (e.g. vcenter.localdomain).",
    },  
    "insecure_connection": {
        "required": False,
        "description": "Allow insecure connections (e.g. self-signed certs), default: True",
        "default": True
    },  
    "vsphere_user": {
        "required": True,
        "description": "Admin vSphere user account to use.",
    },  
    "vsphere_password": {
        "required": True,
        "description": "Admin vSphere user password to use.",
    },  
    "vsphere_datacenter": {
        "required": True,
        "description": "Name of the vSphere datacenter (e.g. MyDC)",
    },  
    "vsphere_datastore": {
        "required": True,
        "description": "Name of the datastore within vsphere_datacenter where VMs should be deployed (e.g. MyDatastore)",
    },  
    "vsphere_resource_pool": {
        "required": False,
        "description": "Name of the vsphere_resource_pool to use (e.g. MyCluster/Resources)."
                       " Must specify either vsphere_resource_pool or vsphere_compute_cluster, not both."
                       " See https://www.terraform.io/docs/providers/vsphere/d/resource_pool.html",
    },  
    "vsphere_compute_cluster": {
        "required": False,
        "description": "Name of the vsphere_compute_cluster to use (e.g. MyCluster)."
                       " Must specify either vsphere_resource_pool or vsphere_compute_cluster, not both."
                       " See https://www.terraform.io/docs/providers/vsphere/d/compute_cluster.html",
    },  
    "vsphere_dvs": {
        "required": False,
        "description": "Name of the Distributed Virtual Switch to use when creating port_groups (e.g. vMySwitch",
    },  

    # group variables
    "parent_folder": {
        "required": True,
        "description": "The parent VM folder to place this project. Must already exist."
    },  

    "uplink": {
        "required": False,
        "description": "Name of the port group to use as a WAN uplink."
                       " DHCP is assumed to be available on this network, and" 
                       " leases should not conflict with IP assignments in this project."
                       " This port group is the 'WAN' interface of the project's firewall."
                       " Using '' (empty string) as the uplink => create a new, isolated port group"
                       " that acts as an 'air gap'."
    },
    "uplink_zone": {
        "required": False,
        "description": "[ADVANCED] The name of the Zone (defined in 'zones') that represents the 'WAN' zone"
                       " Used when 'uplink' is not set."
                       " This zone is the 'WAN' network for the project's firewall."
                       " Any static IP assignments should be compatible with any separately deployed"
                       " projects attached to the same port group."
                       " If uplink is not set but a zone named 'WAN' is defined,"
                       " uplink_zone = WAN is assumed"
    },


    "create_project_folder": {
        "required": False,
        "description": "If true, create a subfolder under parent_folder to hold this project. default: True.",
        "default": True,
    },  
    "create_instance_folders": {
        "required": False,
        "description": "If true, create a subfolder under project_folder for each instance."
                       " Default: True.",
        "default": True,
    },
    "create_zone_folders": {
        "required": False,
        "description": "If true, create a subfolder under project_folder for each instance being built."
                       " Can be overriden at the Zone-level."
                       " Default: False.",
        "default": False,
    },
    "merge_zone_folders":{
        "required": False,
        "description": "If true (and create_zone_folders is true), create *one* zone_folder for each Zone,"
                       " instead of one folder for each *instance* of a Zone."
                       " Can be overriden at the Zone-level."
                       " Default: False",
        "default": False
    },
    "vlan_range": {
        "required": False,
        "description": "Lower (inclusive) and upper (not inclusive) bounds to use for generated VLAN ids. default: [1024, 4000].",
        "default": [1024, 4000],
    },
    "meta_supernet": {
        "required": False,
        "description": "Each instance_supernet is computed as a subnet of this CIDR block. default: 172.22.0.0/13)",
        "default":"172.24.0.0/13"
    },
    "meta_cidr": {
        "required": False,
        "description": "TODO",
        "default": 16
    },
    "meta_overlap": {
        "required": False,
        "description": "TODO",
        "default": False
    },
    "instance_supernet": {
        "required": False,
        "description": "Each instance_subnet is computed as a subnet of this CIDR block.",
    },
    "instance_cidr": {
        "required": False,
        "description": "The CIDR number / prefix length for subnets of instance_supernet. default: 24",
        "default": 24
    },
    "first_instance_subnet": {
        "required": False,
        "description": "The index of the first instance_subnet to use (0-based). default: 1 (skip the first subnet)",
        "default": 1
    },
    "first_instance": {
        "required": False,
        "description": "The human-readable first instance number."
                       " e.g. 1 => Instance-01 is first;"
                       " 0 => Instance-00 is first"
                       " default: 1",
        "default": 1
    },
    "instance_network": {
        "required": False,
        "description": "If given, every instance uses this subnet, instead of a unique subnet of instance_supernet.",
    },
    "log_level": {
        "required": False,
        "description": "The string log level used by logging module to control log verbosity. [DEBUG, INFO, WARNING, ERROR, CRITICAL]"
    }, 

    "vm_user": {
        "required": False,
        "description": "Default Guest OS username to use for guest authentication. Can be overridden at the VM level"
                       "(Note: this user must already exist on the source template)",
    },
    "vm_password": {
        "required": False,
        "description": "Guest OS password to use for guest authentication. Can be overridden at the VM level."
                       "(Note: this must be the password set on the guest template )",
    },
    "domain": {
        "required": False,
        "description": "Project-level default domain name. Can be overriden by Zones or VMs. Defaults to project_name."
    },

    "zones": {
        "required": False,
        "description": "JSON object containing configurations for the Zones"
    },
    "vms": {
        "required": False,
        "description": "JSON object containing VM configs. If used, a default Zone configuration is assumed"
    },
    "projects": {
        "required": False,
        "description": "JSON object containing Project configs. If used, a default Zone configuration is assumed"
    },
    "create_subinfra_folders": {
        "required": False,
        "description": "If true, subprojects will not create a folder for infrastructure resources (e.g. the firewall)"
                       " Default: True",
        "default": True
    },
    "infra": {
        "required": False,
        "description": "[ADVANCED] Configuration for Project Infrastructure.",
    },
    
    "resolve_hosts": {
        "required": False,
        "description": "If true, DNS entries for all project VMs"
                       " with static IPs are added to dns_hosts."
                       "Default: True",
        "default": True
    },

    "create_vapp": {
        "required": False,
        "description": "If true, create a vApp container containing the project's VMs"
                       " Requires vSphere DRS to be on."
                       "Default: True",
        "default": True
    },
    "resource_pool_id": {
        "required": False,
        "description": "[INTERNAL] Raw ID of the resource pool. Do not set manually."
    },
}
