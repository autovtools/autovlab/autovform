INFRASTRUCTURE_OPTS = {
    "fw_name": {
        "required": False,
        "description": "The human-readable name to give the Project Firewall. Default: fw",
        "default": "fw"
    },
    "fw_vsphere_name": {
        "required": False,
        "description": "[ADVANCED] The vSphere name to give the Project Firewall."
                       " Default: computed based on project info."
    },
    "resolve_hosts": {
        "required": False,
        "description": "If true, DNS entries for all project VMs"
                       " with static IPs are added to dns_hosts."
    },
    "dns_servers": {
        "required": False,
        "description": "[ADVANCED] List of DNS servers for the firewall to use."
                       " Default: project_gateway is used as the DNS server, if given."
                       " else, DNS servers must be given via DHCP",
    },
    "dns_hosts": {
        "required": False,
        "description": "[ADVANCED] A dictionary mapping DNS names to ip addresses."
                       "DNS host overrides will be added to perform the DNS resolution"
    },
    "route_projects": {
        "required": False,
        "description": "[ADVANCED] If true, create automatic routes allowing"
                       " this project to reach other parent / subprojects. "
                       " Required for subproject internet acess."
                       " May not function properly if some projects out out of route_projects."
                       " Assumes that IP scheme is valid for such routing."
                       " Default: True",
        "default": True
    },
    "gateway_routes": {
        "required": False,
        "description": "[ADVANCED] A dictionary containing routing information."
    },
    
    "fw_template": {
        "required": False,
        "description": "[ADVANCED] Template to use for the firewall."
                       "It must support the appropriate guestinfo customization."
                       "Default: Selects the most recent (by version) template matching fw_template_tags."
    },
    "fw_template_tags": {
        "required": False,
        "description": "[ADVANCED] The tags used to search for a firewall template",
        "default":{
            "os_variant": "FwProd"
        }
    },


    "create_folder": {
        "required": False,
        "description": "If true, create a folder for intrastructure resources."
                       "Default: True",
    },
    "folder_name": {
        "required": False,
        "description": "Name of the vSphere folder to create for infrastructure resources."
                       " Default: _INFRA_",
        "default": "_INFRA_"
    },
    "create_fw": {
        "required": False,
        "description": "[ADVANCED] If true, create a firewall that connects the zones in the this project"
                       "Default: True",
        "default": True
    },
    "first_instance": {
        "required": False,
        "description": "The human-readable first instance number."
                       " e.g. 1 => Instance-01 is first;"
                       " 0 => Instance-00 is first"
                       " default: inherit from project",
    },

    "guestinfo": {
        "required": False,
        "description": "[ADVANCED] Override guestinfo options, used to customize the GuestOS."
    },
    "vm_config": {
        "required": False,
        "description": "[ADVANCED] Dictionary of additonal settings to pass to created VMs (e.g. the firewall)"
    },
    "infrastructure_name": {
        "required": False,
        "description": "[ADVANCED] Used internally; do not set manually"
    }
    # TODO (Stretch): Project montoring, extra helper apps, etc.
}
