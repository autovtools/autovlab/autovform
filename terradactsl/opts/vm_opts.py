VM_OPTS = { 
    "vm_name": {
        "required": True,
        "description": "The human-readable name to give the VM."
                       " must be unique within this zone.",
    },  
    "template": {
        "required": False,
        "description": "The name of the template to clone from."
                       " Required unless template_tags is set.",
    },  
    "vsphere_name": {
        "required": False,
        "description": "The name to give the VM in vSphere."
                        " default: compute a reasonable name automatically.",
    },  
    "vsphere_folder": {
        "required": False,
        "description": "The vSphere folder to place this VM in."
                        " default: let the Zone assign a folder.",
    },  
    "ignore_changes": {
        "required": False,
        "description": "Changes to these attributes will be ignored by Terraform, not triggering a rebuild."
                       " See https://www.terraform.io/docs/configuration/resources.html#ignore_changes",
        "default":["memory", "num_cpus", "network_interface", "annotation", "tags", "resource_pool_id", "disk"] 
    },  
    "num_cpus": {
        "required": False,
        "description": "Number of vCPUs to give the VM. Default: 0 (same as parent).",
        "default": 0
    },  
    "memory": {
        "required": False,
        "description": "Amount of RAM (MB) to give the VM. Default: 0 (same as parent).",
        "default": 0
    },  
    "linked_clone":{
        "required": False,
        "description": "Make this VM a linked clone. default: True",
        "default": True
    },
    "wait_for_guest_net_routable":{
        "required": False,
        "description": "See https://www.terraform.io/docs/providers/vsphere/r/virtual_machine.html#wait_for_guest_net_routable. default: False",
        "default": True
    },
    "wait_for_guest_net_timeout":{
        "required": False,
        "description": "See https://www.terraform.io/docs/providers/vsphere/r/virtual_machine.html#wait_for_guest_net_timeout. default: 0",
        "default": 0
    },
    "host_name":{
        "required": False,
        "description": "Hostname to use in the Guest OS. Default: vm_name",
    },
    "domain":{
        "required": False,
        "description": "Domain to use in the Guest OS. Overrides any Project or Zone-level setting.",
    },
    "aliases":{
        "required": False,
        "description": "[ADVANCED] A list of additional DNS names to register, if DNS host mappings are enabled."
                       " If a name does not include a domain, the value of 'domain' is used.",
        "default": []
    },
    "time_zone":{
        "required": False,
        "description": "Time-zone to use in the Guest OS. Default: UTC",
        "default": "UTC"
    },
    "customization_type":{
        "required": False,
        "description": "The type of GuestOS customization to perform. Base types: 'linux', 'windows', 'guestinfo' or 'none'."
                       " Note: '+guestinfo' can be appended to add a followup customization step"
                       " If ommitted, the tag 'customization_type' of the template vm is used.",
    },
    "vm_user":{
        "required": False,
        "description": "Guest OS username to use for guest authentication. Overrides the project-level default."
                       "(Note: this user must already exist on the source template)",
    },
    "vm_password":{
        "required": False,
        "description": "Guest OS password to use for guest authentication. Overrides the project-level default."
                       "(Note: this must be the password set on the guest template )",
    },
    ##### Networking #####
    # Manual network information (overrides zone.assign_network())
    "port_group":{
        "required": False,
        "description": "[ADVANCED] Override the zone's setting for port_group. It must already exist or be created elsewhere."
    },
    "network_id": {
        "required": False,
        "description": "[ADVANCED] The network_id of the port group to use. For internal use; do not manually specify."
    },
    "ipv4_address": {
        "required": False,
        "description": "[ADVANCED] Override the zone's ip assignment for this VM.",
    }, 
    "ipv4_netmask": {
        "required": False,
        "description": "[ADVANCED] Override the zone's netmask this VM."
                       " Given in CIDR prefix length, as an int (e.g. 24).",
    },
    "ipv4_gateway": {
        "required": False,
        "description": "[ADVANCED] Override the zone's gateway ip for this VM.",
    }, 
    "dns_servers": {
        "required": False,
        "description": "[ADVANCED] Override the zone's dns_servers for this VM.",
    }, 
    "attached_to": {
        "required": False,
        "description": "[ADVANCED] List of zones this VM is attached to."
                        " Any manual networking info is expected to be lists of the same length."
    },
    ######################
    
    "guestinfo":{
        "required": False,
        "description": "[ADVANCED] A dict blob containing customization options for use by non-standard customizers (e.g. guestinfo).",
    },

    "resource_name": {
        "required": False,
        "description": "[ADVANCED] Manually sets the name of the terraform resource. For Internal use; do not set manually."
    },

    "resolve_host": {
        "required": False,
        "description": "[ADVANCED] If true, a DNS entry for this host will be created,"
                       " if a static IP is asssigned."
                       " Default: inheritied from zone.",
    },
    "instance_names": {
        "required": False,
        "description": "TODO",
    },
    "num_instances": {
        "required": False,
        "description": "If set, create this many VMs of this type within the current context."
                       " E.g. 2 => Create two instances of this VM for each instance of the"
                       " containing project",
    },
    "role": {
        "required": False,
        "description": "[ADVANCED] String label indicating the purpose of this VM."
                       " Used internally for identifying certain classes of VMs (e.g. firewalls)."
                       " Generally should not be set manually",
    },
    "template_tags": {
        "required": False,
        "description": "[ADVANCED] Instead of providing template,"
                       " search for a template matching these tags."
                       " The higest os_version match is used"
         
    },
    "first_instance": {
        "required": False,
        "description": "The human-readable first instance number."
                       " e.g. 1 => Instance-01 is first;"
                       " 0 => Instance-00 is first"
                       " default: Inherit from project",
    },
}
