import json
import pkgutil

from terradactsl.customization.guestinfo import GuestInfo
class pfSense(GuestInfo):
    """
    pfSense customization using guestinfo properties.

    """
    default_guestinfo = {
        "host_name"       : None,
        "domain"          : None,
        "zone_names"      : None,
        "ipv4_addresses"  : None,
        "ipv4_netmasks"   : None,
        "ipv4_gateways"   : None,
        "dns_servers"     : None,
        "hosts"           : None,
        "outbound_nat"    : None,
        "dhcp_ranges"     : None,
        "gateway_routes"  : None,
        "customization_type"       : None,
    }

    def load_config(self):
        # Rename / normalize vm keys to match our guestinfo keys

        # Don't mutate the actual vm_config; make a copy
        self.vm_config = self.vm_config.copy()
        # vm has a singular version of these
        plural = ["ipv4_address", "ipv4_netmask", "ipv4_gateway"]
        for key in plural:
            # Make name plural
            new_key = key
            if key.endswith("s"):
                new_key += "es"
            else:
                new_key += "s"

            # Make value a list
            new_val = self.vm_config[key]
            if new_val and not isinstance(new_val, list):
                new_val = [new_val]
            
            self.vm_config[new_key] = new_val
        
        attached_to = self.vm_config["attached_to"]
        if not isinstance(attached_to, list):
            attached_to = [attached_to]
        
        self.vm_config["zone_names"] = attached_to

        # Let our parent do the merging logic
        super().load_config()

    def run(self):
        create_timeout = 60*60
        customize_timeout = 60*10
        sleep_duration = 30

        # pfSense has a default password; most distros don't
        default = "pfsense"
        vm_password = self.vm_config.get("vm_password", default)
        if not vm_password:
            vm_password = default
        vm_info = {
            "vm_name"       : self.vsphere_name,
            "vm_user"       : "root",
            "vm_password"   : vm_password
        }

        # Wait for the VM to be created
        self.tshell.get_vm(
            vm_info=vm_info,
            wait_for_exists=True,
            timeout=create_timeout,
            sleep_duration=sleep_duration
        )
        
        script = pkgutil.get_data("terradactsl.customization.scripts", "pfsense.py")
        self.tshell.guest_customize(
            script_contents=script,
            vm_info=vm_info,
            timeout=customize_timeout,
        )

