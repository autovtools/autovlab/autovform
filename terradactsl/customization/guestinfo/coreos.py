import json
import copy
import shutil
import pkgutil
import traceback
from pathlib import Path

from terradactsl.customization.guestinfo import GuestInfo
from terradactsl import utils

class CoreOS(GuestInfo):
    """
    CoreOS customization using guestinfo properties.

    """
    default_guestinfo = {
        "bootstrap"     : None, 
        "registry"      : None,
        "nfs"           : None,
        "nodes"         : None,

        "daemon"        : None,
        "daemon_file"   : None,
        "root_certs"    : None
    }
    # root_certs will be bundled into bootstrap archive

    def __init__(self, *args, **kwargs):
        # Because of spooky magic, these need to go first
        self.bootstrap = {}
        self.secrets = {}
        self.vminfo = {}

        super().__init__(*args, **kwargs)

    def load_config(self):
        super().load_config()

        # Needed as part of the bootstrap archive
        self.secrets = {
            "vm_user"       : self.vm_config["vm_user"],
            "vm_password"   : self.vm_config["vm_password"]
        }

        self.vm_info = copy.deepcopy(self.secrets)
        self.vm_info["vm_name"] = self.vsphere_name

        daemon_config = None
        if self.guestinfo.get("daemon_file", None):
            # Allow user to specify an alternate daemon.json base file
            daemon_config = Path(self.guestinfo["daemon_file"]).read_text()
        else:
            daemon_config = pkgutil.get_data("terradactsl.customization.files.coreos", "daemon.json")

        daemon_config = json.loads(daemon_config)

        registry_config = self.guestinfo.get("registry", {})
        # If the upstream registry is insecure, update daemon.json
        # (You should really add the signer's cert instead of using insecure)
        if registry_config:
            registry_url = registry_config["url"]
            insecure = registry_config.get("insecure", False)
            if insecure:
                insecure_registries = daemon_config.get("insecure-registries", [])
                if registry_url not in insecure_registries:
                    insecure_registries.append(registry_url)
                daemon_config["insecure-registries"] = insecure_registries

        # Compute the bootstrap payload now so we fail early on bad configs
        # (do everything but write it to the tempfile)
        if self.guestinfo["daemon"] is not None:
            # Apply user overrides last
            daemon_config = utils.dict_merge(daemon_config, self.guestinfo["daemon"])

        if self.guestinfo.get("bootstrap", None):
            bootstrap = self.guestinfo["bootstrap"]
            if not Path(bootstrap).is_file():
                msg = f"{bootstrap} does not exist!"
                self.logger.error(msg)
                raise ValueError(msg)
            self.logger.info(f"Deploying manual bootstrap: {bootstrap}")
        else:
            # Upload dest
            self.guestinfo["bootstrap"] = "/tmp/bootstrap.tar.gz"
            root_certs_list = self.guestinfo.get("root_certs", None)
            if root_certs_list:
                self.bootstrap["root_certs"] = utils.get_certs(*root_certs_list)
            self.bootstrap["daemon.json"] = json.dumps(daemon_config, indent=2)
            self.bootstrap["secrets.json"] = json.dumps(self.secrets, indent=2)

        # Delete any keys that the customizer script doesn't need
        for key in ["daemon_file", "daemon", "root_certs"]:
            del self.guestinfo[key]

    def run(self):
        """Run the Customizer on the new GuestOS

        The Guest VM is being created, self.guestinfo was already delivered
        """
        create_timeout = 60*60
        customize_timeout = 60*10
        sleep_duration = 30

        # Wait for the VM to be created
        self.tshell.get_vm(
            vm_info=self.vm_info,
            wait_for_exists=True,
            timeout=create_timeout,
            sleep_duration=sleep_duration
        )
        
        script = pkgutil.get_data("terradactsl.customization.scripts", "coreos.py")

        bootstrap = {}
        temp_dir = None
        temp_archive = None

        try:
            self.logger.debug(utils.sanitize_config(self.bootstrap))
            temp_dir = utils.get_temp_dir(prefix="bootstrap")
            utils.write_topology(self.bootstrap, temp_dir)

            temp_archive = utils.archive(temp_dir, prefix="bootstrap")
            checksum = utils.hash_file(temp_archive.name)
            self.logger.debug(f"Uploading bootstrap: {checksum}")

            self.tshell.guest_upload(
                local_path=temp_archive.name,
                remote_path=self.guestinfo["bootstrap"],
                vm_info=self.vm_info,
                permissions=0o400,
                timeout=customize_timeout,
                wait_for_ip=True,
                num_retries=15
            )

            self.logger.debug(f"Bootstrapping...")

            self.tshell.guest_customize(
                script_contents=script,
                vm_info=self.vm_info,
                timeout=customize_timeout,
            )
        except Exception as e:
            self.logger.error(f"Exception in {self.__class__.__name__}.run(): {e}")
            self.logger.error(traceback.format_exc())
        finally:
            # Ensure our tempfiles get deleted
            if temp_dir:
                shutil.rmtree(temp_dir)
            if temp_archive:
                temp_archive.close()
