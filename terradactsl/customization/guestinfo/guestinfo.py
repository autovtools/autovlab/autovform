import json
import logging
from importlib import import_module

from terrashell import TerraShell

from terradactsl import utils as utils

class GuestInfo():
    """
    Guest OS customization using guestinfo technique.

    """
    default_guestinfo = {}

    # List of supported GuestInfo customizers
    customizers = ["pfSense", "CoreOS"]
    def __init__(self, vm_config, project,
                    guestinfo_key="guestinfo.guestinfo", template_tags={}
    ):
        # Our project has nice things like a terrashell and a logger;
        #   ~~steal~~ borrow them from the project
        self.project = project
        self.logger = project.logger
        self.tshell = project.tshell

        self.vm_config = vm_config
        self.vsphere_name = self.vm_config["vsphere_name"]
        self.guestinfo_key = guestinfo_key
        self.template_tags = template_tags
        self.guestinfo = self.default_guestinfo.copy()
        self.load_config()

    def load_config(self):
        guestinfo = self.vm_config["guestinfo"]
        if guestinfo is None:
            guestinfo = {}
        # Apply everything the user explicity passed in a guestinfo block
        self.guestinfo.update(guestinfo)
        # Overwrite any remaining defaults with values
        #   from vm_config that overlap, if they exist
        #   (e.g. use the vm-level host_name if one is not passed in guestinfo)
        for key in self.default_guestinfo.keys():
            manually_set = (key in guestinfo)
            if not manually_set and self.vm_config.get(key, None):
                # Use the value from the vm block
                self.guestinfo[key] = self.vm_config[key]

    def get_extra_config(self):
        self.logger.debug(
            json.dumps(self.guestinfo, indent=2)
        )
        return {
            self.guestinfo_key: json.dumps(self.guestinfo)
        }

    def get_customizer(self):
        """
        Factory to construct and return the correct GuestInfo customizer, based on template tags.
        """
        template = self.vm_config["template"]
        tags = self.tshell.get_vm_tags(vm_name=template) 
        if not tags or "os_flavor" not in tags:
            msg = "Cannot determine GuestInfo customization type without an" \
                  " 'os_flavor' tag on the source template ({}).".format(template)
            self.logger.error(msg)
            raise ValueError(msg)

        os_flavor = tags["os_flavor"]
        if os_flavor not in self.customizers:
            # Remember to update GuestInfo.customizers if you add a new one!
            msg = "There is no GuestInfo customizer for {}." \
                  " Available: ({}).".format(
                os_flavor,
                list(GuestInfo.customizers)
            )
            self.logger.error(msg)
            raise ValueError(msg)
        else:
            # Do a semi-dynamic lookup of the right class to make
            customizer_module = import_module(f".{os_flavor.lower()}",
                package='terradactsl.customization.guestinfo'
            )
            customizer = getattr(customizer_module, os_flavor)

            return customizer(
                self.vm_config,
                self.project,
                guestinfo_key=self.guestinfo_key,
                template_tags=self.template_tags
            )

    def run(self):
        raise NotImplementedError("Call get_customizer() to get a real customizer")

