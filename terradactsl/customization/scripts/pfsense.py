#!/usr/local/bin/python
# Be explicit; PATH may not be set yet during bootup
# (Packinator is in charge of putting a link to python here)

import os
import sys
import shutil
import hashlib
import subprocess
import logging
import traceback
import argparse
import json

import xml.etree.ElementTree as ET

# https://github.com/pfsense/pfsense/blob/master/src/conf.default/config.xml

# Attempt to do proper shell escaping when appropriate
try:  # py3
    from shlex import quote
except ImportError:  # py2
    from pipes import quote

######################################################
# Helpers
######################################################
##### cmdline ######
def get_args():
    parser=argparse.ArgumentParser(
        description='pfSense Customizer: A PyTerraDactSL GuestInfo Customizer.'
    )
    parser.add_argument(
        '-v', '--verbose',
        action="store_true",
        help="Enable debug logging"
    )
    parser.add_argument(
        '-q', '--quiet',
        action="store_true",
        help="Only log errors"
    )
    parser.add_argument(
        '-l', '--log-file',
        help="Also log all output to this file"
    )
    args = parser.parse_args()

    return args

##### logging ######

def _add_handler(logger, handler, fmt, level):
    handler.setLevel(level)
    handler.setFormatter(fmt)
    logger.addHandler(handler)

def get_logger(name=None, level=logging.INFO, log_file=None):
    logger = logging.getLogger(name)
    if len(logger.handlers) == 0:
        logger.setLevel(level)
        logger.propagate = False

        fmt_str = (
            '%(asctime)s'
            ' - %(levelname)s'
            ' - %(name)sCustomizer'
            ' - %(funcName)s:%(lineno)d'
            ' - %(message)s'
        )
        fmt = logging.Formatter(fmt_str)
        _add_handler(logger, logging.StreamHandler(sys.stdout), fmt, level)
        if log_file:
            # Set up File Handler (prints events to STDOUT)
            _add_handler(logger, logging.FileHandler(log_file), fmt, level)
    return logger

##### XML ######

def find_or_append(root, child, content=None):
    tag = root.find(child)
    if tag is None:
        if content is None:
            content = '<{0}></{0}>'.format(child)
        tag = ET.fromstring(content)
        root.append(tag)

    return tag

def set_child(root, key, value):
    tag = find_or_append(root, key)
    root.find(key).text = value


def ensure_child(root, child):
    return find_or_append(root, child)

######################################################
# XML templates
INTERFACE = """        
    <{0}>
        <descr><![CDATA[{1}]]></descr>
        <if>{2}</if>
        <enable></enable>
        <ipaddr>{3}</ipaddr>
        <subnet>{4}</subnet>
        <spoofmac></spoofmac>
        <gateway>{5}</gateway>
        <ipaddrv6></ipaddrv6>
        <subnetv6></subnetv6>
        <gatewayv6></gatewayv6>
    </{0}>
"""
DNS_SERVER = "<dnsserver>{}</dnsserver>"

DHCPD = """
    <{0}>
        <enable/>
        <range>
            <from>{1}</from>
            <to>{2}</to>
        </range>
    </{0}>
"""

DNSMASQ = """
    <dnsmasq>
        <enable></enable>
        <custom_options></custom_options>
        <interface></interface>
    </dnsmasq>
"""

HOSTS = """ 
        <hosts>
            <host>{}</host>
            <domain>{}</domain>
            <ip>{}</ip>
            <descr></descr>
            <aliases></aliases>
        </hosts>
"""

NAT_DISABLED = """
    <outbound>
        <mode>disabled</mode>
    </outbound>
"""

NAT_ENABLED = """
    <outbound>
        <mode>hybrid</mode>
        <rule>
            <source>
                <network>any</network>
            </source>
            <sourceport></sourceport>
            <descr><![CDATA[NAT All Outbound Traffic]]></descr>
            <target></target>
            <targetip></targetip>
            <targetip_subnet></targetip_subnet>
            <interface>wan</interface>
            <poolopts></poolopts>
            <source_hash_key></source_hash_key>
            <destination>
                <any></any>
            </destination>
        </rule>
    </outbound>
"""

GATEWAY = """ 
        <gateway_item>
            <interface>{0}</interface>
            <gateway>{1}</gateway>
            <name>{2}</name>
            <weight>1</weight>
            <ipprotocol>inet</ipprotocol>
            <descr>{2} Gateway</descr>
        </gateway_item>
"""

ROUTE = """
            <route>
                    <network>{0}</network>
                    <gateway>{1}</gateway>
                    <descr>{1} Route</descr>
            </route>
"""         
######################################################


class Customizer():
    """
    Applies baseline customization necessary to get a network up and running.

    Does not (and should not) support all possible pfSense customizations.
    Supported keys:
        host_name
        domain
        zone_names
        ipv4_addresses
        ipv4_netmasks
        ipv4_gateways
        dns_servers
        hosts
        outbound_nat
        dhcp_ranges
        gateway_routes
        customization_type
        
    Options should be passed via 'guestinfo' from the hypervisor.
    ovfEnv should be enabled; we need to read our MAC addresses.
        https://www.virtuallyghetto.com/2012/06/ovf-runtime-environment.html
        https://code.vmware.com/forums/2530/vsphere-powercli#575077

    Directly edits /cf/conf/config.xml (the persistent configuration file),
      so changes in pfSense's config format may break the Customizer
    Example config: https://github.com/pfsense/pfsense/blob/master/src/conf.default/config.xml

    Assumes the starting config.xml was generated by 'packinator', which may use a non-standard default.
    """
    def __init__(self, log_level=logging.INFO, log_file=None):
        self.logger = get_logger(
            self.__class__.__name__,
            level=log_level,
            log_file=log_file
        )
        self.tree = None
        self.root = None
        self.new_zones = []
        self.hash_log = "/var/log/guestinfo.log"

        self.guestinfo, self.guestinfo_hash  = self.get_guestinfo()
        self.guestinfo["mac_addresses"] = self.get_mac_addresses()
        self.vmx_list = self.get_vmx_list()

        if not self.guestinfo.get("zone_names", None):
            self.guestinfo["zone_names"] = self.get_default_zone_names()

        # https://marc.info/?l=pfsense-support&m=115265154320247&w=2
        # Keep xml tags standard; only change the interface description
        self.guestinfo["interface_names"] = self.get_interface_names()

        self.logger.info(
            "[GUESTINFO {}]\n".format(self.guestinfo_hash) +
            json.dumps(self.guestinfo, indent=2)
        )
        
    def should_run(self):
        if not os.path.exists(self.hash_log):
            return True
        with open(self.hash_log, "r") as log:
            for hash_str in log:
                pass
            # Drop trailing newline
            if hash_str.endswith("\n"):
                hash_str = hash_str[:-1]
            # Check last line of file
            # should_run if hash changed
            return hash_str != self.guestinfo_hash

    def get_default_zone_names(self):
        """
        Returns as much of the sequence ["WAN", "LAN", "OPT1", "OPT2" ... ]
            as needed to name all attached interfaces.
        """
        return [name.upper() for name in self.get_interface_names()]

    def get_interface_names(self):
        """
        Returns as much of the sequence ["wan", "lan", "opt1", "opt2" ... ]
            as needed to name all attached interfaces.
        """
        zone_names = ["wan", "lan"]
        if len(self.vmx_list) > len(zone_names):
            num_opt = len(self.vmx_list) - len(zone_names)
            opts = [
                "opt{}".format(index + 1)
                for index in list(range(num_opt))
            ]
            zone_names.extend(opts)
        else:
            zone_names = zone_names[:len(self.vmx_list)]

        return zone_names
        
    def zones(self):
        zone_names = self.guestinfo["zone_names"]
        for vmx, zone in zip(self.vmx_list, zone_names):
            yield vmx, zone
        
    # Returns a dictionary
    def get_guestinfo(self):
        """
        Queries the hypervisor for a json blob of configuration info.

        Uses vmtoolsd to read 'guestinfo' property, set as an advanced setting
            (extra_config{...} in terraform)
        """
        cmd = ["vmtoolsd", "--cmd", "info-get guestinfo.guestinfo"]
        output = subprocess.check_output(cmd)
        hasher = hashlib.sha1()
        hasher.update(output)
        return json.loads(output), hasher.hexdigest()

    def get_mac_addresses(self):
        # https://www.virtuallyghetto.com/2012/06/ovf-runtime-environment.html
        cmd = ["vmtoolsd", "--cmd", "info-get guestinfo.ovfEnv"]
        output = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        output = subprocess.check_output(cmd)
        root = ET.fromstring(output)

        adapters = [child for child in root if child.tag.endswith("EthernetAdapterSection")][0]
        adapters_list = [a.attrib for a in adapters]
        ovf_mac_list = []

        for a in adapters_list:
            _key = next(k for k in a.keys() if k.endswith("mac"))
            ovf_mac_list.append(a[_key])

        return ovf_mac_list

    # After pfSense boots, the order that the vmx* interfaces show up probably won't match the order they are listed in vSphere
    # So, we get the list of mac addresses (in the order the interfaces are attachedin vSphere) letting us programatically
    # determine which vmx* is WAN, LAN, etc. based on the order Terraform attached them.
    def get_vmx_list(self):
        output = subprocess.check_output("ifconfig | grep ether | awk '{print $2}'", shell=True)
        attached_macs = output.decode().splitlines()
        expected_macs = self.guestinfo["mac_addresses"]
        
        # Sort the list of vmx* adapters to match the order of they show in vSphere
        return ['vmx{}'.format(attached_macs.index(mac)) for mac in expected_macs]

    
    def _safe_call(self, func):
        try:
            func()
        except Exception as e:
            self.logger.error("Exception in {}: {}".format(func.__name__, e))
            self.logger.error(traceback.format_exc())
        else:
            self.logger.debug("[OK] {}".format(func.__name__))

    def run(self):
        self.backup_config()
        self.parse_config()
        stages = [
            "set_hostname",
            "add_interface_tags",
            "add_dhcp_tags",
            "add_host_overrides",
            "add_dns_servers",
            "add_gateway_routes",
            "set_outbound_nat"
        ]
        for stage in stages:
            func = getattr(self, stage)
            self._safe_call(func)

        self.write_config()
        self.finalize()
        self.logger.info("[CUSTOMIZER] Customization complete! Reboot to finish applying changes.")

        if self.guestinfo.get("customization_type", None) == "guestinfo_agent":
            # We are installed as an agent and are responsible for rebooting
            os.system("reboot")

    ##### Stages of the Customization: #####
    def backup_config(self):
        shutil.copyfile('/cf/conf/config.xml', '/cf/conf/config.xml.original')

    def parse_config(self):
        self.tree = ET.parse('/cf/conf/config.xml')
        self.root = self.tree.getroot()

    def set_hostname(self):
        system_root = self.root.find("system")
        host_name = self.guestinfo.get("host_name", None)
        domain = self.guestinfo.get("domain", None)

        if host_name:
            set_child(system_root, 'hostname', host_name)
            self.logger.info("[HOSTNAME] Set hostname to {}".format(host_name)) 
        else:
            self.logger.warning("[HOSTNAME] host_name not provided")

        if domain:
            set_child(system_root, 'domain', domain)
            self.logger.info("[HOSTNAME] Set domain to {}".format(domain)) 
        else:
            self.logger.warning("[HOSTNAME] domain not provided")

    def add_interface_tags(self):
        interfaces_root = self.root.find("interfaces")
        ipv4_addresses  = self.guestinfo["ipv4_addresses"]
        ipv4_netmasks   = self.guestinfo["ipv4_netmasks"]
        ipv4_gateways   = self.guestinfo.get("ipv4_gateways", None)

        # ipv4_gateways map to the 'gateway' field in the interface tags
        #   they must match gateway names (i.e. keys) in gateway_routes
        # No gateway = leave the tag blank
        # Use gateway_routes to set default gateway
        #   https://docs.netgate.com/pfsense/en/latest/book/routing/gateways.html
        #   https://docs.netgate.com/pfsense/en/latest/book/routing/gateways.html
        if not ipv4_gateways:
            # dhcp
            ipv4_gateways = [""] * len(ipv4_addresses)
        elif len(ipv4_gateways) < len(ipv4_addresses):
            # User only gave partial gateway info:
            #  (probably only for WAN)
            # Extend the list to the correct length
            padding = [""] * (len(ipv4_addresses) - len(ipv4_gateways))
            ipv4_gateways.extend(padding)

        # If an ip is falsey, the user wants DHCP and no subnet
        ipv4_addresses = [ip if ip else "dhcp" for ip in ipv4_addresses]
        # If a netmask is falsey, the user wants DHCP and no subnet
        ipv4_netmasks = [str(cidr) if cidr else "" for cidr in ipv4_netmasks]

        # Delete any interfaces that are no longer needed
        # Things may break if they have lots of associated configurations
        #  - always start with a clean / minimal config
        for tag in interfaces_root:
            if tag.tag not in self.guestinfo["interface_names"]:
                self.logger.warning("[INTERFACES] Removing old interface: {}".format(tag.tag))
                interfaces_root.remove(tag)

        zones = self.zones()
        # assert these are all the same length
        for interface_name, ipaddr, subnet, gateway in zip(
            self.guestinfo["interface_names"],
            ipv4_addresses,
            ipv4_netmasks,
            ipv4_gateways
        ):
            # Note: in the interfaces secion, tag names are lowercase
            vmx, zone = next(zones)
            self.logger.info(
                "[INTERFACES] Applying {}/{} via '{}' to {} ({})".format(
                    ipaddr, subnet, gateway, vmx, zone
                )
            )

            tag = interfaces_root.find(interface_name)
            if tag is not None:
                # There is currently a tag for this zone, maybe the user has special settings
                # Try to update the existing tag with the user specified ip / subnet / gateway
        
                # Assert the preprocessing at the beginnning of this function means we do not
                #  have to check for special cases here
                set_child(tag, 'if', vmx)
                set_child(tag, 'descr', zone)
                set_child(tag, 'ipaddr', ipaddr)
                set_child(tag, 'subnet', subnet)

                if gateway:
                    set_child(tag, 'gateway', gateway)
                else:
                    set_child(tag, 'gateway', "")
        
                # Ensure enabled
                ensure_child(tag, 'enable')
            else:
                # Tag does not exist, we need to make a new one

                # Assert that nothing is None here, at worst we have ''
                interface_xml = INTERFACE.format(
                    interface_name,
                    zone,
                    vmx,
                    ipaddr,
                    subnet,
                    gateway
                )
                tag = ET.fromstring(interface_xml)
                interfaces_root.append(tag)

                self.logger.info("[ZONE] {}".format(zone))
                # Remember we added this interface / zone,
                #   so we can add an allow-all rule later:
                # easyrule pass {ZONE} any any any any
                self.new_zones.append(interface_name)

    def add_dns_servers(self):
        system_root = self.root.find("system")
        dns_servers = self.guestinfo.get("dns_servers", [])
        if not dns_servers:
            self.logger.info("[DNS] Nothing to do")
            return 

        for dns in dns_servers:
            if dns:
                dns_xml = DNS_SERVER.format(dns)
                tag = ET.fromstring(dns_xml)
                system_root.append(tag)
                self.logger.info("[DNS] {}".format(dns))

    def add_dhcp_tags(self):
        dhcpd_root = find_or_append(self.root, "dhcpd")
        dhcp_ranges = self.guestinfo.get("dhcp_ranges", None)
        if not dhcp_ranges:
            # Since rogue DHCP from the template config.xml may break our network,
            #   assume no DHCP settings => no DHCP
            self.logger.warning("[DHCPD] No dhcp_ranges given; disabling DHCP.")
            dhcp_ranges = [None]*len(self.guestinfo["interface_names"])

        zones = self.zones()
        for interface_name, dhcp_range in zip(
            self.guestinfo["interface_names"],
            dhcp_ranges
        ):
            tag = dhcpd_root.find(interface_name)

            if tag and not dhcp_range:
                # User does not want DHCP, but there is an existing DHCP tag
                enabled = tag.find("enable")
                if enabled:
                    # DHCP is enabled; disable it
                    tag.remove(enabled)

            if not dhcp_range:
                # Continue to next interface / zone
                continue

            # User wants DHCP on this interface / zone
            dhcp_from, dhcp_to = dhcp_range

            self.logger.debug("[DHCPD] Interface: {}".format(interface_name))

            if tag is not None:
                # Tag already exists; update it to preserve settings
                range_tag = tag.find('range')

                set_child(range_tag, 'from', dhcp_from)
                set_child(range_tag, 'to', dhcp_to)

                # The dhcpd entry may exist but be disabled - ensure enabled
                ensure_child(tag, 'enable')
            else:
                # Tag does not exist, we need to make a new one
                dhcpd_xml = DHCPD.format(interface_name, dhcp_from, dhcp_to)

                tag = ET.fromstring(dhcpd_xml)
                dhcpd_root.append(tag)

            self.logger.info(
                "[DHCPD] {} : {} - {}".format(interface_name, dhcp_from, dhcp_to)
            )

    def add_host_overrides(self):

        dnsmasq_root = find_or_append(self.root, "dnsmasq", DNSMASQ)
        hosts = self.guestinfo.get("hosts", {})
        if not hosts:
            self.logger.info("[HOST OVERRIDE] Nothing to do")
            return

        for name, ip in hosts.items():
            self.logger.info("[HOST OVERRIDE] {} -> {}".format(name, ip))
            host = None
            domain = None
            split_fqdn = name.split('.', 1)

            # If we got an fqdn, unpack it
            if len(split_fqdn) > 1:
                host, domain = split_fqdn
            else:
                # use the domain of pfSense
                host = split_fqdn
                domain = self.root.find('domain').text

            # Wildcard hosts are not natively supported, but can be supported with
            # a custom option like 'address=/mydomain/1.2.3.4'
            if host == "*":
                self.logger.info("[DOMAIN OVERRIDE] {} -> {}".format(domain, ip))
                custom_options = find_or_append(dnsmasq_root, 'custom_options').text
                if custom_options:
                    custom_options = custom_options.splitlines()
                else:
                    custom_options = []

                # Remove any conflicting options
                custom_options = [o for o in custom_options if domain not in o.split("/")]
                # Warning: an existing host override will take precidence over this new wildcard
                custom_options.append("address=/{}/{}".format(domain, ip))

                self.logger.debug("[CUSTOM OPTIONS]: {}".format(custom_options))
                set_child(dnsmasq_root, "custom_options", '\n'.join(custom_options) )
            else:
                # Normal (non-wildcards) can be added in the hosts block

                # If a matching entry exists, delete it
                for tag in dnsmasq_root.findall('hosts'):
                    if tag.find('host').text == host and tag.find('domain').text == domain:
                        self.logger.warning("Removing existing hosts entry for {}->{}".format(name, tag.find('ip').text))
                        dnsmasq_root.remove(tag)

                hosts_xml = HOSTS.format(host, domain, ip)
                tag = ET.fromstring(hosts_xml)        
                dnsmasq_root.append(tag)

    def set_outbound_nat(self):
        nat_tag = find_or_append(self.root, "nat", "<nat></nat>")
        # Re-write the outbound tag with the ENABLED or DISBALED template
        outbound_tag = nat_tag.find("outbound")
        if outbound_tag:
            nat_tag.remove(outbound_tag)

        if self.guestinfo.get("outbound_nat", True):
            outbound_tag = ET.fromstring(NAT_ENABLED)
        else:
            outbound_tag = ET.fromstring(NAT_DISABLED)

        nat_tag.append(outbound_tag)

    def add_gateway_routes(self):
        gateways_root = find_or_append(self.root, "gateways")
        staticroutes_root = find_or_append(self.root, "staticroutes")
     
        gateway_routes = self.guestinfo.get("gateway_routes", {})
        if not gateway_routes:
            self.logger.info("[ROUTES] Nothing to do.")
            return

        # If any matching entry exists, delete them (allow re-customization)
        for gateway_name, gateway_data in gateway_routes.items():
            for tag in gateways_root.findall("gateway_item"):
                if tag.find("name").text == gateway_name or tag.find("gateway").text == gateway_data["via"] :
                    self.logger.warning(
                        "[ROUTES] Removing existing gateway entry for {} / {}".format(
                            gateway_name, gateway_data["via"]
                        )
                    )
                    gateways_root.remove(tag)

        # Loop again to actually add the routes
        for gateway_name, gateway_data in gateway_routes.items():
            
            if gateway_name.lower() == "default":
                set_child(gateways_root, "defaultgw4", gateway_name)
                set_child(gateways_root, "defaultgw6", "")

            route_interface = self.zone_to_interface(gateway_data["zone"])

            # WARNING: Do not include non-universal routes or gateways in the template image
            # - No validation is done on the routes, so invalid or duplicate routes will break things
            gateway_xml = GATEWAY.format(
                route_interface,
                gateway_data["via"],
                gateway_name
            )
            tag = ET.fromstring(gateway_xml)
            gateways_root.append(tag)
            self.logger.info("[ROUTES] Configured gateway {} -> {}".format(gateway_name, gateway_data))

            routes = gateway_data.get("routes", [])
            for network in routes:
                route_xml = ROUTE.format(network, gateway_name)
                tag = ET.fromstring(route_xml)
                staticroutes_root.append(tag)

            self.logger.info(
                "[ROUTES] Added {} via {} ({} Gateway)".format(
                    routes, gateway_data["via"], gateway_name
                )
            )
    def zone_to_interface(self, zone_name):
        index = self.guestinfo["zone_names"].index(zone_name)
        return self.guestinfo["interface_names"][index]

    def write_config(self):
        self.logger.info("[CUSTOMIZER] Saving config")
        # Flush + Apply the new config.xml
        # html will make full closing tags like <tag></tag>
        #   instead of default (xml), <tag/>
        # Cosmetic; either is fine
        self.tree.write('/cf/conf/config.xml.custom', method='html')
        # Actually apply the config change: DANGEROUS
        shutil.move('/cf/conf/config.xml.custom', '/cf/conf/config.xml')
        # Delete any backups so pfSense won't restore them if hard-reset
        os.system("rm -f /cf/confg/backup/*")

    def finalize(self):
        # https://docs.netgate.com/pfsense/en/latest/development/executing-commands-at-boot-time.html
        boot_script = "/usr/local/etc/rc.d/allow_all.sh"
        allow_all = """
#!/bin/sh
# First-boot script to open up the firewall
#   (this ensures any configuration traffic is allowed)
for interface in {}; do
    easyrule pass $interface any any any any
done
# Self-delete
rm -- "$0"
"""
        with open(boot_script, "w") as script:
            contents = allow_all.format(
                " ".join(self.guestinfo["interface_names"])
            )
            script.write(contents)

        os.system("chmod +x {}".format(boot_script))

        self.logger.info("[CUSTOMIZER] Run-once script written to: {}".format(boot_script))

        # Log the guestinfo hash so we don't accidentally re-apply it
        with open(self.hash_log, "w") as log:
            log.write("{}\n".format(self.guestinfo_hash))
        
        self.logger.info(
            "[CUSTOMIZER] Guestinfo hash ({}) written to {}".format(
                self.guestinfo_hash,
                self.hash_log,
            )
        )
    ##### END OF Customizer  #####

if __name__ == "__main__":
    args = get_args()
    log_level = logging.INFO
    if args.verbose:
        log_level = logging.DEBUG
    elif args.quiet:
        log_level = logging.ERROR

    cust = Customizer(
        log_level=log_level,
        log_file=args.log_file
    )
    if cust.should_run():
        cust.run()
    else:
        cust.logger.info(
            "Not running because guestinfo has not changed {}.".format(
                cust.guestinfo_hash
            )   
        ) 
