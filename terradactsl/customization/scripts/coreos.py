#!/usr/bin/env python3

import os
import sys
import json
import time
import shlex
import shutil
import socket
import hashlib
import logging
import argparse
import tempfile
import traceback
import subprocess
from pathlib import Path

# Must be pip installed on template image
import paramiko #pylint: disable=import-error

######################################################
# Helpers
######################################################
##### cmdline ######
def get_args():
    parser=argparse.ArgumentParser(
        description='CoreOS Customizer: A PyTerraDactSL GuestInfo Customizer.'
    )
    parser.add_argument(
        '-v', '--verbose',
        action="store_true",
        help="Enable debug logging"
    )
    parser.add_argument(
        '-q', '--quiet',
        action="store_true",
        help="Only log errors"
    )
    parser.add_argument(
        '-l', '--log-file',
        help="Also log all output to this file"
    )
    parser.add_argument(
        '-f', '--file',
        help="Read guestinfo from a file instead of calling vmtoolsd"
    )
    args = parser.parse_args()

    return args

def hash_file(filename):
    chunk_size = 65536
    hasher = hashlib.sha256()
    with open(filename, "rb") as in_file:
        while True:
            data = in_file.read(chunk_size)
            if not data:
                break
            hasher.update(data)
    return hasher.hexdigest()

def load_json(path):
    with open(path, "r") as json_file:
        return json.load(json_file)

def write_json(obj, path, indent=2):
    with open(path, "w") as json_file:
        return json.dump(obj, json_file, indent=indent)
##### logging ######

def _add_handler(logger, handler, fmt, level):
    handler.setLevel(level)
    handler.setFormatter(fmt)
    logger.addHandler(handler)

def get_logger(name=None, level=logging.INFO, log_file=None):
    logger = logging.getLogger(name)
    if len(logger.handlers) == 0:
        logger.setLevel(level)
        logger.propagate = False

        fmt_str = (
            '%(asctime)s'
            ' - %(levelname)s'
            ' - %(name)sCustomizer'
            ' - %(funcName)s:%(lineno)d'
            ' - %(message)s'
        )
        fmt = logging.Formatter(fmt_str)
        _add_handler(logger, logging.StreamHandler(sys.stdout), fmt, level)
        if log_file:
            # Set up File Handler (prints events to STDOUT)
            _add_handler(logger, logging.FileHandler(log_file), fmt, level)
    return logger

class Customizer():
    """Bootstrap a docker swarm cluster using guestinfo config

    """
    def __init__(self, log_level=logging.INFO, log_file=None, guestinfo_file=None):
        self.pkey = None
        self.pub_key = None
        self.key_authorized = False
        self.logger = get_logger(
            self.__class__.__name__,
            level=log_level,
            log_file=log_file
        )
        self.hostname = socket.gethostname()
        self.hash_log = "/var/log/guestinfo.log"

        self.guestinfo, self.guestinfo_hash  = self.get_guestinfo(
            guestinfo_file=guestinfo_file
        )

        self.logger.info(
            "[GUESTINFO {}]\n".format(self.guestinfo_hash) +
            json.dumps(self.guestinfo, indent=2)
        )
        self.temp_dir = tempfile.mkdtemp(prefix="bootstrap")
        self.service_dir = self.guestinfo["nfs"]["services"]
        self.secrets = {}
        
    def should_run(self):
        """Returns True if the guestinfo is 'new', and needs to be applied

        """
        if not os.path.exists(self.hash_log):
            return True
        hash_str = ""
        with open(self.hash_log, "r") as log:
            for hash_str in log:
                pass
            # Drop trailing newline
            if hash_str.endswith("\n"):
                hash_str = hash_str[:-1]
            # Check last line of file
            # should_run if hash changed
            return hash_str != self.guestinfo_hash

    # Returns a dictionary
    def get_guestinfo(self, guestinfo_file=None):
        """Retrieves the guestinfo json config from vmtoolsd, or a file

        Uses vmtoolsd to read 'guestinfo' property, set as an advanced setting
            (extra_config{...} in terraform)
        """
        
        if guestinfo_file:
            contents = load_json(guestinfo_file)
            file_hash = hash_file(guestinfo_file)
            return contents, file_hash

        cmd = ["vmtoolsd", "--cmd", "info-get guestinfo.guestinfo"]
        output = subprocess.check_output(cmd)
        hasher = hashlib.sha256()
        hasher.update(output)
        return json.loads(output), hasher.hexdigest()

    def _safe_call(self, func):
        try:
            func()
        except Exception as e:
            self.logger.error("[FAIL] {}: {}".format(func.__name__, e))
            self.logger.error(traceback.format_exc())
        else:
            self.logger.debug("[OK] {}".format(func.__name__))

    def get_role(self, node_config):
        if node_config.get("manager", False):
            return "manager"
        else:
            return "worker"

    def set_node_label(self, node, key, val):
        self.logger.debug(f"{node} Setting Label: {key}={val}")
        cmd = ["docker", "node", "update", "--label-add", "key=val", node]
        subprocess.check_call(cmd)


    def get_swarm_state(self):
        cmd = ["docker", "info", "--format", "{{.Swarm.LocalNodeState}}"]
        output = subprocess.check_output(cmd).decode()
        # The first line is the status ('active', 'inactive'), etc.
        return output.splitlines()[0]

    def get_deploy_env(self):
        """Return an environment dict needed to deploy services

        Example nfs config:
        "nfs":{
            "services":"/opt/swarm",
            "driver_opts":"addr=127.0.0.1,nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2",
            "volumes":"127.0.0.1:/docker/volumes/"
        }
        """
        env = {}
        nfs = self.guestinfo["nfs"]
        registry = self.guestinfo["registry"]
        env = {
            "REGISTRY": registry["url"], 
            "SERVICE_DIR": nfs["services"],
            "NFS_DRIVER_OPTS": nfs["driver_opts"],
            "NFS_VOLUME_DIR": nfs["volumes"],
        }
        return env

    def get_nodes(self, role=None, exclude_self=False):
        """Iterate over the nodes in the cluster.

        Generates (node, node_config) pairs.
        If role is given, only nodes with that role are returned.
        If exclude_self is true, the node running this script is excluded
        """
        nodes= self.guestinfo["nodes"].items()
        for node, node_config in self.guestinfo["nodes"].items():
            if exclude_self and node == self.hostname:
                continue
            elif role is None:
                yield node, node_config
            elif self.get_role(node_config) == role:
                yield node, node_config
            
        return self.guestinfo["nodes"].items()

    
    def require_temp_dir(self):
        if not (self.temp_dir and os.path.isdir(self.temp_dir)):
            raise Exception(f"[FAIL] temp_dir {self.temp_dir} does not exist")

    def _swarm_join(self, role):
        if role not in ["manager", "worker"]:
            raise ValueError(f"Role {role} invalid!")
        cmd = ["docker", "swarm", "join-token", role]
        output = subprocess.check_output(cmd).decode()
        for line in output.splitlines():
            line = line.strip()
            if line.startswith("docker swarm join"):
                return line
        
        raise ValueError(f"[FAIL] Failed to get join token for {role}!")

    def swarm_join_manager(self):
        return self._swarm_join("manager")

    def swarm_join_worker(self):
        return self._swarm_join("worker")

    def ssh_connect(self, host):
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        # Wait until DNS resolution is available
        while True:
            try:
                socket.gethostbyname(host)
                break
            except Exception as e:
                self.logger.warning(e)
                time.sleep(1)
                
        if self.key_authorized:
            # Key auth as core
            client.connect(
                host,
                username="root",
                pkey=self.pkey
            )
        else:
            # Password auth
            client.connect(
                host,
                username=self.secrets["vm_user"],
                password=self.secrets["vm_password"]
            )
        return client

    def sftp_connect(self, host):
        client = self.ssh_connect(host)
        return client.open_sftp()

    def ssh_upload(self, host, src_path, dest_path):
        with self.sftp_connect(host) as sftp:
            sftp.put(src_path, dest_path)

    def ssh_exec(self, host, command, alt_text=None):
        ret = None
        # Some commands may have secrets in them (like join tokens)
        #   if alt_text is set, display that instead
        display_cmd = alt_text
        if not display_cmd:
            display_cmd = command
        self.logger.debug(f"ssh_exec @{host}: {display_cmd}")
        with self.ssh_connect(host) as client:
            stdout, stderr, stdin = client.exec_command(command)
            ret = stdout.channel.recv_exit_status()
            self.logger.debug(f"ssh_exec @{host}: returned {ret}")
            if ret != 0:
                self.logger.error(f"[FAIL] ssh_exec @{host} {display_cmd} : returned {ret}")
            else:
                self.logger.info(f"[OK] ssh_exec @{host} {display_cmd} : success")
            
        return ret

    def exec_all(self, command, role=None, exclude_self=False, alt_text=None):
        for node, node_config in self.get_nodes(role=role, exclude_self=exclude_self):
            self.ssh_exec(node, command, alt_text=alt_text)

    def put_recursive(self, host, src_path, dest_path):
        # https://gist.github.com/johnfink8/2190472#file-ssh-py-L76
        #  recursively upload a full directory
        os.chdir(os.path.split(src_path)[0])
        parent=os.path.split(src_path)[1]
        with self.sftp_connect(host) as sftp:
            for dir_path, dir_names, file_names in os.walk(parent):
                try:
                    sftp.mkdir(os.path.join(dest_path, dir_path))
                except Exception as e:
                    self.logger.warning(f"put_all: {e}")

                for name in file_names:
                    src = os.path.join(dir_path, name)
                    dest = os.path.join(dest_path, dir_path, name)
                    self.logger.debug(f"put: {src} -> {dest}")
                    sftp.put(src, dest)

    def upload_all(self, src_path, dest_path, role=None, exclude_self=False):
        nodes = self.get_nodes(role=role, exclude_self=exclude_self)
        for node, node_config in nodes:
            if os.path.isfile(src_path):
                self.ssh_upload(node, src_path, dest_path)
            elif os.path.isdir(src_path):
                self.put_recursive(node, src_path, dest_path)
            else:
                raise Exception(f"[FAIL] {src_path} not found!")
            

    ##### Dispatcher: #####

    def run(self):
        stages = [
            "validate_bootstrap",
            "extract_bootstrap",
            "get_secrets",
            "generate_key",
            "authorize_key",
            "install_certs",
            "configure_docker",
            "swarm_init",
            "cluster_nfs_init",
            "label_nodes",
            "harden_nodes",
            "deploy_services",
        ]
        for stage in stages:
            func = getattr(self, stage)
            self._safe_call(func)

        self.finalize()
        self.logger.info("[CUSTOMIZER] Customization complete!")


    ##### Stages of the Customization: #####
    def validate_bootstrap(self):
        """Compute the hash of the bootstrap archive
        
        There is nothing to compare the hash to, because uploader did
            not know the hash until it was too late to update guestinfo

        Hashing would not have given any real security though,
            because obvious race condition, so just trust this is
            'out of band' setup and 'secure' (enough)
        """
        prefix = "[VALIDATE_BOOTSTRAP]"
        bootstrap_archive = self.guestinfo["bootstrap"]
        if not os.path.isfile(bootstrap_archive):
            err = f"{prefix} Archive not found: {bootstrap_archive}"
            self.logger.error(err)
            raise Exception(err)

        actual = hash_file(bootstrap_archive)
        msg = f"{prefix} Archive hash: {actual}"
        self.logger.info(msg)

    def extract_bootstrap(self):
        """Extract the bootstrap archive to the temp dir

        bootstrap.tar.gz
          - secrets.json  : vm_user / vm_password for initial auth
          - daemon.json   : a baseline /etc/docker/daemon.json
          - root_certs/
            - Certs (e.g. root CA certs) to add to trusted root store
          - autoXXXXX/
            - bootstrap services needed to kick of remaining services
          - XXXX/
            - all other services that should be deployed
        """
        self.require_temp_dir()
        prefix = "[EXTRACT_BOOTSTRAP]"
        bootstrap_archive = self.guestinfo["bootstrap"]
        self.logger.debug(f"{prefix} Extracting {bootstrap_archive} to {self.temp_dir}")
        shutil.unpack_archive(bootstrap_archive, self.temp_dir)
        self.logger.info(f"{prefix} Extracted {bootstrap_archive} to {self.temp_dir}")

    def generate_key(self):
        prefix = "[GENERATE_KEY]"
        key_dir = Path("/root/.ssh/")
        key_name = "cluster.id_rsa"
        num_bits = 4096
        key_file = str(key_dir / key_name)
        pkey = None
        if os.path.exists(key_file):
            pkey = paramiko.RSAKey.from_private_key_file(key_file)
            # If we have run the customizer before, we may have an authorized key
            self.logger.info(f"{prefix} Using existing key from {key_file}")
            # Assuming the nodes are hardened, it's the only way to auth
            self.key_authorized = True
        else:
            # Generate an RSA key
            self.logger.debug(f"{prefix} Generating key with {num_bits} bits...")
            pkey = paramiko.RSAKey.generate(num_bits)
            # mkdir -p /root/.ssh ; chmod 07000 /root/.ssh
            self.logger.debug(f"{prefix} Creating root .ssh dir")
            key_dir.mkdir(parents=True, exist_ok=True)
            os.chmod(str(key_dir), 0o700)
            # Write the private keyto /root/.ssh/ in case the user needs it later
            self.logger.info(f"{prefix} Writing private key to {key_file}")
            pkey.write_private_key_file(key_file)

        # Save the key object and pub key string for other stages
        self.pkey = pkey
        self.pub_key = f"{pkey.get_name()} {pkey.get_base64()} {key_name}"

        self.logger.info(f"{prefix} Cluster key: {self.pub_key}")

    def authorize_key(self):
        """Authorize the management key for core@host

        Note: The core user is part of FCOS
        """
        prefix = "[AUTHORIZE_KEY]"
        if not self.pub_key:
            raise Exception(f"{prefix} Key does not exist")
        if self.key_authorized:
            self.logger.info(f"{prefix} Assuming key was already authorized")
            return
        # Carefully construct a command string
        #   Note: no user input, so probably safe
        ssh_dir = "$HOME/.ssh"
        authorized_keys = f"{ssh_dir}/authorized_keys"
        commands = [
            f'mkdir -p "{ssh_dir}"',
            f'chmod 0700 "{ssh_dir}"',
            f'echo "{self.pub_key}" >> "{authorized_keys}"',
            f'chmod 0600 "{authorized_keys}"',
        ]
        authorize_payload = "; ".join(commands)
        # The user created by packer allows password-less sudo
        # 1) sudo (become root)
        # 2) su to the target user (core / root)
        #   (which is allowed without password because we are root)
        # 3) execute the payload, which will drop the key in the correct ~/.ssh
        #   (with correct perms / ownership for target user)
        # Note: also authorize the key for root,
        #   since core ~= root and we need root for uploading /etc/ files
        for user in ["root", "core"]:
            authorize_cmd = "sudo su {} -c {}".format(
                user,
                shlex.quote(authorize_payload)
            )
            self.exec_all(authorize_cmd)
        # Use key auth from now on
        self.key_authorized = True

    def get_secrets(self):
        self.require_temp_dir()
        prefix = "[GET_SECRETS]"
        secrets_path = os.path.join(self.temp_dir, "secrets.json")
        self.logger.debug(f"{prefix} Loading secrets from {secrets_path}")
        self.secrets = load_json(secrets_path)

        # Don't log the actual secrets
        self.logger.info(f"{prefix} Loaded {len(self.secrets)} secrets")
        self.logger.debug(f"{prefix} Secret names: {list(self.secrets)}")
        
        os.remove(secrets_path)
        self.logger.debug(f"{prefix} Deleted {secrets_path}")

    def install_certs(self):
        """Installs bootstrap certs to the trusted root store.

        These certs should include any upstream CA certs needed for services
        to work correctly
        
        In general, this should minimally contain the signer of the
        container registry cert that we intend to pull from
        """
        self.require_temp_dir()
        prefix = "[INSTALL_CERTS]"
        dest_dir = "/etc/pki/ca-trust/source/anchors/"
        
        certs_path = os.path.join(self.temp_dir, "root_certs")
        if not os.path.isdir(certs_path):
            self.logger.info(f"{prefix} No root_certs to install ({certs_path} not found)")
            return
        shutil.copytree(certs_path, dest_dir, dirs_exist_ok=True)
        installed = os.listdir(dest_dir)
        # Lock down permissions
        for cert_file in installed:
            cert_path = os.path.join(dest_dir, cert_file)
            os.chmod(cert_path, 0o400)

        self.upload_all(certs_path, dest_dir)
        self.logger.info(f"{prefix} Installed: {installed})")
        
        self.exec_all("update-ca-trust")
        self.logger.info(f"{prefix} Trusted: {len(installed)} certs")

    def configure_docker(self):
        """Compute a daemon.json for Docker and deploy it to all nodes

        """
        self.require_temp_dir()
        prefix = "[CONFIGURE_DOCKER]"
        dest_path = "/etc/docker/daemon.json"
        daemon_path = os.path.join(self.temp_dir, "daemon.json")

        os.chmod(daemon_path, 0o600)

        self.logger.debug(f"{prefix} Uploading {daemon_path} to {dest_path}")
        self.upload_all(daemon_path, dest_path)
        self.logger.info(f"{prefix} Uploaded daemon.json to {dest_path}")
        
        self.logger.debug(f"{prefix} Restarting docker daemon")
        self.exec_all("systemctl restart docker")
        self.logger.info(f"{prefix} Restarted docker daemon")

        os.remove(daemon_path)
        self.logger.debug(f"{prefix} Deleted {daemon_path}")
        
    def swarm_init(self):
        prefix = "[SWARM_INIT]"
        self.logger.info(f"{prefix} Initalizing swarm")
        # If we are already in a swarm, swarm init would fail
        state = self.get_swarm_state()
        self.logger.info(f"{prefix} Swarm: {state}")
        if state == "inactive":
            # Haven't called swarm init yet
            self.logger.info(f"{prefix} Starting a new swarm")
            subprocess.check_call(["docker", "swarm", "init"])
        else:
            self.logger.info(f"{prefix} Using existing swarm")
            
        roles = ["manager", "worker"]
        cmds = [self.swarm_join_manager(), self.swarm_join_worker()]
        # Don't log the token, it's a secret
        alt_text = "docker swarm join [{}]"
        role = "manager"
        for role, cmd in zip(roles, cmds):
            self.logger.info(f"{prefix} Joining all {role}s ...")
            self.exec_all(cmd, role=role, exclude_self=True, alt_text=alt_text.format(role))

    def cluster_nfs_init(self):
        """Bootstrap a nfs share for the cluster to use

        Currently, this node will store all data.
        Eventually, this may not be required, and there may be an option
            to replicate the share on multiple nodes as warm spares
        """
        prefix = "[CLUSTER_NFS_INIT]"
        proxy_env_path = "/etc/cluster-nfs/cluster-nfs-proxy.env"
        proxy_env = "\n".join(
            [
                f"CLUSTER_NFS=cluster-nfs.{self.hostname}",
                f"MOUNT_LINK={self.service_dir}"
            ]
        )
        
        self.logger.info(f"{prefix} {proxy_env}")
        with open(proxy_env_path, "w") as env_file:
            env_file.write(proxy_env)
        os.chmod(proxy_env_path, 0o600)
        self.logger.info(f"{prefix} Uploading {proxy_env_path} to all nodes")
        self.upload_all(proxy_env_path, proxy_env_path, exclude_self=True)
        
        self.logger.info(f"{prefix} Starting cluster-nfs ...")
        
        nfs_svc = "cluster-nfs.service"
        proxy_svc = "cluster-nfs-proxy.service"

        enable_cmd = ["systemctl", "enable", nfs_svc]
        start_cmd = ["systemctl", "start", nfs_svc]
        subprocess.check_call(enable_cmd)
        subprocess.check_call(start_cmd)
        self.logger.info(f"{prefix} cluster-nfs started")

        init_proxy = [
            f"systemctl enable {proxy_svc}",
            f"systemctl start {proxy_svc}",
        ]
        proxy_cmd = "; ".join(init_proxy)
        
        self.logger.info(f"{prefix} Starting cluster-nfs-proxy ...")
        self.exec_all(proxy_cmd)
        self.logger.info(f"{prefix} cluster-nfs-proxy started")

    def label_nodes(self):
        prefix = "[LABEL_NODES]"
        for node, node_config in self.get_nodes():
            labels = node_config.get("labels", {})
            for key, val in labels.items():
                self.set_node_label(node, key, val)

    def harden_nodes(self):
        prefix = "[HARDEN_NODES]"
        self.logger.info(f"{prefix} Hardening all nodes ...")
        self.exec_all("/usr/local/bin/harden-node")

    def deploy_services(self):
        """Deploy docker services from the bootstrap archive

        """
        prefix = "[DEPLOY_SERVICES]"
        self.logger.debug(f"{prefix} Copying {self.temp_dir}/* to {self.service_dir}")
        shutil.copytree(self.temp_dir, self.service_dir, dirs_exist_ok=True)
        self.logger.info(f"{prefix} Service files copied to {self.service_dir}")

        env = self.get_deploy_env()
        self.logger.debug(f"env: {json.dumps(env, indent=2)}")
        # Make sure that the root dir where volumes will be created exists
        volume_dir = env["NFS_VOLUME_DIR"].split(":", 1)[-1]
        if volume_dir.startswith("/"):
            volume_dir = volume_dir[1:]
        volume_dir = os.path.join(self.service_dir, volume_dir)
        self.logger.info(f"{prefix} Creating {volume_dir}")
        Path(volume_dir).mkdir(parents=True, exist_ok=True)

        autostackman_dir = os.path.join(self.service_dir, "autostackman")
        self.logger.debug(f"{prefix} Env: {json.dumps(env, indent=2)}")

        if not os.path.isdir(autostackman_dir):
            raise Exception(f"{prefix} {autostackman_dir} not found!")

        subprocess.check_call(
            ["docker", "stack", "deploy", "-c", "stack.yml", "autostackman"],
            env=env,
            cwd=autostackman_dir
        )
        
    def finalize(self):
        # Log the guestinfo hash so we don't accidentally re-apply it
        with open(self.hash_log, "w") as log:
            log.write("{}\n".format(self.guestinfo_hash))
        
        self.logger.info(
            "[CUSTOMIZER] Guestinfo hash ({}) written to {}".format(
                self.guestinfo_hash,
                self.hash_log,
            )
        )
        self.logger.debug(f"Removing {self.temp_dir}")
        ##############
        self.logger.warning(self.temp_dir)
        shutil.rmtree(self.temp_dir)
        ##############
    ##### END OF Customizer  #####

def ensure_root():
    # If we  are not root, re-exec self with sudo
    #   (default sudo on CoreOS is no password)
    if os.geteuid() != 0:
        argv = list(sys.argv)
        argv.insert(0, "/usr/bin/sudo")
        os.execv(argv[0], argv)

if __name__ == "__main__":
    ensure_root()
    args = get_args()
    log_level = logging.INFO
    #########################
    args.verbose = True
    #########################
    if args.verbose:
        log_level = logging.DEBUG
    elif args.quiet:
        log_level = logging.ERROR
    cust = Customizer(
        log_level=log_level,
        log_file=args.log_file,
        guestinfo_file=args.file
    )
    if cust.should_run():
        cust.run()
    else:
        cust.logger.info(
            "Not running because guestinfo has not changed {}.".format(
                cust.guestinfo_hash
            )   
        ) 
